package com.hemc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class WebTestSupport {
	private static final String URL = "http://xiaoyuanjiangzuo.com:7070/hemc2/api/";
	// private static final String URL = "http://localhost:8080/hemc2/api/";
	// private static final String URL = "http://115.29.174.55/act/f_a.act";
	private static HttpClient httpClient;

	@BeforeClass
	public static void init() {
		httpClient = new DefaultHttpClient();
	}

	protected String doPost(String name, JSONObject obj) {
		try {
			HttpClient httpClient = new DefaultHttpClient();
			HttpPost httpPostReq = new HttpPost(URL + name);
			String jsonString = obj.toString();
			StringEntity s = new StringEntity(jsonString, "UTF-8");
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);
			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				return result.toString();
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	protected String doGet(String string) {
		HttpGet get = new HttpGet(URL + string);
		String resp = null;
		HttpResponse response = null;
		try {
			response = httpClient.execute(get);
			HttpEntity responseEntity = response.getEntity();
			resp = EntityUtils.toString(responseEntity);
		} catch (ClientProtocolException e) {

		} catch (IOException e) {

		}

		return resp;
	}

	@AfterClass
	public static void close() {
	}
}
