package com.hemc.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class CommonMethod {
	/*
	 * 生成随机字符串
	 */
	public static String getRandomString(int length) {
		Random randGen = null;
		char[] numbersAndLetters = null;
		if (length < 1) {
			return null;
		}
		if (randGen == null) {
			randGen = new Random();
			numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz"
					+ "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
		}
		char[] randBuffer = new char[length];
		for (int i = 0; i < randBuffer.length; i++) {
			randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
		}
		return new String(randBuffer);
	}

	// 转化十六进制编码为字符串
	public static String HextoString(String s) {
		byte[] baKeyword = new byte[s.length() / 2];
		for (int i = 0; i < baKeyword.length; i++) {
			try {
				baKeyword[i] = (byte) (0xff & Integer.parseInt(
						s.substring(i * 2, i * 2 + 2), 16));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			s = new String(baKeyword, "gbk");// UTF-16le:Not
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return s;
	}

	// 将字符串转换为十六进制
	public static String StringtoHex(String s) {
		byte[] bstr = null;
		try {
			bstr = s.getBytes("gbk");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return ByteHexToString(bstr, bstr.length);
	}

	public static String ByteHexToString(byte[] b, int lenth) { // 将十六进制字节数组转换为十六进制字符串
		String str_hex = "";
		for (int i = 0; i < lenth; i++) {
			String hex = Integer.toHexString(b[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}

			str_hex = str_hex + hex.toUpperCase();

		}
		// System.out.print("\n");
		return str_hex;
	}

	public static byte[] signedBytesToUnsignedBytes(byte[] signedBytes) {
		byte[] unsignedBytes = new byte[signedBytes.length];
		for (int i = 0; i < signedBytes.length; i++) {
			int byteValue = signedBytes[i];
			if (byteValue <= 0) {
				byteValue = 256 + byteValue;
			}
			unsignedBytes[i] = (byte) byteValue;

		}
		return unsignedBytes;

	}

	/**
	 * MD5加密
	 * 
	 * @Title: getMD5
	 * @Description: TODO
	 * @param @param source
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String getMD5(byte[] source) {
		String s = null;
		char hexDigits[] = { // 用来将字节转换成 16 进制表示的字符
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
				'e', 'f' };
		try {
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			md.update(source);
			byte tmp[] = md.digest(); // MD5 的计算结果是一个 128 位的长整数，
										// 用字节表示就是 16 个字节
			char str[] = new char[16 * 2]; // 每个字节用 16 进制表示的话，使用两个字符，
											// 所以表示成 16 进制需要 32 个字符
			int k = 0; // 表示转换结果中对应的字符位置
			for (int i = 0; i < 16; i++) { // 从第一个字节开始，对 MD5 的每一个字节
											// 转换成 16 进制字符的转换
				byte byte0 = tmp[i]; // 取第 i 个字节
				str[k++] = hexDigits[byte0 >>> 4 & 0xf]; // 取字节中高 4 位的数字转换,
															// >>>
															// 为逻辑右移，将符号位一起右移
				str[k++] = hexDigits[byte0 & 0xf]; // 取字节中低 4 位的数字转换
			}
			s = new String(str); // 换后的结果转换为字符串

		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * 根据出生日期获取年龄
	 * 
	 * @Title: getAge
	 * @Description: TODO
	 * @param @param cBirthday
	 * @param @return
	 * @param @throws Exception
	 * @return String
	 * @throws
	 */
	public static String getAge(Calendar cBirthday) throws Exception {
		Date birthDay = cBirthday.getTime();
		Calendar cal = Calendar.getInstance();

		if (cal.before(birthDay)) {
			throw new IllegalArgumentException(
					"The birthDay is before Now.It's unbelievable!");
		}

		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH) + 1;
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);

		cal.setTime(birthDay);
		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

		int age = yearNow - yearBirth;

		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				// monthNow==monthBirth
				if (dayOfMonthNow < dayOfMonthBirth) {
					age--;
				}
			} else {
				// monthNow>monthBirth
				age--;
			}
		}

		return age + "";
	}

	public static Calendar StringToCalendar(String value,String format) {
		if (value == null || value.equals("")) {
			return null;
		}
		if (format==null) {
			format="yyyy-MM-dd HH:mm:ss";
		}
		Calendar dayc1 = new GregorianCalendar();
		DateFormat df = new SimpleDateFormat(format);
		Date daystart = null;
		try {
			daystart = df.parse(value);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} // start_date是类似"2013-02-02"的字符串
		dayc1.setTime(daystart); // 得到的dayc1就是你需要的calendar了
		return dayc1;
	}

	/**
	 * @param calendar
	 * @param format
	 *            默认转换格式 yyyy-MM-dd mm:HH:ss
	 * @return
	 */
	public static String CalendarToString(Calendar calendar, String format) {
		if (format == null) {
			format = "yyyy-MM-dd HH:mm:ss";
		}
		if (calendar == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateStr = sdf.format(calendar.getTime());
		return dateStr;
	}

	/**
	 * 时间比较
	 * 
	 * @Title: CompareTime
	 * @Description: TODO
	 * @param @param beginCalendar
	 * @param @param endCalendar
	 * @param @return
	 * @return String 返回多少时间前，用在话题，评论查看页面
	 * @throws
	 */
	public static String CompareTime(Calendar beginCalendar,
			Calendar endCalendar) {

		long start = beginCalendar.getTimeInMillis();
		long end = endCalendar.getTimeInMillis();

		long month = endCalendar.get(Calendar.MONTH)
				- beginCalendar.get(Calendar.MONTH);
		long day = endCalendar.get(Calendar.DATE)
				- beginCalendar.get(Calendar.DATE);
		long hour = ((end - start) / 1000 / 60 / 60);
		long minute = ((end - start) / 1000 / 60);
		long second = ((end - start) / 1000);

		if (month != 0) {
			return CalendarToString(beginCalendar, "MM-dd");
		}
		if (day != 0) {
			if (day <= 2) {
				return CalendarToString(beginCalendar, "昨天 HH:mm");
			} else {
				return CalendarToString(beginCalendar, "MM-dd HH:mm");
			}

		}
		if (hour != 0) {
			return CalendarToString(beginCalendar, "今天 HH:mm");
		}
		if (minute != 0) {
			return minute + "分钟前";
		}
		if (second != 0) {
			return "刚刚";
		}
		return "刚刚";
	}

	/**
	 * 时间距离
	 * 
	 * @param start
	 * @param end
	 * @return
	 */
	public static String getDistanceTimes(Calendar start, Calendar end) {

		long day = 0;
		long hour = 0;
		long min = 0;
		long sec = 0;
		long time1 = start.getTimeInMillis();
		long time2 = end.getTimeInMillis();
		long diff;
		if (time1 < time2) {
			diff = time2 - time1;
		} else {
			diff = time1 - time2;
		}
		day = diff / (24 * 60 * 60 * 1000);
		hour = (diff / (60 * 60 * 1000) - day * 24);
		min = ((diff / (60 * 1000)) - day * 24 * 60 - hour * 60);
		sec = (diff / 1000 - day * 24 * 60 * 60 - hour * 60 * 60 - min * 60);

		if (day > 1) {

			return day + "天后";
		}
		if (day == 1) {
			// 相差一天，不超过两天，则判断日期
			long dayDiff = end.get(Calendar.DATE) - start.get(Calendar.DATE);
			// 如果日历中日期的数字差为1的话就为明天
			if (dayDiff == 1) {
				return "明天";
			}
			// 如果日历中日期的数字差大于1的话就为N天以后了
			else if (dayDiff > 1) {
				return dayDiff + "天后";
			} else if (dayDiff < 0) {
				// 如果日历中日期的数字差小于0了应该是跨月了，因为day=1说明时间差大于24小时，大于24小时，日期按常理是+1的，所以这里应该是跨月了
				return "明天";
			} else {
				return null;// 不可能发生
			}
		} else {
			// day=0
			long dayDiff = end.get(Calendar.DATE) - start.get(Calendar.DATE);
			// 如果日历中日期的数字差为1的话就为明天
			if (dayDiff == 1) {
				return "明天";
			} else {
				return "即将开始";
			}
		}

	}

	public static String getWeekDay(Calendar c) {
		if (c == null) {
			return "周一";
		}

		if (Calendar.MONDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周一";
		}
		if (Calendar.TUESDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周二";
		}
		if (Calendar.WEDNESDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周三";
		}
		if (Calendar.THURSDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周四";
		}
		if (Calendar.FRIDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周五";
		}
		if (Calendar.SATURDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周六";
		}
		if (Calendar.SUNDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周日";
		}

		return "周一";
	}

	/**
	 * 判断两个时间是否同一天
	 * 
	 * @Title: isSameDay
	 * @Description: TODO
	 * @param @param b 时间1
	 * @param @param e 时间2
	 * @param @return
	 * @return Boolean
	 * @throws
	 */
	public static Boolean isSameDay(Calendar b, Calendar e) {
		if (b.get(Calendar.MONTH) == e.get(Calendar.MONTH)
				&& b.get(Calendar.DAY_OF_MONTH) == e.get(Calendar.DAY_OF_MONTH)) {
			return true;
		} else {
			return false;
		}

	}

	public static String getTrace(Throwable t) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		t.printStackTrace(writer);
		StringBuffer buffer = stringWriter.getBuffer();
		return buffer.toString();
	}
}
