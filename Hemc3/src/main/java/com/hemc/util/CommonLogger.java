package com.hemc.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;

public class CommonLogger {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");

	public static void error(Exception e) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		forerror.error(sw.toString());
	}

	public static void error(String error) {
		forerror.error(error);
	}

	public static void info(String info) {
		forinfo.info(info);
	}

	public static void debug(String debug) {
		fordebug.debug(debug);
	}
}
