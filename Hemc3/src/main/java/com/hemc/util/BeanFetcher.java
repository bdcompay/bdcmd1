package com.hemc.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

/**
 * 对beanFactory的封装
 * 
 * @author QingFeng.Su
 * 
 */
@Component
public class BeanFetcher implements BeanFactoryAware {
	// 定义BeanFactory为类静态变量
	private static BeanFactory factory;

	// 定义为类静态方法
	public static <T>T  getBean(String beanId,Class<T> clazz) {
		return factory.getBean(beanId, clazz);
	}

	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		factory = beanFactory;
		
	}
}
