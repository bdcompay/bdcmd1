package com.hemc.service;

import java.util.List;

import com.hemc.constant.UserType;
import com.hemc.web.dto.UserDto;

public interface UserService {

	/**
	 * 获取用户列表
	 * 
	 * @param namekeyword
	 *            用户名关键字
	 * @param userType
	 *            用户类型
	 * @param page
	 *            当前页
	 * @param pageSize
	 *            页面大小
	 * @return 用户列表
	 */
	public List<UserDto> getUserDtos(String namekeyword, UserType userType,
			int page, int pageSize);

	/**
	 * 获取用户数量
	 * 
	 * @param namekeyword
	 * @param userType
	 * @return
	 */
	public int getUserDtosCount(String namekeyword, UserType userType);

	/**
	 * 删除用户
	 * 
	 * @param userId
	 * @return
	 */
	public Boolean deleteUser(int userId);

	/**
	 * 添加用户
	 * 
	 * @param name
	 * @param userType
	 * @return
	 */
	public Boolean addUser(String name, UserType userType);

	/**
	 * 编辑用户
	 * 
	 * @param userId
	 * @param name
	 * @return
	 */
	public Boolean editUser(Integer userId, String name);

	/**
	 * 获取用户实体
	 * @param userId
	 * @return
	 */
	public UserDto getUserDto(Integer userId);
}
