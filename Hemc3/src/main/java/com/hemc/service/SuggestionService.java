package com.hemc.service;

import java.util.List;

import com.hemc.web.dto.SuggestionDto;

public interface SuggestionService {

	public List<SuggestionDto> getSuggestionDtos(String keyword, int page,
			int pageSize);

	public int getSuggestionDtosCount(String keyword);

	public Boolean addSuggestion(String contact, String content);

	public Boolean deleteSuggestion(Integer id);
}
