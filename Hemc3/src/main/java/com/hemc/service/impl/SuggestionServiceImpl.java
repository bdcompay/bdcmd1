package com.hemc.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hemc.dao.SuggestionDao;
import com.hemc.entity.Suggestion;
import com.hemc.entity.User;
import com.hemc.service.SuggestionService;
import com.hemc.util.BeansUtil;
import com.hemc.util.CommonMethod;
import com.hemc.web.dto.SuggestionDto;

@Service
public class SuggestionServiceImpl implements SuggestionService {

	@Autowired
	private SuggestionDao suggestionDao;

	private SuggestionDto castToSuggestionDto(Suggestion suggestion) {
		SuggestionDto a = new SuggestionDto();
		// 复制基本属性
		BeansUtil.copyBean(a, suggestion, true);
		a.setId(suggestion.getId());
		
		a.setCreateTime(CommonMethod.CalendarToString(
				suggestion.getCreateTime(), null));
		return a;
	}

	public List<SuggestionDto> getSuggestionDtos(String keyword, int page,
			int pageSize) {
		List<Suggestion> suggestions = suggestionDao.query(keyword, page,
				pageSize);
		List<SuggestionDto> suggestionDtos = new ArrayList<SuggestionDto>();
		for (Suggestion suggestion : suggestions) {
			SuggestionDto sDto = castToSuggestionDto(suggestion);
			suggestionDtos.add(sDto);
		}
		return suggestionDtos;
	}

	public int getSuggestionDtosCount(String keyword) {
		// TODO Auto-generated method stub
		return suggestionDao.queryAmount(keyword);
	}

	public Boolean addSuggestion(String contact, String content) {
		Suggestion suggestion = new Suggestion();
		suggestion.setContent(content);
		suggestion.setCreateTime(Calendar.getInstance());
		suggestionDao.save(suggestion);
		return true;
	}

	public Boolean deleteSuggestion(Integer id) {
		suggestionDao.delete(id);
		return true;
	}

}
