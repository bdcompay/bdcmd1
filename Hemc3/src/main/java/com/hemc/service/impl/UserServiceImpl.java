package com.hemc.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.hemc.constant.UserType;
import com.hemc.dao.UserDao;
import com.hemc.entity.User;
import com.hemc.service.UserService;
import com.hemc.util.BeansUtil;
import com.hemc.util.CommonMethod;
import com.hemc.web.dto.UserDto;

@Service
public class UserServiceImpl implements UserService {

	@Value("${test.resourceExample}")
	private String resourceExample;

	@Autowired
	private UserDao userDao;

	public List<UserDto> getUserDtos(String namekeyword, UserType userType,
			int page, int pageSize) {
		List<User> users = userDao.query(namekeyword, userType, null, page,
				pageSize);
		List<UserDto> userDtos = new ArrayList<UserDto>();
		for (User user : users) {
			UserDto userDto = castUserToUserDto(user);
			userDtos.add(userDto);
		}
		return userDtos;
	}

	public int getUserDtosCount(String namekeyword, UserType userType) {
		// TODO Auto-generated method stub
		return userDao.queryUserCount(namekeyword, userType);
	}

	private UserDto castUserToUserDto(User user) {
		UserDto userDto = new UserDto();
		BeansUtil.copyBean(userDto, user, true);
		userDto.setCreateTimeStr(CommonMethod.CalendarToString(
				user.getCreateTime(), null));
		userDto.setManagerHtml("");
		userDto.setResourceExample(resourceExample);
		return userDto;
	}

	public Boolean deleteUser(int userId) {
		userDao.delete(userId);
		return true;
	}

	public Boolean addUser(String name, UserType userType) {
		// TODO Auto-generated method stub
		User user = new User();
		user.setUsername(name);
		user.setUserType(UserType.COMMON);
		user.setCreateTime(Calendar.getInstance());
		userDao.save(user);
		return true;
	}

	public Boolean editUser(Integer userId, String name) {
		// TODO Auto-generated method stub
		User user = userDao.get(userId);
		user.setUsername(name);
		userDao.update(user);
		return true;
	}

	public UserDto getUserDto(Integer userId) {
		// TODO Auto-generated method stub

		return castUserToUserDto(userDao.get(userId));
	}
}
