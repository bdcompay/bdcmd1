package com.hemc.dao;

import java.util.List;

import com.hemc.constant.UserType;
import com.hemc.entity.User;

public interface UserDao extends IDao<User, Integer> {
	List<User> query(String namekeyword, UserType userType, String orderStr,
			int page, int pageSize);

	public int queryUserCount(String namekeyword, UserType userType);
}
