package com.hemc.dao;

import java.util.List;

import com.hemc.entity.Suggestion;


public interface SuggestionDao extends IDao<Suggestion, Integer> {
	public int queryAmount(String keyWord);

	public List<Suggestion> query(String keyWord, int page, int pageSize);

}
