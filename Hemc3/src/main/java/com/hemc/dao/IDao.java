package com.hemc.dao;

import java.io.Serializable;
import java.util.List;

/**
 * 对实体通用CRUD操作的抽象
 * 
 * @author QingFeng.Su
 * 
 * @param <E>
 *            实体的类型
 * @param <ID>
 *            实体的主键
 */
public interface IDao<E, ID extends Serializable> {

	E save(E entity);

	void delete(ID id);

	void deleteObject(E entity);

	void update(E entity);

	E get(ID id);

	int countAll();

	int maxId();

	List<E> listAll();

	List<E> list(int page, int pageSize);

	boolean exists(ID id);

	public <T> List<T> getList(final String hql, final int page,
			final int pageSize, final Object... paramlist);
}
