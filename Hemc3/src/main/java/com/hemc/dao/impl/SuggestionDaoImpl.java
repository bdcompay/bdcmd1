package com.hemc.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hemc.dao.SuggestionDao;
import com.hemc.entity.Suggestion;

@Repository
public class SuggestionDaoImpl extends IDaoImpl<Suggestion, Integer> implements
		SuggestionDao {

	public int queryAmount(String keyword) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from Suggestion t where 1=1 ");
		if (keyword != null && keyword.trim().length() > 0) {
			hql.append(" and (t.content like '%").append(keyword).append("%' ")
					.append(" or t.contact like '%").append(keyword)
					.append("%') ");
		}

		return count(hql.toString());
	}

	public List<Suggestion> query(String keyword, int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from Suggestion t where 1=1 ");
		if (keyword != null && keyword.trim().length() > 0) {
			hql.append(" and (t.content like '%").append(keyword).append("%' ")
					.append(" or t.contact like '%").append(keyword)
					.append("%') ");
		}
		hql.append(" order by t.createTime desc");
		List<Suggestion> suggestions = getList(hql.toString(), page, pageSize);
		return suggestions;
	}

}
