package com.hemc.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.hemc.constant.UserType;
import com.hemc.dao.UserDao;
import com.hemc.entity.User;

@Repository
public class UserDaoImpl extends IDaoImpl<User, Integer> implements UserDao {

	public List<User> query(String namekeyword, UserType userType,
			String orderStr, int page, int pageSize) {
		StringBuffer hql = new StringBuffer("from User  t where 1=1");
		if (namekeyword != null) {
			hql.append(" and t.username like '%").append(namekeyword)
					.append("%'");
		}
		if (userType != null) {
			hql.append(" and t.userType= '").append(userType.toString())
					.append("'");
		}
		if (orderStr != null) {
			hql.append(" order by  ").append(orderStr);
		}
		return getList(hql.toString(), page, pageSize);
		
	}

	public int queryUserCount(String namekeyword, UserType userType) {
		StringBuffer hql = new StringBuffer(
				"select count(*) from User t where 1=1");
		if (namekeyword != null) {
			hql.append(" and t.username like '%").append(namekeyword)
					.append("%'");
		}
		if (userType != null) {
			hql.append(" and t.userType= '").append(userType.toString())
					.append("'");
		}
		return count(hql.toString());
	}
}
