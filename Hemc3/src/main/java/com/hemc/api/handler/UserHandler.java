package com.hemc.api.handler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.hemc.api.dto.ObjectResult;
import com.hemc.api.dto.PostSuggestionParam;
import com.hemc.constant.APIException;
import com.hemc.constant.ResultStatus;
import com.hemc.service.SuggestionService;
import com.hemc.service.UserService;

@Controller
@RequestMapping("/api")
public class UserHandler {
	@Autowired
	private SuggestionService suggestionService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/User-postSuggestionAdd", method = RequestMethod.POST)
	public @ResponseBody ObjectResult postSuggestionAdd(
			@RequestBody PostSuggestionParam q) throws APIException {
		Boolean value = null;
		value = suggestionService.addSuggestion(q.getContact(), q.getContent());
		if (value != null) {
			ObjectResult result = new ObjectResult(value);
			result.setMessage("成功提交建议信息");
			return result;
		} else {
			ObjectResult result = new ObjectResult();
			result.setMessage("系统错误");
			result.setStatus(ResultStatus.SYS_ERROR);
			return result;
		}
	}

}
