package com.hemc.api.dto;


public class ParamBase {

	private String onlineKey;
	private Integer page;
	private Integer pageSize;

	public String getOnlineKey() {
		return onlineKey;
	}

	public void setOnlineKey(String onlineKey) {
		this.onlineKey = onlineKey;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


}