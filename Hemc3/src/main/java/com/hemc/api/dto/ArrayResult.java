package com.hemc.api.dto;

import java.util.ArrayList;
import java.util.List;

import com.hemc.constant.ListResult;
import com.hemc.constant.ResultStatus;

/**
 * @author QingFeng.Su
 * 
 * @param <E>
 */
public class ArrayResult<E> extends HandleResult {
	private int total;
	private int page;
	private int pageSize;

	private List<E> rows = new ArrayList<E>();

	public List<E> getRows() {
		return rows;
	}

	public void setRows(List<E> rows) {
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public ArrayResult(int total, List<E> rows) {
		setStatus(ResultStatus.OK);
		this.total = total;
		this.rows = rows;
	}

	public ArrayResult() {
	}

	public ArrayResult(ListResult<E> listResult) {
		setRows(listResult.getRows());
		setTotal(listResult.getTotal());
		setPage(listResult.getPage());
		setPageSize(listResult.getPageSize());
	}

}
