package com.hemc.api.dto;

public class PostSuggestionParam extends PostParam {
	private String content;
	private String contact;

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

}