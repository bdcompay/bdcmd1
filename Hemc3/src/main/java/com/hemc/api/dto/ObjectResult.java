package com.hemc.api.dto;

import com.hemc.constant.ResultStatus;


/**
 * json消息响应的包装类
 * @author QingFeng.Su
 *
 */
public class ObjectResult extends HandleResult{
	private Object result;

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}

	public ObjectResult(ResultStatus status,Object result) {
		this.result = result;
		setStatus(status);
	}

	public ObjectResult() {
		super();
	}

	public ObjectResult(ResultStatus status,String message) {
		if(status != null){
			setMessage("["+status.toString() + "]" + message);
		}
		else{
			setMessage(message);
		}
		setStatus(status);
	}

	public ObjectResult(Object result) {
		setStatus(ResultStatus.OK);
		this.result = result;
	}
	

}
