package com.hemc.web.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.hemc.api.dto.ObjectResult;
import com.hemc.constant.ResultStatus;
import com.hemc.constant.UserType;
import com.hemc.service.SuggestionService;
import com.hemc.service.UserService;
import com.hemc.web.dto.ListParam;
import com.hemc.web.dto.SuggestionDto;
import com.hemc.web.dto.UserDto;

@Controller
public class ManagerHandler {
	@Autowired
	private UserService userService;

	@Autowired
	private SuggestionService suggestionService;

	@RequestMapping(value = "/managerUsers.html", method = RequestMethod.GET)
	public ModelAndView queryUserImages() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("managerUsers");
		return mav;
	}

	@RequestMapping(value = "/queryUsers.do", method = RequestMethod.GET)
	public @ResponseBody ListParam queryUsers(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam String sSearch,
			@RequestParam Integer iSortCol_0, @RequestParam String sSortDir_0) {
		int page = iDisplayStart == 0 ? 1 : iDisplayLength / iDisplayStart + 1;
		int pageSize = iDisplayLength;

		List<UserDto> userDtos = userService.getUserDtos(sSearch, null, page,
				pageSize);
		Integer count = userService.getUserDtosCount(sSearch, null);
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(userDtos);
		return listParam;

	}

	@RequestMapping(value = "/deleteUser.do", method = RequestMethod.POST)
	public @ResponseBody Boolean deleteUser(@RequestParam Integer userId) {
		return userService.deleteUser(userId);
	}

	@RequestMapping(value = "/addUser.do", method = RequestMethod.POST)
	public @ResponseBody Boolean addUser(@RequestParam String name) {
		return userService.addUser(name, UserType.COMMON);
	}

	@RequestMapping(value = "/addUser.html")
	public ModelAndView addUser() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("createUser");
		return mav;
	}

	@RequestMapping(value = "/editUser.html", method = RequestMethod.GET)
	public ModelAndView editUser(@RequestParam Integer userId) {
		ModelAndView mav = new ModelAndView();
		UserDto userDto = userService.getUserDto(userId);
		mav.addObject("userDto", userDto);
		mav.setViewName("editUser");
		return mav;
	}

	@RequestMapping(value = "/editUser.do", method = RequestMethod.POST)
	public @ResponseBody ObjectResult editUser(@RequestParam Integer userId,
			@RequestParam String name) {
		// @ResponseBody
		// 可以返回任何类型的实体，为了让json数据传输格式标准化，我们定义一个ObjectResult作为单个实体的传输，里面包含了状态信息，描述等。
		Boolean updateResult = userService.editUser(userId, name);
		ObjectResult result = new ObjectResult();
		if (updateResult) {
			result.setMessage("更新成功了");
			result.setStatus(ResultStatus.OK);
		} else {
			result.setMessage("更新失败了");
			result.setStatus(ResultStatus.FAILED);
		}
		UserDto userDto = userService.getUserDto(userId);
		result.setResult(userDto);

		return result;
	}

	/********************* 以下是建议报错管理 ****************/
	@RequestMapping(value = "/managerSuggestion.html", method = RequestMethod.GET)
	public ModelAndView querySuggestions() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("managerSuggestion");
		return mav;
	}

	@RequestMapping(value = "/querySuggestions.do", method = RequestMethod.GET)
	public @ResponseBody ListParam querySuggestions(
			@RequestParam int iDisplayStart, @RequestParam int iDisplayLength,
			@RequestParam int iColumns, @RequestParam String sEcho,
			@RequestParam String sSearch, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0) {
		int page = iDisplayStart == 0 ? 1 : iDisplayLength / iDisplayStart + 1;
		int pageSize = iDisplayLength;

		List<SuggestionDto> suggestionDtos = suggestionService
				.getSuggestionDtos(sSearch, page, pageSize);
		Integer count = suggestionService.getSuggestionDtosCount(sSearch);
		ListParam listParam = new ListParam();
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setsEcho(sEcho);
		listParam.setAaData(suggestionDtos);
		return listParam;
	}

	@RequestMapping(value = "/deleteSuggestion.do", method = RequestMethod.POST)
	public @ResponseBody Boolean deleteSuggestion(
			@RequestParam Integer suggestionId) {
		return suggestionService.deleteSuggestion(suggestionId);
	}
	/********************* 以上是建议报错管理 ****************/
}
