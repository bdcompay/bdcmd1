package com.hemc.web.dto;

import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import com.hemc.entity.User;

/**
 * @author will.yan
 *
 */

public class UserDto extends User {

	private String createTimeStr;
	private String managerHtml;
	private String resourceExample;

	public String getResourceExample() {
		return resourceExample;
	}

	public void setResourceExample(String resourceExample) {
		this.resourceExample = resourceExample;
	}

	public String getCreateTimeStr() {
		return createTimeStr;
	}

	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}

	public String getManagerHtml() {
		return managerHtml;
	}

	public void setManagerHtml(String managerHtml) {
		this.managerHtml = managerHtml;
	}

}
