package com.hemc.constant;

/**
 * 北斗卡来源
 * 
 * @author will.yan
 */
public enum CardSourceType {

	/**
	 * 平台北斗卡，由内部人员添加
	 */
	SOURCE_AUTH(1, "平台卡"), /**
	 * 外部卡，由企业用户添加的卡
	 */
	SOURCE_OUT(2, "外来卡"), /**
	 * 未知北斗卡，当平台收到新卡的数据时变为该类型北斗卡，但当企业用户进行添加时，归为企业用户
	 */
	SOURCE_UNKNOW(3, "未知北斗卡");

	private int _value;
	private String _str;

	private CardSourceType(int value, String str) {
		_value = value;
		_str = str;
	}

	public int value() {
		return _value;
	}

	public String str() {
		return _str;
	}
}
