package com.hemc.constant;

/**
 * @author zfc
 *
 */
public class APIException extends Exception{

	private static final long serialVersionUID = 1L;
	private ResultStatus resultStatus;
	private String message;
	public APIException(ResultStatus status,String msg) {
		this.message = msg;
		this.resultStatus = status;
	}
	public ResultStatus getResultStatus() {
		return resultStatus;
	}
	public String getMessage() {
		return message;
	}

}
