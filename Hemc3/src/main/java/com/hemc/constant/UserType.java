package com.hemc.constant;

/**
 * 定义用户的角色
 * 
 * @ClassName: UserType
 * @Description: TODO
 * @author QingFeng.Su
 * @date 2013年11月16日 下午12:36:34
 */
public enum UserType {
	/**
	 * 普通用户
	 */
	COMMON("普通用户"),
	/**
	 * 社团用户
	 */
	SOCIETY("普通用户");

	private String _str;

	private UserType(String str) {
		_str = str;
	}

	public String str() {
		return _str;
	}

	public static UserType getFromString(String str) {
		if (str == null) {
			return null;
		}
		if (str.equals(COMMON.toString())) {
			return COMMON;
		}
		if (str.equals(SOCIETY.toString())) {
			return SOCIETY;
		}

		return null;
	}
}
