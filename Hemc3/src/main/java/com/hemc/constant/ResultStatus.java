package com.hemc.constant;

/**
 * @author QingFeng.Su
 *
 */
public enum ResultStatus {
	/**
	 * 成功
	 */
	OK("000"),
	/**
	 * 不存在要查找的对象
	 */
	NOT_EXIST("101"),
	/**
	 * 操作对象已存在
	 */
	ALREADY_EXIST("102"),
	/**
	 * 对象不支持(不适用)当前操作
	 */
	UNSUPPORTED("103"),
	/**
	 * 不合法的请求或参数有误
	 */
	BAD_REQUEST("201"),
	/**
	 * 密码错误或认证失败
	 */
	AUTH_FAILURE("202"),
	/**
	 * 无此权限，key无效
	 */
	NO_PERMISSION("203"),
	/**
	 * 操作失败
	 */
	FAILED("204"),
	/**
	 * 系统内部错误
	 */
	SYS_ERROR("301");
	
	/**
	 * 获得该状态对应的状态码
	 */
	private final String msgCode;

	public String getMsgCode() {
		return msgCode;
	}

	private ResultStatus(String msgCode) {
		this.msgCode = msgCode;
	}
	
	/**以该错误状态生成一个Exception,用于返回给客户端错误状态
	 * @param message 错误信息的描述
	 * @return
	 */
	public APIException exception(String message){
		return new APIException(this, message);
	}
	
}
