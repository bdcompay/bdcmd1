package com.hemc.constant;

import java.util.ArrayList;
import java.util.List;

public class ListResult<T> {
	private List<T> rows = new ArrayList<T>();
	private int total;
	private int page;
	private int pageSize;

	public List<T> getRows() {
		return rows;
	}

	public void setRows(List<T> rows) {
		this.rows = rows;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public ListResult(List<T> rows, int total, int page, int pageSize) {
		super();
		this.rows = rows;
		this.total = total;
		this.page = page;
		this.pageSize = pageSize;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

}
