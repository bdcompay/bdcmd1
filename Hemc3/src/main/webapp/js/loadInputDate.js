$(document).ready(function(){
	//加载日期框日期
	loadInputDate();
    $('.showtime').focus(function(e){
    	
	   WdatePicker({dateFmt:"yyyy-MM-dd HH:mm:ss",minDate:todayToDateStr(),maxDate:'%y-{%M+2}-%d' });

    });

});
//加载日期框日期
function loadInputDate(){
	var $showtime=$('.showtime');
	if($showtime.val()==''){$showtime.val(todayToDateStr());}

}

/**
 * 返回当前日期的字符串形式
 */
function todayToDateStr() {
	var dt= new Date();
	var str=dateToStr(dt);
	return str;
}
/**
 * 日期格式转换成字符串格式
 */
function dateToStr(getInputDate) {
	//获得输入的年
	var getInputYear=getInputDate.getUTCFullYear();
	//获得输入的月
	var getInputMonth=getInputDate.getMonth()+1;
	//获得输入的天
	var getInputDay=getInputDate.getDate();
	if (getInputYear<1000) {
		getInputYear=1900+getInputYear;
	}
	//当月份小于10时，月份前面补零
	if(getInputMonth<10){
		getInputMonth="0"+getInputMonth;
	}
	//当日期小于10时，日期前面补零
	if(getInputDay<10){
		getInputDay="0"+getInputDay;
	}
	var dtStr=getInputYear+"-"+getInputMonth+"-"+getInputDay+" 00:00:00";
	return dtStr;
}