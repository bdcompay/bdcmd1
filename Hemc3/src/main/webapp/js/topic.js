
//用户选择框
$('#sel_user').focus(function()
{
	$('.select_user_box').show();

  $('.select_user_box li a').click(function()
  {
	  $('#sel_user').val($(this).html());
	  $('.reply_topic_user_txt').val('');
	  $('.select_user_box').hide();
  });
});

$('#eventCommentList').on('focus','.reply_topic_user_txt',function()
{   	
	$('.select_user_box').show();

  $('.select_user_box li a').click(function()
  {
	  $('.reply_topic_user_txt').val($(this).html());
	  $('#sel_user').val('');
	  $('.select_user_box').hide();
  });
});	



//发布评论
$('#sendComment').click(function(){
	postComment();
})

function postComment()
{
	var userTxt=$('#sel_user').val();//发布者的名字
	var commentTxt=$('#topic_textarea').val();//发布的内容
	
	if(userTxt=='' || commentTxt=='')//名字和内容为空
	{
		alert('输入完整信息');
		//return;
	}
	else
	{
		addNewComment(/*topicId, userId,*/ userTxt, commentTxt);
	}
}

function addNewComment(/*topicId, userId,*/ userTxt, commentTxt)
{
	//alert('话题Id:'+topicId+',用户Id:'+userId+',话题内容:'+sTopicContent);
	var oUl=$('.feed-list');
	
	//生成新的评论
	oUl.prepend('<li class="media"> <a class="pullLeft" href="" title="威廉姆叔"> <img src="/website/images/user/1.jpg" class="user-logo" /> </a><div class="mediaBody"><h4 class="mediaHeading"> <span class="uesrName">'+userTxt+'</span> <span class="time">2013/12/30 20:18:56</span> </h4><p>'+commentTxt+'</p><div class="func"><div class="pullLeft"> </div><div class="reply"> <span>回复</span> <span>0</span> </div></div><div class="reply-topic-box" style="display:none;"><textarea class="reply_topic_textarea">回复@'+userTxt+':</textarea><div class="func"><div class="pullLeft"></div><input class="reply_topic_user_txt" type="text" /><button type="button" class="btn btnSm btn-blue replyTopic-btn">确定</button></div></div></div></li>');

    
}	

//回复评论
$('.feed-list').on('click','.replyTopic-btn',function()
{
	var replyUserTxt=$(this).prev().val();//回复者名字
	var replyTxt=$(this).parent().prev().val();//回复的内容
	var replyBox=$(this).parent().parent().next();
	//var oUl=replyBox.children('.feed-reply-list');//在此ul下生成新回
	var oUl=$('.feed-list');
	var commentBox=$(this).parents().filter('.reply-topic-box');//评论框
		
	if(replyUserTxt=='')
	{
		alert('请输入完整信息');
	}
	else
	{
		//生成新回复
		oUl.prepend('<li class="media"> <a class="pullLeft" href="" title="威廉姆叔"> <img src="/website/images/user/1.jpg" class="user-logo" /> </a><div class="mediaBody"><h4 class="mediaHeading"> <span class="uesrName">'+replyUserTxt+'</span> <span class="time">2013/12/30 20:18:56</span> </h4><p>'+replyTxt+'</p><div class="func"><div class="pullLeft"> </div><div class="reply"> <span>回复</span> <span>0</span> </div></div><div class="reply-topic-box" style="display:none;"><textarea class="reply_topic_textarea">回复@'+replyUserTxt+':</textarea><div class="func"><div class="pullLeft"></div><input class="reply_topic_user_txt" type="text" /><button type="button" class="btn btnSm btn-blue replyTopic-btn">确定</button></div></div></div></li>');
		
        $(commentBox).hide();//评论框消失
	};
})


//评论框
$('.feed-list').on('click','.reply',function(){
	
	
	var oBox=$(this).parent().next();

	if($(oBox).css('display')=='none')
	{
		$('.reply-topic-box').hide();
		$(oBox).show();
	}
	else
	{
		$('.reply-topic-box').hide();
	}
	
});

