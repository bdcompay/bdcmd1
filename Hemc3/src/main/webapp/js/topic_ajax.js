/*function InitAjax()
{
	var ajax=false;
	try
	{   
		ajax = new ActiveXObject("Msxml2.XMLHTTP");//
	}
	catch (e)
	{
		try
		{
			ajax = new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch (E)
		{
			ajax = false;
		}
	}

	if (!ajax && typeof XMLHttpRequest!='undefined') //XMLHttpRequest,后台与服务器交换数据,部分加载
	{
		ajax = new XMLHttpRequest();
	}
	return ajax;
}*/

function DoAjaxGet(ajax, url, func_succ)
{
	//ajax.open("GET", url, true);
	ajax.onreadystatechange = function()
	{
		if (ajax.readyState == 4 && ajax.status == 200) //State:(0: 请求未初始化;1: 服务器连接已建立;2: 请求已接收;3: 请求处理中;4: 请求已完成，且响应已就绪)
		{                                               //status:(200: "OK";404: 未找到页面)
			func_succ(ajax.responseText);
		}
		else
		{
			//alert("ajax faild readyState:"+ajax.readyState+" status:"+ajax.status);
		}
	};
	ajax.send(null);
}

function DoAjaxPost(ajax, url, func_succ, post_datas)
{
	ajax.open("POST", url, true);
	ajax.onreadystatechange = function()
	{
		if (ajax.readyState == 4 && ajax.status == 200)
		{
			func_succ(ajax.responseText); //获得来自服务器的响应,responseText获取形式为字符串;responseXML为xml形式
		}
		else
		{
			alert('ajax faild readyState:'+ajax.readyState+" status:"+ajax.status);
		}
	};
	ajax.setRequestHeader("Content-Type","application/x-www-form-urlencoded");
	ajax.send(post_datas);
}