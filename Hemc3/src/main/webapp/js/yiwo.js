/*一窝校园讲座*/

/*加入计划
__________________________________________*/
//获取屏幕中点
$(function() {    
     $(window).scroll(function(){    
        var point = getObjWh1( "fd" );    
        //$('#pos').html('x:'+point.x+',<br>y:'+point.y);  
        $('.planed').css( {left: point.x , top:point.y} );
		$('.planCancel').css( {left: point.x , top:point.y} ); 
		$('.tips-box').css( {left: point.x , top:point.y} ); 
    });    
   });     
     
   function getObjWh1(obj){    
      var point={};    
      var st=$(document).scrollTop();//滚动条距顶部的距离    
      var sl= $(document).scrollLeft();//滚动条距左边的距离    
      var ch=$(window).height();//屏幕的高度    
      var cw=$(window).width();//屏幕的宽度    
        
      var objH=$("."+obj).height();//浮动对象的高度    
      var objW=$("."+obj).width();//浮动对象的宽度    
  
      var objT=Number(st)+(Number(ch)-Number(objH))/2;    
      var objL=Number(sl)+(Number(cw)-Number(objW))/2;    
      point.x = objL ;    
      point.y = objT;      
      return point;    
       
}    
//加入计划
function plan(curr){
    
    if($(curr).hasClass('btn-blue'))
	{
		$('.planCancel').hide();
		$('.planed').show();
		setTimeout("hideTip()",1000);
		$(curr).removeClass('btn-blue');
		$(curr).addClass('btn-red');
		$(curr).html('取消计划');
	}
	else
	{
		$('.planed').hide();
		$('.planCancel').show();
		setTimeout("hideTip()",1000);
		$(curr).removeClass('btn-red');
		$(curr).addClass('btn-blue');
		$(curr).html('加入计划');
	}
};
//关闭
function hideTip(){
	$('.planed').hide();
	$('.planCancel').hide();
}
/*end*/


/*用户中心(右上角)
___________________________________*/
$(".userBox").hover(function(){
	$(".dropBox-menu").fadeIn();
}, function() {
	$(".dropBox-menu").hide();
});
/*end*/
