<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="common/common.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>一窝</title>
<meta name="distribution" content="global" />
<meta name="author" content="一窝" />
<meta name="publisher" content="一窝" />
<meta name="rating" content="general" />
<meta name="robots" content="all" />
<meta name="spiders" content="all" />
<meta name="webcrawlers" content="all" />
<meta name="company" content="一窝" />
<meta name="description" content="一窝" />
<meta name="keywords" content="一窝" />
<link rel="stylesheet" type="text/css" href="<%=basepath%>/css/fxb.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/jquery-ui/development-bundle/themes/base/jquery.ui.all.css">
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/upload/jquery.fileupload.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/upload/jquery.fileupload-ui.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/upload/blueimp-gallery.min.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/validate/jquery.validator.css" />
<script src="<%=basepath%>/js/jquery-1.10.2.min.js"
	type="text/javascript"></script>

<script
	src="<%=basepath%>/jQuery/jquery-ui/js/jquery-ui-1.10.3.custom.min.js"></script>
<script
	src="<%=basepath%>/jQuery/jquery-ui/development-bundle/ui/jquery.ui.core.js"></script>
<script
	src="<%=basepath%>/jQuery/jquery-ui/development-bundle/ui/jquery.ui.widget.js"></script>
<script
	src="<%=basepath%>/jQuery/jquery-ui/development-bundle/ui/jquery.ui.datepicker.js"></script>

<script src="http://api.map.baidu.com/api?v=1.4" type="text/javascript"></script>


<style>
.tips-box .tips-content {
	font-size: 16px;
	color: #666;
}

.tips-box .tips-content .jumpPage {
	font-size: 16px;
	vertical-align: baseline;
}
</style>
</head>
<body>
	<%@ include file="common/navbar.jsp"%>
	<div class="container">
		<div style="margin-top: 60px;">
			<form class="form-horizontal" id="eventForm" name="eventForm"
				method="post">
				<div class="content">
					<div class="pullLeft">
						<div class="control-group">
							<label class="control-label" for="inputImg">添加用户</label>
							<div class="controls">
								<noscript>
									<input type="hidden" name="redirect" value="">

								</noscript>
								<table role="presentation" class="table table-striped"
									style="margin-left: -4px;">
									<tbody class="files">
									</tbody>
								</table>

							</div>
						</div>
						<div class="control-group">
							<label class="control-label" for="name">用户名称<span
								class="ps">*</span></label>
							<div class="controls">
								<input type="text" id="name" name="name"
									class="input-xlarge step1" value="${userDto.username}" />
							</div>
						</div>
						<div class="control-group">
							<div class="buttonWrapper">
								<input name="submit" type="submit" id="submit"
									class="nextbutton btn btn-blue btnMd" value="提交" alt="提交"
									title="发布" />
							</div>
						</div>
					</div>
			</form>
		</div>
	</div>
	<div class="footer">
		<ul>
			<li><a href="http://dxcfxb.com/aboutus.jsp">关于我们</a></li>
			<li><a href="http://dxcfxb.com/newsroom.jsp">最新动态</a></li>
			<li><a href="http://dxcfxb.com/contact.jsp">联系我们</a></li>
			<li><a href="http://dxcfxb.com/jobs.jsp">加入我们</a></li>
		</ul>
		<span>www.dxcfxb.com 2014 © All Rights Reserved.</span>
	</div>
	<!--背景变暗-->
	<div class="fade black_overlay"></div>
	<div class="tips-box" style="margin-top: -100px;">
		<!--<span class="close">X</span>-->
		<div class="showOk" style="margin-top: 60px;">
			<img src="<%=basepath%>/images/ok.png" width="24" height="28">
			<span>提交成功<br /> <br /> <span class="tips-content">即将跳到<a
					href="managerUser.html">用户管理中心</a></span>
			</span>
		</div>
	</div>


	<!--图片浏览-->
	<div id="blueimp-gallery"
		class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
		<div class="slides"></div>
		<h3 class="title"></h3>
		<a class="prev">‹</a> <a class="next">›</a> <a class="close">×</a> <a
			class="play-pause"></a>
		<ol class="indicator">
		</ol>
	</div>
</body>
<script src="<%=basepath%>/js/yiwo.js"></script>

<script src="<%=basepath%>/jQuery/ueditor/ueditor.config.js"></script>
<script src="<%=basepath%>/jQuery/ueditor/ueditor.all.min.js"></script>
<script src="<%=basepath%>/jQuery/ueditor/lang/zh-cn/zh-cn.js"></script>

<script src="<%=basepath%>/jQuery/validate/jquery.validator.min.js"></script>
<script src="<%=basepath%>/jQuery/validate/zh_CN.js"></script>
<script type="text/javascript">
	var editor = UE.getEditor('editor1');
</script>
<!--upload files-->
<script src="<%=basepath%>/jQuery/upload/tmpl.min.js"></script>
<script src="<%=basepath%>/jQuery/upload/load-image.min.js"></script>
<script src="<%=basepath%>/jQuery/upload/canvas-to-blob.min.js"></script>
<script src="<%=basepath%>/jQuery/upload/blueimp-gallery.min.js"></script>
<script src="<%=basepath%>/jQuery/upload/jquery.fileupload.js"></script>
<script src="<%=basepath%>/jQuery/upload/jquery.fileupload-image.js"></script>
<script src="<%=basepath%>/jQuery/upload/jquery.fileupload-process.js"></script>
<script src="<%=basepath%>/jQuery/upload/jquery.fileupload-ui.js"></script>
<script src="<%=basepath%>/jQuery/upload/jquery.fileupload-validate.js"></script>
<script src="<%=basepath%>/jQuery/upload/jquery.iframe-transport.js"></script>
<script src="<%=basepath%>/jQuery/upload/jquery.ui.widget.js"></script>
<script src="<%=basepath%>/js/loadInputDate.js"></script>
<!--<script src="<%=basepath%>/jQuery/upload/main.js"></script>-->
<!--[if (gte IE 8)&(lt IE 10)]>
<script src="js/cors/jquery.xdr-transport.js"></script>
<![endif]-->

<script>
	$('#submit').click(function() {
		var name = $("#name").val();
		var jsondata = "name=" + name;
		
		$('#eventForm').validator({
			// 传入字段集合
			fields : {
				'name' : 'required;title',
			}
		}).on('valid.form', function(e, form) {
			$.ajax({
				url : "addUser.do",
				data : jsondata,
				type : "POST",
				dataType : "json",
				success : function(data) {
					if (data == true) {
						$('.tips-box').show();
						$('.fade').show();
						//跳到个人页
						$(function() {
							function jump(count) {
								window.setTimeout(function() {
									count--;
									if (count > 0) {
									} else {
										location.href = "managerUsers.html";
									}
								}, 1000);
							}
							jump(1);
						});
					}
				}
			});
		});
	});
</script>


</html>
