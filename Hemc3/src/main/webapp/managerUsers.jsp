<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="common/common.jsp"%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>用户管理</title>
<meta name="distribution" content="global" />
<meta name="author" content="一窝" />
<meta name="publisher" content="一窝" />
<meta name="rating" content="general" />
<meta name="robots" content="all" />
<meta name="spiders" content="all" />
<meta name="webcrawlers" content="all" />
<meta name="company" content="一窝" />
<meta name="description" content="一窝" />
<meta name="keywords" content="一窝" />
<link rel="stylesheet" type="text/css" href="<%=basepath%>/css/fxb.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/css/management.css" />
<script type="text/javascript"
	src="<%=basepath%>/js/jquery-1.10.2.min.js"></script>
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/datatables/demo_page.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/datatables/demo_table.css" />
<link rel="stylesheet" type="text/css"
	href="<%=basepath%>/jQuery/datatables/TableTools.css" />
<script type="text/javascript"
	src="<%=basepath%>/jQuery/datatables/jquery.dataTables.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="<%=basepath%>/jQuery/datatables/fnStandingRedraw.js"
	charset="utf-8"></script>
<script type="text/javascript"
	src="<%=basepath%>/jQuery/datatables/ZeroClipboard.js" charset="utf-8"></script>
<style type="text/css">
.btn-red {
	background: #F00;
}

.btn-yellow {
	background: #FF0;
}
</style>
</head>

<body>
	<%@ include file="common/navbar.jsp"%>
	<div class="cont">
		<div class="control-group">
			<a href="<%=basepath%>/addUser.html"><button
					class="btn btn-blue btnMd">新增用户</button></a>
		</div>
		<input type="text" id="eventId" name="eventId" value="${eventDto.id}"
			style="display: none;">
		<table cellpadding="0" cellspacing="0" border="0" class="display"
			id="man_event">
			<thead>
				<tr>
					<th>ID</th>
					<th>测试从配置文件拿属性值</th>
					<th>名称</th>
					<th>创建时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>

</body>
<script>
	var userImageTable = $('#man_event').dataTable(
			{
				"sDom" : 'T<"clear">lfrtip',
				"sPaginationType" : "full_numbers",
				"oLanguage" : {
					"sInfo" : "共 _TOTAL_ 条记录  (第_START_-_END_条)",
					"sLengthMenu" : "显示 _MENU_ 条记录/页",
					"sSearch" : "名称关键字搜索",
					"sZeroRecords" : "没有找到相关图像",
					"sInfoFiltered" : "",
					"sInfoEmpty" : "",
					"oPaginate" : {
						"sLast" : "尾页",
						"sFirst" : "首页",
						"sPrevious" : "上一页",
						"sNext" : "下一页",
					}
				},
				"bFilter" : true,
				"bAutoWidth" : false,
				"bProcessing" : false,
				"bLengthChange" : true,
				"bInfo" : true,//页脚信息
				"iDisplayLength" : 25,
				"sAjaxSource" : 'queryUsers.do',
				"bServerSide" : true,
				"fnServerData" : retData,// 自定义数据获取函数
				"aoColumns" : [
						{
							"sWidth" : "5%",
							"sClass" : "center",
							"mDataProp" : "id",
							"bSortable" : false
						},
						{
							"sWidth" : "15%",
							"sClass" : "center",
							"mDataProp" : "resourceExample",
							"bSortable" : false
						},
						{
							"sWidth" : "30%",
							"sClass" : "center",
							"mDataProp" : "username",
							"bSortable" : false
						},
						{
							"sWidth" : "10%",
							"sClass" : "center",
							"mDataProp" : "createTimeStr",
							"bSortable" : false
						},
						{
							"sWidth" : "15%",
							"sClass" : "center",
							"bSortable" : false,
							"mDataProp" : "managerHtml",
							"fnRender" : function(obj) {
								var id = obj.aData['id'];
								return "<a href=\"javascript:deleteUser(" + id
										+ ")\">彻底删除</a>&nbsp"
										+ "<a href=\"editUser.html?userId="
										+ id + "\">编辑</a>";
							}
						} ]
			});
	userImageTable.css("font-size", "14px");//设置文本的样式；

	//自定义数据获取函数
	function retData(sSource, aoData, fnCallback) {
		$.ajax({
			"type" : "GET",
			"url" : sSource,
			"dataType" : "json",
			"data" : aoData,
			"success" : function(resp) {
				fnCallback(resp);
			},
			"error" : function(resp) {
			}
		});
	}
	function deleteUser(id) {
		if (window.confirm('你确定要彻底删除该用户？')) {
			$.ajax({
				"type" : "POST",
				"url" : "deleteUser.do",
				"dataType" : "json",
				"data" : "userId=" + id,
				"success" : function(resp) {
					if (resp == true) {
						alert("成功删除用户");
						userImageTable.fnStandingRedraw();
					}
				},
				"error" : function(resp) {
				}
			});
		}

	}
</script>


</html>
