﻿   
        jQuery('.portlet .tools a.remove').click(function () {
            var removable = jQuery(this).parents(".portlet");
            if (removable.next().hasClass('portlet') || removable.prev().hasClass('portlet')) {
                jQuery(this).parents(".portlet").remove();
            } else {
                jQuery(this).parents(".portlet").parent().remove();
            }
        });
		
		// 改变com口状态
		jQuery('.portlet .actions a.change').click(function () {
            var removable = jQuery(this).parents(".portlet");
			var content = jQuery(this).text();
			
			if(content=="开启"){
				removable.removeClass();
				removable.addClass("portlet box green");
				jQuery(this).text("关闭");
			}else{
				removable.removeClass();
				removable.addClass("portlet box grey");
				jQuery(this).text("开启");
			}
        });
		
		//展开全部com主体
		jQuery('button.expand').click(function () {
			var el = jQuery(".portlet-body");
			jQuery('.portlet .tools .collapse, .portlet .tools .expand').removeClass("expand").addClass("collapse");
            el.slideDown(200);
			
            
        });
		
		//收缩全部com主体
		jQuery('button.shrink').click(function () {
			var el = jQuery(".portlet-body");
			jQuery('.portlet .tools .collapse, .portlet .tools .expand').removeClass("collapse").addClass("expand");
            el.slideUp(200);
        });
		
		//开启全部com口
		jQuery('button.startup').click(function () {
			var portlet = jQuery(".portlet");
			portlet.removeClass();
			portlet.addClass("portlet box green");
			jQuery('.portlet .actions a.change').text("关闭");
        });
		
		//关闭全部com口
		jQuery('button.closeall').click(function () {
			var portlet = jQuery(".portlet");
			portlet.removeClass();
			portlet.addClass("portlet box grey");
			jQuery('.portlet .actions a.change').text("开启");
        });
		
		//复选框
		jQuery('#comboxall').click(function(){
			if(jQuery(this).attr("checked")){
				jQuery(this).attr("disabled",true);
				jQuery('input.checkboxes').each(function() {  
					if($(this).attr("name")=='combox'){
						$(this).attr("checked",true);
					}
				});
				jQuery.uniform.update('input.checkboxes');
			}
		});
		
		//name=combox的一组checkbox是否全选
		jQuery('input.checkboxes').click(function(){
			var num = true;
			jQuery('input.checkboxes').each(function() {  
				if($(this).attr("name")=='combox'){
					if(!$(this).attr("checked")){
						num =false;
					}
				}
			});
			if(num){
				jQuery('#comboxall').attr("checked",true);
				jQuery('#comboxall').attr("disabled",true);
			}else{
				jQuery('#comboxall').attr("checked",false);
				jQuery('#comboxall').attr("disabled",false);
			}
			jQuery.uniform.update('input.checkboxes');
		});

        jQuery('.portlet .tools a.reload').click(function () {
            var el = jQuery(this).parents(".portlet");
            App.blockUI(el);
            window.setTimeout(function () {
                App.unblockUI(el);
            }, 1000);
        });

        jQuery('.portlet .tools .collapse, .portlet .tools .expand').click(function () {
            var el = jQuery(this).parents(".portlet").children(".portlet-body");
            if (jQuery(this).hasClass("collapse")) {
                jQuery(this).removeClass("collapse").addClass("expand");
                el.slideUp(200);
            } else {
                jQuery(this).removeClass("expand").addClass("collapse");
                el.slideDown(200);
            }
        });

        /*
        sample code to handle portlet config popup on close
        $('#portlet-config').on('hide', function (e) {
            //alert(1);
            //if (!data) return e.preventDefault() // stops modal from being shown
        });
        */
    
