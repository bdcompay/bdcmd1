﻿/**
 * 通道接口机
 * 
 * 
 * 
 */

	//页面初始加载
	createAllCombox('/bdcmd1/queryCards.do');
	
		//双击挑战到详细页面
		jQuery('.portlet .portlet-title').die().live(
			"dblclick", function () { 
			var portlet = jQuery(this).parents(".portlet");
			var comname = portlet.find("h4").children("span").text();  //com名称
			window.location.href="/bdcmd1/comdetail.do?comName="+comname;
		});
			
		// 改变com口状态
		jQuery('.portlet .actions a.change').die().live("click",function () {
			var portlet = jQuery(this).parents(".portlet");
			var comname = portlet.find("h4").children("span").text();  //com名称
			var insertPoint = portlet.parent();
			
			var content = jQuery(this).text();
			if(content=="开启"){  
				//开启com
				var uri = '/bdcmd1/startup.do';
				startcom(uri,comname);
			}else{   
				//关闭com
				var uri = '/bdcmd1/closecom.do';
				createCombox(uri,comname,insertPoint);
			}
		});
		
		//更新单个数据
		jQuery('.portlet .tools a.reload').die().live("click",function () {
			var el = jQuery(this).parents(".portlet");
			var portlet = el;
			var comname = portlet.find("h4").children("span").text();  //com名称
			var insertPoint = portlet.parent();
			
			//更新com
			var uri = '/bdcmd1/findcom.do';
			createCombox(uri,comname,insertPoint);
			
			App.blockUI(el);
			window.setTimeout(function () {
				App.unblockUI(el);
			}, 1000);
		});
		
		
		//开启全部com口
		jQuery('button.startup').die().live("click",function () {
			if(confirm("你确定要开启全部com吗？")){
				var uri = '/bdcmd1/startupAll.do';
				updateAll(uri);
			}
			
	    });
		
		//关闭全部com口
		jQuery('button.closeall').die().live("click",function () {
			if(confirm("你确定要关闭全部com吗？")){
				var uri = '/bdcmd1/closeAll.do';
				updateAll(uri);
			}
	    });
		
	//更新全部com
	function updateAll(uri,comnames){
		var coms = jQuery('.portlet').find("h4").children("span");
		var comnames=[];
		for(var i=0;i<coms.length;i++){
			comnames[i]=coms[i].innerHTML;
		};
		
		$.ajax({     
			url:uri,     
			type:'get',     
			data:'comNames='+comnames, 
			dataType:'json',
			async : true, //默认为true 异步     
			error:function(){
				alert("error");
			},
			success:function(data){     
				createAllCombox('/bdcmd1/queryCards.do');
			}  
		});	 
	}
		
		

	//创建全部combox
	function createAllCombox(uri){
		$.ajax({     
			url:uri,     
			type:'get',     
			//data:'comName='+values, 
			dataType:'json',
			async : true, //默认为true 异步     
			error:function(){
				alert("error");
			},
			success:function(data){     
				//console.log(data);
				var comobjects=data.aaData;
				var boxs = jQuery('.portlet').find("h4").children("span");
				if(boxs.length==0){
					var comlist = $("#comlist");
					comlist.append(get_htmlstr(comobjects));
					App.init();
				}else{
					for(var i=0;i<comobjects.length;i++){
						setcombycomname(comobjects[i]);
						
					}
				}

				//5秒刷新一次页面数据
				window.setTimeout("createAllCombox('/bdcmd1/queryCards.do')",5000);
			}  
		});	 
	}
	
	//创建单个combox
	function createCombox(uri,comname,insertPoint){
		$.ajax({     
			url:uri,     
			type:'get',     
			data:'comName='+comname, 
			dataType:'json',
			async : true, //默认为true 异步     
			error:function(){
				alert("error");
			},
			success:function(data){   
				//console.log(data);
				setcombycomname(data);
				App.init();
				
			}  
		});
	}
	
	//开启com
	function startcom(uri,comname){
		$.ajax({     
			url:uri,     
			type:'get',     
			data:'comName='+comname, 
			dataType:'',
			async : true, //默认为true 异步     
			error:function(){
				alert("error");
			},
			success:function(data){  
				if(data.message!=null){
					alert(data);
				}
			}  
		});
	}

	 //创建com盒子
	 function get_combox(com){ 
	    var lastGlTime = com.lastGlTime;		//功率最后更新时间
		var glFreq = com.glFreq;					//功率频率	
		var comPort = com.comPort;					//com端口号
		var comname = com.com;				//com名称
		var deviceType = com.deviceType; 		//协议类型
		var serverCard = com.serverCard;			//服务卡号
		var freq = com.freq;				//频率
		var busyfreq = com.busyfreq;				//剩余忙碌时间(倒计时)
		var openStatus = com.openStatus;				//开启状态
		var status = com.status;				//状态
		var sentCount =	com.sentCount;		//发送数量
		var revCount = 	com.revCount;				//接收数量
		var gleast = com.gleast;		  				//东星
		var glwest = com.glwest;						//西星
		var glback = com.glback;						//备用星
		var open = com.open;				//是否打开
		var normal = com.normal;					//是否正常
		var busy = com.busy;					//是否忙碌
		var apply = com.apply;				//是否正在申请数据
		var channelGroupName = com.channelGroupName;	//所属组名
		var rdGroupCell = com.rdGroupCell;				//绑定的单元名
		var bindGroup = com.bindGroup;				//是否绑定组单元
		var alias = "没绑定圆盘";
		if(bindGroup){
			alias = "圆盘"+channelGroupName+"单元"+rdGroupCell
		}
		
		
		 
		
		var combox = "<div id="+comname+" class=\"portlet  box "+gat_com_status(open,normal,busy)+"\">"+
				"<div class=\"portlet-title\"  >"+
					"<h4><i class=\"icon-reorder\"></i><span>"+alias+"</span></h4>"+
					"<div class=\"tools\" style='width:100px;margin-left:4px;'>"+
						"<img style=\"margin-top:-1px;width:20px;height:20px; float:left;\" class=\"gleast\" src=\""+get_signal(gleast)+"\"  />"+
						"<img style=\"margin-top:-2px;width:20px;height:20px;float:left;\" class=\"glwest\" src=\""+get_signal(glwest)+"\" />"+
						"<img style=\"margin-top:-2px;width:20px;height:20px;float:left;\" class=\"glback\" src=\""+get_signal(glback)+"\" />"+
						"<a href=\"javascript:;\" class=\"collapse\"></a>"+
						"<a href=\"javascript:;\" class=\"reload\"></a>"+
					"</div>"+
					"<div class=\"actions\">"+
							"<a href=\"javascript:;\" style=\"margin-top:-7px;\" class=\"btn blue mini change\" >"+gat_btn_status(!open)+"</a>"+
					"</div>"+
				"</div>"+
				"<div class=\"portlet-body\">"+
					"<div class=\"scroller\" data-height=\"220px\" data-always-visible=\"1\">"+
						//"<strong>ID:</strong><b>"+ id +"</b><br />"+
						"<strong>COM端口:</strong><b class=\"comname\">"+ comname +"</b><br />"+
						"<strong>串口状态:</strong><b class=\"isopen\">"+ gat_btn_status(open)+"</b><br />"+
						"<strong>收发状态:</strong>" +
								"<select style='width: 96px;height: 26px;' id='apply"+comname+"' name='"+comname+"' onchange='applyChange(this)'>"+
									"<option value='1' "+getApplySeleted(true,apply)+">且收且发</option>"+
									 "<option value='0' "+getApplySeleted(false,apply)+">只收不发</option>"+
								"</select><br />"+
						//"<strong>信号:</strong><b>"+signal+"</b><br />"+
						"<strong>倒计时:</strong><b class=\"busyfreq\">"+busyfreq+"</b><br />"+
						"<strong>运行状态:</strong><b class=\"status\">"+status+"</b><br />"+
						"<strong>服务卡：</strong><b class=\"serverCard\">"+serverCard+"</b><br />"+
						"<strong>频度：</strong><b class=\"freq\">"+freq+"</b><br />"+
						"<strong>设备协议类型：</strong><b class=\"deviceType\">"+deviceType+"</b><br />"+
						//"<strong>卡类型：</strong><b>"+card_type+"</b><br />"+
						//"<strong>东星：</strong><b>"+gleast+"</b><br />"+
						//"<strong>西星：</strong><b>"+glwest+"</b><br />"+
						//"<strong>备用星：</strong><b>"+glback+"</b><br />"+
						"<strong>发送数量：</strong><b class=\"sentCount\">"+sentCount+"</b><br />"+
						"<strong>接收数量：</strong><b class=\"revCount\">"+revCount+"</b><br />"+
						"<strong>功率最后更新时间:</strong><br /><b class=\"lastGlTime\">&nbsp;&nbsp;"+ convertTime(lastGlTime)+"</b><br />"+
					"</div>"+
				"</div>"+
		"</div>";
		
		return combox;
	}
	 
	 function setcombycomname(com){
		 //console.log(com);
		//获得com数据
		var lastGlTime = com.lastGlTime;		//功率最后更新时间
		var glFreq = com.glFreq;					//功率频率	
		var comPort = com.comPort;					//com端口号
		var comname = com.com;				//com名称
		var deviceType = com.deviceType; 		//协议类型
		var serverCard = com.serverCard;			//服务卡号
		var freq = com.freq;				//频率
		var busyfreq = com.busyfreq;				//剩余忙碌时间(倒计时)
		var openStatus = com.openStatus;				//开启状态
		var status = com.status;				//状态
		var sentCount =	com.sentCount;		//发送数量
		var revCount = 	com.revCount;				//接收数量
		var gleast = com.gleast;		  				//东星
		var glwest = com.glwest;						//西星
		var glback = com.glback;						//备用星
		var open = com.open;				//是否打开
		var normal = com.normal;					//是否正常
		var busy = com.busy;					//是否忙碌
		var apply = com.apply;				//是否正在申请数据
		var channelGroupName = com.channelGroupName;	//所属组名
		var rdGroupCell = com.rdGroupCell;				//绑定的单元名
		var bindGroup = com.bindGroup;				//是否绑定组单元
		var alias = "没绑定圆盘";
		if(bindGroup){
			alias = "圆盘"+channelGroupName+"单元"+rdGroupCell
		}
		
		
		//获得对象
		var portlet = $("#"+comname);         //combox对象
		var cname = portlet.find("h4").children("span"); //标题名称
		var gleastsignal = portlet.find(".gleast");     //东星
		var glwestsignal = portlet.find(".glwest");		//西星
		var glbacksignal = portlet.find(".glback");     //备用星
		var butstatus = portlet.find(".actions").children(".change");    //开关按钮
		var box_isopen = portlet.find(".scroller").find(".isopen"); 		//串口状态
		var box_busyfreq = portlet.find(".scroller").find(".busyfreq");  		//倒计时
		var box_status = portlet.find(".scroller").find(".status");  			//运行状态
		var box_serverCard = portlet.find(".scroller").find(".serverCard");   		//服务卡
		var box_freq = portlet.find(".scroller").find(".freq");   		//频度
		var box_deviceType = portlet.find(".scroller").find(".deviceType");   		//设备协议类型
		var box_sentCount = portlet.find(".scroller").find(".sentCount");   		//发送数量
		var box_revCount = portlet.find(".scroller").find(".revCount");          	//接收数量
		var box_lastGlTime = portlet.find(".scroller").find(".lastGlTime");          	//功率最后更新时间
		
		//设置数据
		portlet.removeClass();
		portlet.addClass("portlet box "+gat_com_status(open,normal,busy));
		cname.text(alias);
		gleastsignal.attr("src",get_signal(gleast));
		glwestsignal.attr("src",get_signal(glwest));
		glbacksignal.attr("src",get_signal(glback));
		butstatus.text(gat_btn_status(!open));
		box_isopen.text(gat_btn_status(open));
		box_busyfreq.text(busyfreq);
		box_status.text(status);
		box_serverCard.text(serverCard);
		box_freq.text(freq);
		box_deviceType.text(deviceType);
		box_sentCount.text(sentCount);
		box_revCount.text(revCount);
		box_lastGlTime.text(convertTime(lastGlTime));
	 }
	 
	 //com运行状态是否正常
	 function isnormal(normal){
		 if(normal==true){
			 return "正常";
		 }else{
			 return "故障";
		 }
	 }
	 
	 //将时间戳格式转换为日期格式
	 function convertTime(datetime){
		 var date = new Date(datetime);
		 Y = date.getFullYear() + '-';
		 M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
		 D = date.getDate() + ' ';
		 h = date.getHours() + ':';
		 m = date.getMinutes() + ':';
		 s = date.getSeconds(); 
		 return Y+M+D+h+m+s;
		 console.log(Y+M+D+h+m+s);
	 }
	 
	//获得要增加的html代码
	function get_htmlstr(comobjects){
		//row起始标签
		 var rowbegin ="<div class=\"row-fluid combox\">";
		 //div结束标签
		 var divend ="</div>";
		 
		 var colbegin = "<div class=\"span3 \" style=\"min-width:290px;\">";
		
		var htmlstr ="";
		for(var i=0;i<comobjects.length;i++){
			//添加row起始标签
			if(i%4==0){ htmlstr+=rowbegin; }
			
			var size = 0;
			//添加col HTML代码
			var n = 3;	   //row里创建n+1个com框
			if(comobjects.length-i<4){ n = comobjects.length-i-1;}
			for(var j=i;j<i+n+1;j++){
				
				htmlstr+=colbegin;
				htmlstr+=get_combox(comobjects[j]);
				htmlstr+=divend;
				size = j;
			}
			i=size;  //当前创建了com框的个数
			
			//添加row结束标签
			if((i+1)%4==0){ htmlstr+=divend;}
			if((comobjects.length+1)%4!=0){
				htmlstr+=divend;
			}
		}
		if((comobjects.length+1)%4!=0){
			htmlstr+=divend;
		}
		return htmlstr;
	}
	
	//com状态
	function gat_com_status(comstatusv,normal,isBusy){
		if(comstatusv){
			if(normal){
				if(isBusy){
					 return "yellow";
				}else{
					 return "green";
				}
			}else{
				 return "red";
			}
		}else{
			return "grey";
		}
		
	/*	if(comstatusv==0){
			return "grey";
		}else if(comstatusv==1){
		   return "green";
		}else if(comstatusv==2){
			return "red";
		}*/
	}			
	
	//信号图标
	function get_signal(signalv){
		switch(signalv){
			case 1: return "assets/img/signal/stat_sys_signal_1.png"; break;
			case 2: return "assets/img/signal/stat_sys_signal_2.png"; break;
			case 3: return "assets/img/signal/stat_sys_signal_3.png"; break;
			case 4: return "assets/img/signal/stat_sys_signal_4.png"; break;
			default: return "assets/img/signal/stat_sys_signal_null.png"; break;
		}
	}

	//按钮状态
	function  gat_btn_status(statusv){
		if(statusv==true){
			return "开启";
		}else{
			return "关闭";
		}
	}
	
	//收发是否被选中
	function getApplySeleted(thisapply,isapply){
		if(thisapply==isapply){
			return "selected='selected'";
		}else{
			return "";
		}
	}
	
    //收发状态改变
	function applyChange(select){
		var $select=$(select,this);
		var comname=$select[0].name;
		var selectValue=$select[0].value;
		$.ajax({
			
			url:"controlSendRecieve.do?comName="+comname+"&sendStatus="+selectValue,     
			type:'get',     
//			data:'{comName:'+comname+',sendStatus:'+selectValue+'}', 
			dataType:'json',
			async : true, //默认为true 异步     
			error:function(){
				alert("error");
			},
			success:function(data){   
				//console.log(data);
				if(data.apply==true){
					$select.val(1);
				}else{
					$select.val(0);
				}
			}
		});
	}
				
				
				
				
				
				
				
				
				
