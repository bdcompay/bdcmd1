<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>RD通道(COM)管理-RD通道机</title>
	<jsp:include page="common/loadcss.jsp"></jsp:include>
</head>
<!-- END HEAD -->


<!-- BEGIN BODY -->
<body class="fixed-top">
	<jsp:include page="common/top.jsp"></jsp:include>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<jsp:include page="common/leftnavbar.jsp"></jsp:include>
		
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid" id="comlist">
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">		
						<ul class="breadcrumb">
							<li>
								<a href="index.html">RD通道机</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li style="margin-left:1px;">
								<a href="javascript:;">RD通道(COM)管理</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				<!-- BEGIN PAGE FUNCTION　BUTTON -->
				<div class="row-fluid breadcrumb" style="margin-top:-15px;" >
					<div class="span12">
						<div class="dashboard" style="margin-top:0px;margin-right:10px;">
							<button type="button" class="btn btn-primary green expand" style="float:right;margin-right:10px;margin-top:5px;">展开全部</button>
							<button type="button" class="btn btn-primary green shrink" style="float:right;margin-right:10px;margin-top:5px;">收缩全部</button>		
							<button type="button" class="btn btn-primary green closeall" style="float:right;margin-right:10px;margin-top:5px;">关闭全部com</button>	
							<button type="button" class="btn btn-primary green startup" style="float:right;margin-right:10px;margin-top:5px;">开启全部com</button>		
						</div>
					</div>
				</div>
				<!-- END PAGE FUNCTION BUTTON -->
				
				<!-- BEGIN PAGE BODY -->
				<!-- END PAGE BODY -->
			</div>
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	
	<!-- BEGIN POPUP -->
	<!-- END POPUP -->
	
	<!-- BEGIN FOOTER -->
	<jsp:include page="common/footer.jsp"></jsp:include>
	<!-- END FOOTER -->
	
	<!-- BEGIN SCRIPT -->
	<script type="text/javascript" src="${proName}/js/pagejs/manager-comlist.js"></script>
	<script type="text/javascript">	
		$('#manager-comlist').addClass("active");
	</script>
	<!-- END SCRIPT -->	
</body>
<!-- END BODY -->
</html>