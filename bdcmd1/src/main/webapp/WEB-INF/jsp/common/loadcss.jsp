<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	<jsp:include page="common.jsp"></jsp:include>
	<meta content="width=device-width, initial-scale=1.0" name="viewport" />
	<meta name="author" content="JLJ" />
	<link href="${proName}/assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
	<link href="${proName}/assets/css/metro.css" rel="stylesheet" />
	<link href="${proName}/assets/bootstrap/css/bootstrap-responsive.min.css" rel="stylesheet" />
	<link href="${proName}/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
	<link href="${proName}/assets/css/style.css" rel="stylesheet" />
	<link href="${proName}/assets/css/style_responsive.css" rel="stylesheet" />
	<link href="${proName}/assets/css/style_light.css" rel="stylesheet" id="style_color" rel="stylesheet" type="text/css"  />
	<link href="${proName}/assets/uniform/css/uniform.default.css" />
	<link href="${proName}/css/chosen.css" type="text/css" rel="stylesheet" />
	<link href="${proName}/css/demo_table.css" type="text/css" rel="stylesheet" />
	<link href="${proName}/assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
	<link href="${proName}/assets/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"  />
	<link href="${proName}/assets/chosen-bootstrap/chosen/chosen.css"  rel="stylesheet" type="text/css" />
	<link href="${proName}/assets/data-tables/DT_bootstrap.css" rel="stylesheet" type="text/css" />
	<link href="${proName}/css/popup.css" type="text/css" rel="stylesheet" type="text/css"  />
	<link rel="shortcut icon" href="favicon.ico" />