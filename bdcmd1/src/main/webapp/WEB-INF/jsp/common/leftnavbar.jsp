<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
		<!-- BEGIN SIDEBAR -->
		<div class="page-sidebar nav-collapse collapse">
			<!-- BEGIN SIDEBAR MENU -->        	
			<ul>
				<li>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
					<div class="sidebar-toggler hidden-phone"></div>
					<!-- BEGIN SIDEBAR TOGGLER BUTTON -->
				</li>
				<li class="has-sub" id="manager-comlist">
					<a href="index.html">
						<i class="icon-home"></i> 
						<span class="title">COM管理</span>
						<span class="selected"></span>
					</a>
				</li>
				<li class="has-sub" id="manager-group">
					<a href="manager-group.html" >
						<i class="icon-home"></i> 
						<span class="title">RD通道组管理</span>
						<span class="selected"></span>
					</a>
				</li>
			</ul>
			<!-- END SIDEBAR MENU -->
		</div>
		<!-- END SIDEBAR -->