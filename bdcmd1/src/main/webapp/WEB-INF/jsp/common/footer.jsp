<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <jsp:include page="common.jsp"></jsp:include>
	<div class="footer">
		2015 &copy; 广州祺智通信科技股份有限公司.
		<div class="span pull-right">
			<span class="go-top"><i class="icon-angle-up"></i></span>
		</div>
	</div>
	
	<!-- Load javascripts at bottom, this will reduce page load time -->
	<script src="${proName}/assets/js/jquery-1.8.3.min.js"></script>
	<script src="${proName}/assets/breakpoints/breakpoints.js"></script>
	<script src="${proName}/assets/jquery-ui/jquery-ui-1.10.1.custom.min.js"></script>	
	<script src="${proName}/assets/jquery-slimscroll/jquery.slimscroll.min.js"></script>
	<script src="${proName}/assets/bootstrap/js/bootstrap.min.js"></script>
	<script src="${proName}/assets/js/jquery.blockui.js"></script>
	<script src="${proName}/assets/js/jquery.cookie.js"></script>
	<!--script src="assets/js/stylechange.js"></script-->
	<!-- ie8 fixes -->
	<!--[if lt IE 9]>
	<script src="assets/js/excanvas.js"></script>
	<script src="assets/js/respond.js"></script>
	<![endif]-->
	<script type="text/javascript" src="${proName}/assets/uniform/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="${proName}/assets/data-tables/jquery.dataTables.js"></script>
	<script type="text/javascript" src="${proName}/assets/data-tables/DT_bootstrap.js"></script>
	<script type="text/javascript" src="${proName}/js/topnav.bak.js"></script>
	<script type="text/javascript" src="${proName}/js/jquery.chosen.js"></script>
	<script src="${proName}/assets/js/app.js"></script>	
	<script type="text/javascript">
		jQuery(document).ready(function() {		
			App.init();
		});
	</script>