<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>RD通道组管理-RD通道机</title>
	<jsp:include page="common/loadcss.jsp"></jsp:include>
</head>
<!-- END HEAD -->


<!-- BEGIN BODY -->
<body class="fixed-top">
	<jsp:include page="common/top.jsp"></jsp:include>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<jsp:include page="common/leftnavbar.jsp"></jsp:include>
		
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid" >
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">		
						<ul class="breadcrumb">
							<li>
								<a href="index.html">RD通道机</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li style="margin-left:1px;">
								<a href="javascript:;">RD通道组管理</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				<!-- BEGIN PAGE FUNCTION　BUTTON -->
				<div class="row-fluid breadcrumb" style="margin-top:-15px;" >
					<div class="span6"> 
						<div style="margin-bottom:10px;margin-top:0px;" class="toola">
							<button type="button" class="btn btn-primary green addrdgroup">添加</button>		 
						</div>
					</div>
				</div>
				<!-- END PAGE FUNCTION BUTTON -->
				
				<!-- BEGIN PAGE TABLE -->
				<div id="grouptable">
					<table class="grouptable table table-striped table-bordered bootstrap-datatable">
						<thead>
							<th>组别</th>
							<th>单元01</th>
							<th>单元02</th>
							<th>单元03</th>
							<th>单元04</th>
							<th>单元05</th>
							<th>单元06</th>
							<th>单元07</th>
							<th>单元08</th>
							<th>操作</th>
						</thead>
						<tbody>
	
						</tbody>
					</table>
				</div>
				<!-- END PAGE TABLE -->
			</div>
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	
	<!-- BEGIN POPUP -->
	<jsp:include page="popup/manager-group-popup.jsp"></jsp:include>
	<!-- END POPUP -->
	
	<!-- BEGIN FOOTER -->
	<jsp:include page="common/footer.jsp"></jsp:include>
	<!-- END FOOTER -->
	
	<!-- BEGIN SCRIPT -->
	<script type="text/javascript" src="${proName}/js/pagejs/manager-grouprd.js"></script>
	<script type="text/javascript">	
		$('#manager-group').addClass("active");
	</script>
	<!-- END SCRIPT -->	
</body>
<!-- END BODY -->
</html>