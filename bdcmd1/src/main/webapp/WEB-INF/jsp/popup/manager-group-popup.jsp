<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
	<!-- BEGIN SAVE RDGROUP POPUP -->
	<form id="pop" runat="server">
	<div id="saverdgroup" class="popdiv" style="display:none;">
    	<!-- BEGIN POPUP HEAD -->  
  		<div class="pophead">
    		<div class="popheadtitle"><b>增加RD组</b></div>
    		<a href="javascript:void(0);"><div class="pop-hide"></div></a>
    	</div> 
    	<!-- END POPUP HEAD -->
    	
    	<!-- BEGIN POPUP BODY -->
        <div class="popbody" style="height:150px;">
   			<div class="control-group">
				<label class="control-label" >RD组名称</label>
				<div class="controls">
					<span class="message"><input type="text"  name="rdgroupname" id="rdgroupname" /></span>
					<span class="errerMessage" id="rdgroupnameError"></span>
				</div>
		    </div>
		    <div class="control-group">
				<label class="control-label" >发射频度(毫秒)</label>
				<div class="controls">
					<span class="message"><input type="text"  name="savesendfreq" id="savesendfreq" /></span>
					<span class="errerMessage" id="savesendfreqError"></span>
				</div>
		    </div>
        </div>
        <!-- END POPUP BODY -->
        
        <!-- BEGIN POPUP FLOOR -->
		<div class="popfloor">
			<label class="popfloortitle"></label>
			<button class="btn blue saverdgropbtn" type="button">提交</button>
          	<button class="btn" type="reset">重置</button>
		</div>
		<!-- END POPUP FLOOR -->
	</div></form> 
	
	<form id="pop2" runat="server">
	<div id="updaterdgroup" class="popdiv" style="display:none;">
    	<!-- BEGIN POPUP HEAD -->  
  		<div class="pophead">
    		<div class="popheadtitle"><b>修改RD组名称</b></div>
    		<a href="javascript:void(0);"><div class="pop-hide"></div></a>
    	</div> 
    	<!-- END POPUP HEAD -->
    	
    	<!-- BEGIN POPUP BODY -->
        <div class="popbody" style="height:150px;">
   			<div class="control-group">
				<label class="control-label" >RD组名称</label>
				<div class="controls">
					<span class="message"><input type="text"  name="updrdgroupname" id="updrdgroupname" /></span>
					<span class="errerMessage" id="updrdgroupnameError"></span>
				</div>
		    </div>
		    <div class="control-group">
				<label class="control-label" >发射频度(毫秒)</label>
				<div class="controls">
					<span class="message"><input type="text"  name="updatesendfreq" id="updatesendfreq" /></span>
					<span class="errerMessage" id="updatesendfreqError"></span>
				</div>
		    </div>
        </div>
        <!-- END POPUP BODY -->
        
        <!-- BEGIN POPUP FLOOR -->
		<div class="popfloor">
			<label class="popfloortitle"></label>
			<button class="btn blue updrdgropbtn" type="button">提交</button>
          	<button class="btn" type="reset">重置</button>
		</div>
		<!-- END POPUP FLOOR -->
	</div></form> 
	<!-- END SAVE RDGROUP POPUP -->	