<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
<!-- BEGIN HEAD -->
<head>
	<meta charset="utf-8" />
	<title>RD通道（COM）详细-RD通道（COM）管理-RD通道机</title>
	<jsp:include page="common/loadcss.jsp"></jsp:include>
	<style>
		.toola .badge {
			background-color: #e02222;
			-webkit-border-radius: 12px !important;
			-moz-border-radius: 12px !important;
		}
		#actons{
			margin-left:10px;
			width:175px; 
			height:50px;
			float:right;
		}
		
		#comstatus{
			min-width:200px;
			margin-top:10px;
			float:left;
		}
		
		#comstatus .commessage{
			overflow:auto;
			min-width:115px;
			margin-left:10px;
			border:#000 solid 1px;
			margin-bottom:1px;
			float:left;
			word-wrap:break-word;
			height:22px;
		}
		#comstatus .commessage strong{
			float:left;
		}
	</style>
</head>
<!-- END HEAD -->


<!-- BEGIN BODY -->
<body class="fixed-top">
	<jsp:include page="common/top.jsp"></jsp:include>

	<!-- BEGIN CONTAINER -->
	<div class="page-container row-fluid">
		<jsp:include page="common/leftnavbar.jsp"></jsp:include>
		
		<!-- BEGIN PAGE -->
		<div class="page-content">
			<div class="container-fluid" >
				<!-- BEGIN PAGE HEADER-->
				<div class="row-fluid">
					<div class="span12">		
						<ul class="breadcrumb">
							<li>
								<a href="index.html">RD通道机</a> 
								<i class="icon-angle-right"></i>
							</li>
							<li style="margin-left:1px;">
								<a href="index.html">RD通道（COM）管理</a>
								<i class="icon-angle-right"></i>
							</li>
							<li style="margin-left:1px;">
								<a href="javascript:;">RD通道（COM）详细</a>
							</li>
						</ul>
					</div>
				</div>
				<!-- END PAGE HEADER-->
				
				<!-- BEGIN PAGE FUNCTION　BUTTON & COM STATUS -->
				<div class="row-fluid ">
					<div class="span12" style="margin-top:-15px;"> 
						<div id="comstatus" style="background-color:#f0f0f0">
							<div id="actons"class="toola">
								<button type="button" class="btn btn-primary green startacom">开启com</button>
								<button type="button" class="btn btn-primary green clearall">清除全部</button>
							</div>
						
							<div class="commessage"><strong>服务卡：</strong><span id="serverCard"></span></div>	
							<!-- <div class="commessage" ><strong>ID:</strong><p>1</p></div>	 -->
							<!-- <div class="commessage" ><strong>串口状态:</strong><p id="isopen"></p></div> -->
							<div class="commessage" ><strong>信号:</strong>
								<img class="gleast" src="" style="width:20px;height:20px;" width="20px; height:28px;"  />
								<img class="glwest" src="" style="width:20px;height:20px;" width="20px; height:28px;"  />
								<img class="glback" src="" style="width:20px;height:20px;" width="20px; height:28px;"  />
							</div>
							<div class="commessage" ><strong>倒计时:</strong><span id="countdown"></span></div>	
							<!-- <div class="commessage" ><strong>是否正常:</strong><p id="normal"></p></div> -->
							<div class="commessage"><strong>频度:</strong><span id="freq"></span></div>	
							<!-- <div class="commessage"><strong>设备协议类型：</strong><p id="deviceType"></p></div> -->
							<!-- <div class="commessage"><strong>卡类型：</strong><p id="">60秒卡</p></div> -->
							<div class="commessage"><strong>发送数量：</strong><span id="sentCount"></span></div>
							<div class="commessage"><strong>接收数量：</strong><span id="revCount"></span></div>
							<div class="commessage"><strong>运行状态:</strong><span id="status"></span></div>
							<div class="commessage"><strong>更新时间：</strong><span id="lastGlTime"></span></div>
						</div>
					</div>
				</div>
				<!-- END PAGE FUNCTION BUTTON  & COM STATUS -->
				
				<!-- BEGIN PAGE LOG BOX INFO -->
				<div class="row-fluid" style="margin-top:8px;">
					<div class="span6 ">
						<!-- BEGIN Portlet PORTLET-->
						<div class="portlet box grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i><span id="httpSendMassage"></span></h4>
								<div class="actions">
									<a href="javascript:;" class="btn blue mini clear" style="margin-top:-3px;">清除</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="scroller" data-height="175px" data-always-visible="1">
									
								</div>
							</div>
						</div>
						<!-- END Portlet PORTLET-->
					</div>
					
					<div class="span6 ">
						<!-- BEGIN Portlet PORTLET-->
						<div class="portlet box grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i><span id="comSendMassage"></span></h4>
								<div class="actions">
									<a href="javascript:;" class="btn blue mini clear" style="margin-top:-3px;">清除</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="scroller" data-height="175px" data-always-visible="1">
									
								</div>
							</div>
						</div>
						<!-- END Portlet PORTLET-->
					</div>
				</div>
				
				<div class="row-fluid" >
					<div class="span6 " style="margin-top:-15px;">
						<!-- BEGIN Portlet PORTLET-->
						<div class="portlet box grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder" id="" ></i><span id="httpRecieveMessage"></span></h4>
								<div class="actions">
									<a href="javascript:;" class="btn blue mini clear" style="margin-top:-3px;">清除</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="scroller" data-height="175px" data-always-visible="1">
									
								</div>
							</div>
						</div>
						<!-- END Portlet PORTLET-->
					</div>
					
					<div class="span6 " style="margin-top:-15px;">
						<!-- BEGIN Portlet PORTLET-->
						<div class="portlet box grey">
							<div class="portlet-title">
								<h4><i class="icon-reorder"></i><span id="comRecieveMessage"></span></h4>
								<div class="actions">
									<a href="javascript:;" class="btn blue mini clear"  style="margin-top:-3px;">清除</a>
								</div>
							</div>
							<div class="portlet-body">
								<div class="scroller" data-height="175px" data-always-visible="1">
								
								</div>
							</div>
						</div>
						<!-- END Portlet PORTLET-->
					</div>
				</div>
				<!-- END PAGE LOG BOX INFO -->
			</div>
		</div>
		<!-- END PAGE -->
	</div>
	<!-- END CONTAINER -->
	
	<!-- BEGIN POPUP -->
	<!-- END POPUP -->
	
	<!-- BEGIN FOOTER -->
	<jsp:include page="common/footer.jsp"></jsp:include>
	<!-- END FOOTER -->
	
	<!-- BEGIN SCRIPT -->
	<script type="text/javascript" src="${proName}/js/pagejs/manager-com-detail.js"></script>
	<script type="text/javascript">	
		$('#manager-comlist').addClass("active");
		
		var comname = "${comName}";
		var uri = "${proName}/findcom.do";
		init(uri,comname);
		page(uri,comname);
	</script>
	<!-- END SCRIPT -->	
</body>
<!-- END BODY -->
</html>