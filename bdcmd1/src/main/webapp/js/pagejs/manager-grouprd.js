/**
 * rd通道机
 * rd通道分组管理
 * 
 */
/****************************** 弹出层 *************************/
	//显示弹出层div
	function showPopup(popupDivId){
		//document.getElementById(popDiv).style.display='block';
		var div_obj = $("#"+popupDivId);  
        var windowWidth = document.body.clientWidth;       
        var windowHeight = document.body.clientHeight;  
        //var popupHeight = div_obj.height();       
        var popupWidth = div_obj.width();   
		div_obj.css({"position": "fixed"})   
        .animate({left: windowWidth/2-popupWidth/2,    
                  top: 100, opacity: "show" }, "speedy"); 
		
		//添加并显示遮罩层   
        $("<div id='mask'></div>").addClass("mask")   
                                  .width(windowWidth + document.body.scrollWidth)   
                                  .height(windowHeight + document.body.scrollHeight)  
                                  .appendTo("body")   
                                  .fadeIn(200); 
	}
	
	//关闭弹出层div
	function closePopup(popupDivId){
		document.getElementById(popupDivId).style.display='none';
		$("#mask").remove(); 
	}
	
	//打开添加rd通道组弹出层
	$(document).on("click",".addrdgroup",function(){
		showPopup('saverdgroup');
	});
	
	//关闭添加rd通道组弹出层
	$(document).on("click",".pop-hide",function(){
		closePopup('saverdgroup');
	});
	
	//打开修改rd通道组弹出层
	var resrdname = "";
	$(document).on("click",".updrdgroup",function(){
		$("#updrdgroupname").val("");
		showPopup('updaterdgroup');
		//赋值
		var rid = $(this).attr("fid");
		rdgroupid = rid;
		$.ajax({
			"type":"POST",
			"url":"getRdGroup.do",
			"data":{"id":rid},
			"datatype":"json",
			"async":false,
			"success":function(res){
				$("#updrdgroupname").val(res.name);
				$("#updatesendfreq").val(res.sendfreq);
				resrdname = res.name;
			},
			"error":function(e){
				
			}
		});
	});
	
	//关闭修改rd通道组弹出层
	$(document).on("click",".pop-hide",function(){
		closePopup('updaterdgroup');
	});
	

/******************************* 参数验证 *******************************/
	//参数值
	var rdgroupname;    //RD组名称
	var rdgroupid;    //RD组id
	var sendfreq;

	//验证标志
	var flag1 = false;  //RD组名称
	var flag2 = false;  //发射频度
	
	//参数验证正确
	function showok(element){
		$("#"+element).removeClass('input_error');
		$("#"+element+"Error").text('');
		$("#"+element+"Error").append('<img style="margin-top:10px;" src="img/02/yes.png">');
	}
	
	//参数验证错误
	function showerror(element,errormessage){
		$("#"+element).addClass('input_error');
		$("#"+element+"Error").text(errormessage);
	}
	
	
	//rd组名称
	$('#rdgroupname,#updrdgroupname').focus(function() {
		
	}).blur(function() {
		var $id = $.trim($(this).attr("id"));
		checkrdgroupname($id);
	});
	var nameexist = true;
	function checkrdgroupname($id){
		var $value = $.trim($("#"+$id).val());
		if($value.length==0){
			showerror($id,"rd组名称不能为空！");
			flag1 = false;
		}else{
			rdgroupnameexist($value);
			if(nameexist){
				if($value==resrdname && $id=="updrdgroupname"){
					//showerror($id,"rd组名称未改变");
					showok($id);
					flag1 = true;
					rdgroupname = $value;
				}else{
					showerror($id,"rd组名称已存在");
					flag1 = false;
				}
				
			}else{
				showok($id);
				flag1 = true;
				rdgroupname = $value;
			}
		}
	}
	//检查rd组名称是否存在
	function rdgroupnameexist(rdgroupname){
		$.ajax({
			"type":"POST",
			"url":"rdGroupIsExist.do",
			"data":{"rdGroupName":rdgroupname},
			"datatype":"json",
			"async":false,  //是否异步
			"success":function(res){
				if(res.status=="FAILED"){
					nameexist = false;
				}else{
					nameexist = true;
				}
			},
			"error":function(e){
				alert("出现错误，"+e);
			}
		});
	}
	
	//发射频度
	$('#savesendfreq,#updatesendfreq').focus(function() {
		
	}).blur(function() {
		var $id = $.trim($(this).attr("id"));
		checksendfreq($id);
	});
	function checksendfreq($id){
		var $value = $.trim($("#"+$id).val());
		var reg = /^\d*$/;
		if($value.length==0){
			showerror($id,"发射频度不能为空！");
			flag2 = false;
		}else if(!reg.test($value)){
			showerror($id,"发射频度必须是数字！");
			flag2 = false;
		}else if($value<0){
			showerror($id,"发射频度不能小于0！");
			flag2 = false;
		}else if($value>10000){
			showerror($id,"发射频度不能大于10000！");
			flag2 = false;
		}else{
			showok($id);
			flag2 = true;
			sendfreq = $value;
		}
	}
	

/******************************* 功能操作 *******************************/
/*
 * 查询RD通道组信息
 * 通过jquery.datatable插件查询RD通道组的信息，并显示在页面
 */
$(function() {
	oTable2 = $('.grouptable').dataTable($.extend(
				dparams,
				{
					"sAjaxSource" : 'listRdGroup.do',
					"fnServerData" : retrieveData,// 自定义数据获取函数
					"bRetrieve": true,
					"aoColumns" : [
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "name",
										"fnRender" : function(obj) {
											var name = obj.aData['name'];
											return name;
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell1",
										"fnRender" : function(obj) {
											var cell1 = obj.aData['cell1'];
											var cell1ChannelType = obj.aData['cell1ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell1,cell1ChannelType,1);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell2",
										"fnRender" : function(obj) {
											var cell = obj.aData['cell2'];
											var cellChannelType = obj.aData['cell2ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell,cellChannelType,2);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell3",
										"fnRender" : function(obj) {
											var cell = obj.aData['cell3'];
											var cellChannelType = obj.aData['cell3ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell,cellChannelType,3);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell4",
										"fnRender" : function(obj) {
											var cell = obj.aData['cell4'];
											var cellChannelType = obj.aData['cell4ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell,cellChannelType,4);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell5",
										"fnRender" : function(obj) {
											var cell = obj.aData['cell5'];
											var cellChannelType = obj.aData['cell5ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell,cellChannelType,5);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell6",
										"fnRender" : function(obj) {
											var cell = obj.aData['cell6'];
											var cellChannelType = obj.aData['cell6ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell,cellChannelType,6);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell7",
										"fnRender" : function(obj) {
											var cell = obj.aData['cell7'];
											var cellChannelType = obj.aData['cell7ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell,cellChannelType,7);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "cell8",
										"fnRender" : function(obj) {
											var cell = obj.aData['cell8'];
											var cellChannelType = obj.aData['cell8ChannelType'];
											var id = obj.aData['id'];
											
											return cellvaluation(id,cell,cellChannelType,8);
										},
										"bSortable" : false
									},
									{
										"sWidth" : "120px",
										"sClass" : "center",
										"mDataProp" : "id",
										"fnRender" : function(obj){
											var id = obj.aData['id'];
											var result = '<a href="javascript:void(0);" class="delrdgroup" fid="'+id+'">删除</a> <a href="javascript:void(0);" class="updrdgroup" fid="'+id+'">修改</a>';
											select_flag = false;
											return result;
										},
										"bSortable" : false
									},
					               ],
				}));
				
	
	$("#mycheck").click(function test() {
		oTable2.fnDraw();
	});
});

// 自定义数据获取函数
function retrieveData(sSource, aoData, fnCallback) {
	aoData.push();
	$.ajax({
		"type" : "POST",
		"url" : sSource,
		"dataType" : "json",
		"data" : aoData,
		"success" : function(resp) {
			fnCallback(resp);
			$('.com_select').chosen();
		},
		"error" : function(resp) {
		}
	});
}

//单元赋值
function cellvaluation(id,cell,cellChannelType,cellnumber){
	var begin = '<select field="'+cellnumber+'" fid="'+id+'" data-placeholder="请选择COM"  style="width:95px;" class="com_select">';
	var end = '</select>';
	var options = "";
	var readonly = "";
	if(cell!='' && cell!=null){
		options+='<option value="0">请选择COM</option>';
		options+='<option value="'+cell+'" selected="selected">'+cell+'</option>';
	}else{
		options+='<option value="0">请选择COM</option>';	
		readonly = 'disabled="disabled"';
	}
	var result = begin+options+end ;
	
	var typeselect0 = "";
	var typeselect1 = "";
	if(cellChannelType){
		typeselect1 = 'selected="selected"'
	}else{
		typeselect0 = 'selected="selected"'
	}
	var typeselect = '<select style="width:95px;margin-top:5px;" class="channelType" '+readonly+' >'
		+'<option value="0" '+typeselect0+'>只收不发</option>'
		+'<option value="1" '+typeselect1+'>且发且收</option>';
	return result+typeselect;
}

/*
 * 新增RD通道组
 * 
 */
$(document).on("click",".saverdgropbtn",function(){
	//提交参数验证
	checkrdgroupname('rdgroupname');
	checksendfreq('savesendfreq');
	if(!flag1||!flag2) return ;
	//判断是否为重复提交
	var $disabled = $(".saverdgropbtn").attr("disabled");
	if($disabled=='disabled') return false;
	//禁止按钮，防止重复提交
    $(".saverdgropbtn").attr("disabled","disabled");

	$.ajax({
		"type":"POST",
		"url":"saveRdGroup.do",
		"data":{"rdgroupname":rdgroupname,"groupSendFreq":sendfreq},
		"datatype":"json",
		"success":function(res){
			if(res.status=="OK"){
				alert("添加成功");
				closePopup('saverdgroup');
				oTable2.fnDraw();
			}else{
				alert("添加成功,"+res.message);
			}
			$(".saverdgropbtn").removeAttr("disabled");
		},
		"error":function(e){
			alert("添加失败，详情："+e);
			$(".saverdgropbtn").removeAttr("disabled");
		}
	});
});

/*
 * 通过id删除RD组
 * 删除前需要解除所有绑定的rd通道，否则不予删除
 */
$(document).on("click",".delrdgroup",function(){
	var rid = $(this).attr("fid");
	var result = "";
	var r_flag = true;
	$.ajax({
		"type":"POST",
		"url":"getRdGroup.do",
		"data":{"id":rid},
		"datatype":"json",
		"async":false,
		"success":function(res){
			if(res.cell1!=null){
				result += "圆盘"+res.name+"单元1绑定"+res.cell1+"\n";
				r_flag = false;
			}
			if(res.cell2!=null){
				result += "圆盘"+res.name+"单元2绑定"+res.cell2+"\n";
				r_flag = false;
			}
			if(res.cell3!=null){
				result += "圆盘"+res.name+"单元3绑定"+res.cell3+"\n";
				r_flag = false;
			}
			if(res.cell4!=null){
				result += "圆盘"+res.name+"单元4绑定"+res.cell4+"\n";
				r_flag = false;
			}
			if(res.cell5!=null){
				result += "圆盘"+res.name+"单元5绑定"+res.cell5+"\n";
				r_flag = false;
			}
			if(res.cell6!=null){
				result += "圆盘"+res.name+"单元6绑定"+res.cell6+"\n";
				r_flag = false;
			}
			if(res.cell7!=null){
				result += "圆盘"+res.name+"单元7绑定"+res.cell7+"\n";
				r_flag = false;
			}
			if(res.cell8!=null){
				result += "圆盘"+res.name+"单元8绑定"+res.cell8+"\n";
				r_flag = false;
			}
			result +="请先解除全部绑定的通道再删除！！！";
		},
		"error":function(e){
			
		}
	});
	if(!r_flag){
		alert(result);
	}else{
		if(confirm("你确定要删除该rd通道组吗？")){
			$.ajax({
				"type":"POST",
				"url":"deleteRdGroup.do",
				"data":{"id":rid},
				"datatype":"json",
				"async":false,
				"success":function(res){
					if(res.status=="OK"){
						alert("删除成功");
						oTable2.fnDraw();
					}else{
						alert("删除失败");
					}
				},
				"error":function(e){
					
				}
			});
		}
	}
});

//修改rd组名称
$(document).on("click",".updrdgropbtn",function(){
	//提交参数验证
	checkrdgroupname('updrdgroupname');
	checksendfreq('updatesendfreq');
	if(!flag1||!flag) return ;
	//判断是否为重复提交
	var $disabled = $(".updrdgropbtn").attr("disabled");
	if($disabled=='disabled') return false;
	//禁止按钮，防止重复提交
    $(".updrdgropbtn").attr("disabled","disabled");
    
    $.ajax({
		"type":"POST",
		"url":"updateRdGroup.do",
		"data":{"id":rdgroupid,"rdgroupname":rdgroupname,"groupSendFreq":sendfreq},
		"datatype":"json",
		"success":function(res){
			if(res.status=="OK"){
				alert("操作成功");
				closePopup('updaterdgroup');
				oTable2.fnDraw();
			}else{
				alert("操作失败,"+res.message);
			}
			$(".updrdgropbtn").removeAttr("disabled");
		},
		"error":function(e){
			alert("修改失败，详情："+e);
			$(".updrdgropbtn").removeAttr("disabled");
		}
	});
});



//获取没绑定的COM
$(document).on("click",".chzn-container",function(){
	var comselect = $(this).prev();
	var comname = comselect.val();
	var comfield = comselect.attr("field");
	var comfid = comselect.attr("fid");
	//alert(comname+"       "+comfield+"         "+comfid);

	comselect.empty();
	$.ajax({
		"type":"POST",
		"url":"getAllNobindComs.do",
		"data":{},
		"datatype":"json",
		"aynch":false,
		"success":function(res){
			var options = "";
			options+='<option value="0">请选择COM</option>';
			if(comname!=0){
				options+='<option value="'+comname+'"  selected="selected">'+comname+'</option>';
			}
			
			for(var o in res){
				options+='<option value="'+res[o].com+'">'+res[o].com+'</option>';
			}
			comselect.append(options);
			comselect.trigger("liszt:updated");
			comselect.chosen();
		},
		"error":function(e){
			alert("获取未绑定COM失败，"+e);
		}
	});
});

//rd组绑定rd通道
$(document).on("change",".com_select",function(){
	var comselect = $(this);
	var comname = comselect.val();
	var cellNumber = comselect.attr("field");
	var rid = comselect.attr("fid");
	
	//获取父对象
	var parent = $(this).parent();
	//获取父对象的所有select子对象
	var selects = parent.find('select');
	var typeselect = selects.eq(1);
	//alert(comname+"       "+cellNumber+"         "+rid);
	$.ajax({
		"type":"POST",
		"url":"cellBindCom.do",
		"data":{"id":rid,"comName":comname,"cellNumber":cellNumber},
		"datatype":"json",
		"success":function(res){
			if(res.status=="OK"){
				if(comname!=null && comname!="0"){
					var statusv = queryChannelType(comname);
					if(statusv==0){
						typeselect.empty();
						var options = '<option value="0" selected="selected">只收不发</option>'
						+'<option value="1">且发且收</option>';
						typeselect.append(options);
					}else if(statusv==1){
						typeselect.empty();
						var options = '<option value="0" >只收不发</option>'
						+'<option value="1" selected="selected">且发且收</option>';
						typeselect.append(options);
					}
					typeselect.removeAttr("disabled");
				}else{
					typeselect.empty();
					var options = '<option value="0" selected="selected">只收不发</option>'
					+'<option value="1">且发且收</option>';
					typeselect.append(options);
					typeselect.attr("disabled","disabled");
				}
			}else{
				alert("操作失败"+res.message);
			}
		},
		"error":function(e){
			alert("操作失败，"+e);
		},
	});
});

//查询通道收发类型
function queryChannelType(comname){
	var f = 2;
	$.ajax({
		"type":"GET",
		"url":"findcom.do",
		"data":{"comName":comname},
		"datatype":"json",
		"async":false,
		"success":function(res){
			if(res.apply){
				f = 1;
			}else{
				f = 0;
			}
		},
		"error":function(e){
			alert("操作失败，"+e);
		},
	});
	return f;
}

//rd通道类型发生改变(收发状态改变)
$(document).on("change",".channelType",function(){
	//获取当前对象
	var channelType = $(this);
	//获取父对象
	var parent = $(this).parent();
	//获取父对象的所有select子对象
	var selects = parent.find('select');
	var cellselect = selects.eq(0);
	
	var comname = cellselect.val();
	var selectValue = $(this).val();

	$.ajax({
		url : "controlSendRecieve.do?comName=" + comname
				+ "&sendStatus=" + selectValue,
		type : 'get',
		// data:'{comName:'+comname+',sendStatus:'+selectValue+'}',
		dataType : 'json',
		async : true, // 默认为true 异步
		error : function() {
			alert("error");
		},
		success : function(data) {
			if (data.apply == true) {
				$(this).val(1);
			} else {
				$(this).val(0);
			}
		}
	});
});



		
		





