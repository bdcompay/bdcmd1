/**
 * com详细页面js
 * 
 * 
 */	
	var uri;
	var comname;
	function init(uri1,comname1){
		uri = uri1;
		comname = comname1;
	}
	
	
	//页面初始加载
	function page(uri,comname){
		$.ajax({     
			url:uri,     
			type:'get',     
			data:'comName='+comname, 
			dataType:'json',
			async : false, //默认为true 异步     
			error:function(){
				alert("error");
			},
			success:function(data){  
				//com=data.aaData;
				//console.log(data);
				loadpage(data);
			}  
		});
	}
	
	//获取一次com运行信息
	function comrun_msg(){
		$.ajax({     
			url:'/bdcmd1/runmsg.do', 
			type:'get',     
			data:'comName='+comname, 
			dataType:'json',
			async : true, //默认为true 异步     
			error:function(){
				alert("msg error");
			},
			success:function(data){ 
				var msg = data.log;
				//boxmsg(msg);
			}  
		});
	}
	
	//页面加载com信息
	function loadpage(com){
		//var com = coms[0];
	    var lastGlTime = com.lastGlTime;			//功率最后更新时间
		var glFreq = com.glFreq;					//功率频率	
		var comPort = com.comPort;					//com端口号
		var comname = com.com;						//com名称
		var deviceType = com.deviceType; 			//协议类型
		var serverCard = com.serverCard;			//服务卡号
		var freq = com.freq;						//频率
		var busyfreq = com.busyfreq;				//剩余忙碌时间(倒计时)
		var openStatus = com.openStatus;			//开启状态
		var status = com.status;					//状态
		var sentCount =	com.sentCount;				//发送数量
		var revCount = 	com.revCount;				//接收数量
		var gleast = com.gleast;		  			//东星
		var glwest = com.glwest;					//西星
		var glback = com.glback;					//备用星
		var open = com.open;						//是否打开
		var normal = com.normal;					//是否正常
		var busy = com.busy;						//是否忙碌
		var apply = com.apply;						//是否正在申请数据
		var comSendMassage = com.comSendMassage;			//发送数据
		var comRecieveMessage =com.comRecieveMessage;		//接收数据
		var httpSendMassage = com.httpSendMassage;			//向平台发送数据	
		var httpRecieveMessage = com.httpRecieveMessage; 	//从平台接收数据
		
		//com状态信息
		$("#serverCard").text(serverCard);
		$("#isopen").text(gat_btn_status(open));
		$("#countdown").text(show_date_time(busyfreq));
		//$("#normal").text(isnormal(normal));
		$("#status").text(status);
		$("#freq").text(freq);
		$("#sentCount").text(sentCount);
		$("#revCount").text(revCount);
		$("#lastGlTime").text(convertTime(lastGlTime));
		$("#deviceType").text(deviceType);
		
		//box头部
		var portlet = jQuery(".portlet");
		$("#comSendMassage").text(comname+"通道接口机发送数据");
		$("#comRecieveMessage").text(comname+"通道接口机接收数据");
		$("#httpSendMassage").text(comname+"发送数据到北斗平台");
		$("#httpRecieveMessage").text(comname+"从北斗平台申请数据");
		$(".gleast").attr("src",get_signal(gleast));
		$(".glwest").attr("src",get_signal(glwest));
		$(".glback").attr("src",get_signal(glback));
		if(open==true){
			portlet.removeClass();
			if(normal){
				portlet.addClass("portlet box green");
			}else{
				portlet.addClass("portlet box red");
			}
			jQuery('button.startacom').text("关闭com");
		}else{
			portlet.removeClass();
			portlet.addClass("portlet box grey");
			jQuery('button.startacom').text("开启com");
		}
		
		//box主体    com信息记录
		var comSend = $("#comSendMassage").parents(".portlet").find(".scroller");
		var comReciver = $("#comRecieveMessage").parents(".portlet").find(".scroller");
		var httpSend = $("#httpSendMassage").parents(".portlet").find(".scroller");
		var httpReciver = $("#httpRecieveMessage").parents(".portlet").find(".scroller");
		addLogMsg(comSend,comSendMassage);
		addLogMsg(comReciver,comRecieveMessage);
		addLogMsg(httpSend,httpSendMassage);
		addLogMsg(httpReciver,httpRecieveMessage);
		
		//5秒刷新一次页面数据
		window.setTimeout("page(uri,comname)",5000);
	}
	
	//添加记录信息
	function addLogMsg(insertPoint,logMessage){
		var logList = logMessage.messageList;
		insertPoint.text("");
		for(var i=0;i<logList.length;i++){
			insertPoint.prepend(logList[i]+"<br />");
		}; 
	}
	
	//倒计时
	function show_date_time(target){   
		 seconds = target-1;
		 if(seconds>0){
			 document.getElementById("countdown").innerHTML="<font color=red>"+seconds+"秒</font>";
			 window.setTimeout("show_date_time(seconds)",1000);
		 }else{
			 document.getElementById("countdown").innerHTML="<font color=red>"+0+"秒</font>";
		 }
	}
	
	 //com运行状态是否正常
	 function isnormal(normal){
		 if(normal==true){
			 return "正常";
		 }else{
			 return "故障";
		 }
	 }
	 
	 //将时间戳格式转换为日期格式
	 function convertTime(datetime){
		 var date = new Date(datetime);
		 Y = date.getFullYear() + '-';
		 M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
		 D = date.getDate() + ' ';
		 h = date.getHours() + ':';
		 m = date.getMinutes() + ':';
		 s = date.getSeconds(); 
		 return Y+M+D+h+m+s;
	 }
	
	//com状态
	function gat_com_status(comstatusv){
		if(comstatusv==0){
			return "grey";
		}else if(comstatusv==1){
		   return "green";
		}else if(comstatusv==2){
			return "red";
		}
	}			
	
	//信号图标
	function get_signal(signalv){
		switch(signalv){
			case 1: return "assets/img/signal/stat_sys_signal_1.png"; break;
			case 2: return "assets/img/signal/stat_sys_signal_2.png"; break;
			case 3: return "assets/img/signal/stat_sys_signal_3.png"; break;
			case 4: return "assets/img/signal/stat_sys_signal_4.png"; break;
			default: return "assets/img/signal/stat_sys_signal_null.png"; break;
		}
	}

	//按钮状态
	function  gat_btn_status(statusv){
		if(statusv==true){
			return "开启";
		}else{
			return "关闭";
		}
	}
			
	// 改变com口状态
	jQuery('button.startacom').die().live("click",function () {
		var portlet = jQuery(this).parents(".portlet");
		var content = jQuery(this).text();
		if(content=="开启com"){
			var uri = "/bdcmd1//startup.do";
			page(uri,comname);
		}else{
			var uri = "/bdcmd1//closecom.do";
			page(uri,comname);
		};
	});
	
	//清空消息框
	jQuery('.portlet .actions a.clear').die().live("click",function () {
		var portlet = jQuery(this).parents(".portlet");
		portlet.find(".scroller").text("");
	});
	
	//清空全部消息框
	jQuery('button.clearall').die().live("click",function () {
		jQuery(".portlet  .scroller").text("");
	});
	
	