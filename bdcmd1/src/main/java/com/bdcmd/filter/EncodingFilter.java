package com.bdcmd.filter; 
import java.io.IOException; 

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession; 

import com.bdcmd.authority.AuthorityInterceptor;
 
public class EncodingFilter implements Filter {

	
	  protected String encoding = null;
	 protected FilterConfig filterConfig;

 public void destroy() {
        //TODO
 }

 public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
       
		HttpServletRequest hrequest = (HttpServletRequest) request;
		HttpServletResponse hresponse = (HttpServletResponse) response;

		HttpSession session = hrequest.getSession();
		String path = hrequest.getRequestURI();
		Object obj = session.getAttribute(AuthorityInterceptor.UserHandler);
		if (this.encoding != null && !this.encoding.equals("")) {
			request.setCharacterEncoding(this.encoding);
			response.setCharacterEncoding(this.encoding);
		} else {
			request.setCharacterEncoding("UTF-8");
			response.setCharacterEncoding("UTF-8");
		}
		if (obj != null) {
			chain.doFilter(request, response);
		} else {
			if (path.indexOf("/login.do") > -1
					||path.indexOf("/login1.do") > -1
					||path.indexOf("/login.html")>-1) {
				
				chain.doFilter(request, response);
			} else {
				hresponse.sendRedirect("login.do");
				return;
			}
		}
    }

 public void init(FilterConfig config) throws ServletException {
	 this.encoding = config.getInitParameter("encoding");
 }
}
