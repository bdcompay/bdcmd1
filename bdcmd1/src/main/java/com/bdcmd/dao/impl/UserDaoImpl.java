package com.bdcmd.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdcmd.dao.UserDao;
import com.bdcmd.entity.Card;
import com.bdcmd.entity.User;

@Repository("UserDaoImpl") 
public class UserDaoImpl extends IDaoImpl<User, Integer> implements UserDao {

	public User query(String username,String password){
		StringBuffer hql = new StringBuffer("from User t where 1=1");
		if (username != null) {
			hql.append(" and t.username='").append(username).append("'");
		}
		if (password != null) {
			hql.append(" and t.password='").append(password).append("'");
		}
 		List<User> list =getList(hql.toString(), 1, 1);
 		if (list.size() != 0) {
			return list.get(0);
		}
		return null;
	}
	
	public User query2(String username){
		StringBuffer hql = new StringBuffer("from User t where 1=1");
		if (username != null) {
			hql.append(" and t.username='").append(username).append("'");
		} 
 		List<User> list =getList(hql.toString(), 1, 1);
 		if (list.size() != 0) {
			return list.get(0);
		}
		return null;
	}


}
