package com.bdcmd.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.bdcmd.dao.ICommDao;
import com.bdcmd.entity.Comm;

@Repository("CommDao")
public class CommDaoImpl extends IDaoImpl<Comm, Integer> implements ICommDao{
	
	/*
	 * (non-Javadoc)
	 * @see com.bdcmd.dao.ICommDao#query
	 */
	public List<Comm> query(Boolean isOpen,Boolean isApply,Integer glFreq,
			String com,String deviceType,String serverCard,String freq,String status,
			Boolean isBindGroup,String groupName,int page,int pageSize){
		StringBuffer hql = new StringBuffer("from Comm where 1=1 ");
		if(isOpen!=null){
			hql.append(" and Open=").append(isOpen);
		}
		if(isApply!=null){
			hql.append(" and Apply=").append(isApply);
		}
		if(glFreq!=null){
			hql.append(" and glFreq=").append(glFreq);
		}
		if(com!=null){
			hql.append(" and com like '%").append(com).append("%'");
		}
		if(deviceType!=null){
			hql.append(" and deviceType='").append(deviceType).append("'");
		}
		if(serverCard!=null){
			hql.append(" and serverCard like '%").append(serverCard).append("%'");
		}
		if(freq!=null){
			hql.append(" and freq='").append(freq).append("'");
		}
		if(status!=null){
			hql.append(" and status='").append(status).append("'");
		}
		if(isBindGroup!=null){
			hql.append(" and BindGroup=").append(isBindGroup);
		}
		if(groupName!=null){
			hql.append(" and channelGroupName='").append(groupName).append("'");
		}
		
		List<Comm> list = getList(hql.toString(), page, pageSize);
		return list;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.bdcmd.dao.ICommDao#count
	 */
	public int count(Boolean isOpen,Boolean isApply,Integer glFreq,
			String com,String deviceType,String serverCard,String freq,String status,
			Boolean isBindGroup,String groupName){
		StringBuffer hql = new StringBuffer("from Comm where 1=1 ");
		if(isOpen!=null){
			hql.append(" and Open=").append(isOpen);
		}
		if(isApply!=null){
			hql.append(" and Apply=").append(isApply);
		}
		if(glFreq!=null){
			hql.append(" and glFreq=").append(glFreq);
		}
		if(com!=null){
			hql.append(" and com like '%").append(com).append("%'");
		}
		if(deviceType!=null){
			hql.append(" and deviceType='").append(deviceType).append("'");
		}
		if(serverCard!=null){
			hql.append(" and serverCard like '%").append(serverCard).append("%'");
		}
		if(freq!=null){
			hql.append(" and freq='").append(freq).append("'");
		}
		if(status!=null){
			hql.append(" and status='").append(status).append("'");
		}
		if(isBindGroup!=null){
			hql.append(" and BindGroup=").append(isBindGroup);
		}
		if(groupName!=null){
			hql.append(" and channelGroupName='").append(groupName).append("'");
		}
		int count = count(hql.toString());
		return count;
	}
}
