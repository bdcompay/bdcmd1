package com.bdcmd.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdcmd.dao.IMessageDao;
import com.bdcmd.entity.Message;

@Repository("messageDaoImpl")
public class MessageDaoImpl extends IDaoImpl<Message, Integer> implements IMessageDao{

}
