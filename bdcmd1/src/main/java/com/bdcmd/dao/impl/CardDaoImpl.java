package com.bdcmd.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdcmd.dao.ICardDao;
import com.bdcmd.entity.Card;

@Repository("cardDaoImpl")
public class CardDaoImpl extends IDaoImpl<Card, Integer> implements ICardDao{

}
