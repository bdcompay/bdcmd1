package com.bdcmd.dao;

 import com.bdcmd.entity.User;

public interface UserDao extends IDao<User, Integer>{

	public User query(String username,String password);
	public User query2(String username);
}
