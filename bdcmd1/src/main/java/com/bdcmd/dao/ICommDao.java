package com.bdcmd.dao;

import java.util.List;

import com.bdcmd.entity.Comm;

public interface ICommDao extends IDao<Comm, Integer>{
	/**
	 * 多条件联合分页查询
	 * 
	 * @param isOpen
	 * 			是否打开
	 * @param isApply
	 * 			是否在申请下发数据
	 * @param glFreq
	 * 			功率监测频度
	 * @param com
	 * 			com口名称
	 * @param deviceType
	 * 			协议
	 * @param serverCard
	 * 			服务卡号
	 * @param freq
	 * 			服务频度
	 * @param status
	 * 			状态
	 * @param isBindGroup
	 * 			是否绑定组
	 * @return
	 * 		查询结果
	 */
	public List<Comm> query(Boolean isOpen,Boolean isApply,Integer glFreq,
			String com,String deviceType,String serverCard,String freq,String status,
			Boolean isBindGroup,String groupName,int page,int pageSize);
	
	/**
	 * 多条件统计
	 * 
	 * @param isOpen
	 * 			是否打开
	 * @param isApply
	 * 			是否在申请下发数据
	 * @param glFreq
	 * 			功率监测频度
	 * @param com
	 * 			com口名称
	 * @param deviceType
	 * 			协议
	 * @param serverCard
	 * 			服务卡号
	 * @param freq
	 * 			服务频度
	 * @param status
	 * 			状态
	 * @param isBindGroup
	 * 			是否绑定组
	 * @return
	 * 		统计数量
	 */
	public int count(Boolean isOpen,Boolean isApply,Integer glFreq,
			String com,String deviceType,String serverCard,String freq,String status,
			Boolean isBindGroup,String groupName);
}
