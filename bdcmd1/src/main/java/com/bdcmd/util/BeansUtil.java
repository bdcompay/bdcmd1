package com.bdcmd.util;

import java.lang.reflect.Method;

import org.apache.log4j.Logger;

/**
 * Bean工具
 * 
 * @author zfc
 * 
 */
public class BeansUtil {
	private static Logger logger = Logger.getLogger(BeansUtil.class);

	/**
	 * 根据属性名拷贝bean的属性
	 * 
	 * @param dest
	 * @param orig
	 * @param ignoreNull
	 *            是否忽略值为null的属性
	 * @param ignoreProperties
	 */
	public static void copyBean(Object dest, final Object orig,
			boolean ignoreNull, final String... ignoreProperties) {
		StringBuffer ignore = new StringBuffer("");
		if (ignoreProperties != null) {
			for (String s : ignoreProperties) {
				ignore.append(s).append(",");
			}
		}

		if (orig == null || dest == null) {
			dest = null;
		} else {
			Class<?> origClass = orig.getClass();
			Class<?> destClass = dest.getClass();
			Method[] destMethods = destClass.getMethods();
			String destMethodName, fieldName;
			Object fieldValue;
			Method origMethod;
			for (Method destMethod : destMethods) {
				destMethodName = destMethod.getName();
				if (destMethodName.matches("^set\\w+")) {
					fieldName = destMethodName.replaceAll("^set", "");
					if (ignore.toString().contains(
							fieldName.substring(0, 1).toLowerCase()
									+ fieldName.substring(1)))
						continue;
					try {
						origMethod = origClass.getMethod("get" + fieldName);
					} catch (NoSuchMethodException e) {
						try {
							origMethod = origClass.getMethod("is" + fieldName);
						} catch (Exception e1) {
							origMethod = null;
						}
					}
					if (origMethod == null) {
						// logger.debug("源对象没有属性" + fieldName +
						// "的获取方法,将忽略此属性的拷贝");
					} else {
						try {
							fieldValue = origMethod.invoke(orig);
							if ((!ignoreNull || fieldValue != null)
									&& destMethod.getParameterTypes()[0] == origMethod
											.getReturnType()) {
								destMethod.invoke(dest, fieldValue);
							}
						} catch (Exception e) {
							logger.error("拷贝属性" + fieldName + "失败");
						}
					}
				}
			}

		}
	}

	/**
	 * 根据属性名拷贝bean的属性
	 * 
	 * @param dest
	 *            目标对象
	 * @param orig
	 *            源对象
	 * @param ignoreNull
	 *            是否忽略值为null的属性
	 */
	public static void copyBean(Object dest, final Object orig,
			boolean ignoreNull) {
		copyBean(dest, orig, ignoreNull, "");
	}

	public static Object getProperty(Object bean, String field) {
		if (bean == null || field == null || field.length() < 1) {
			return null;
		}
		field = field.substring(0, 1).toUpperCase() + field.substring(1);
		Class<?> clazz = bean.getClass();
		Method method = null;
		try {
			method = clazz.getMethod("get" + field);
		} catch (NoSuchMethodException e) {
			try {
				method = clazz.getMethod("is" + field);
			} catch (Exception e1) {
				method = null;
			}
		}
		if (method == null) {
			return null;
		}
		Object result = null;
		try {
			result = method.invoke(bean);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	/**
	 * 根据属性名拷贝bean的属性
	 * 
	 * @param dest
	 *            目标
	 * @param orig
	 *            源
	 * @param ignoreProperties
	 *            要忽略的属性
	 */
	public static void copyBean(Object dest, final Object orig,
			String... ignoreProperties) {
		copyBean(dest, orig, false, ignoreProperties);
	}
}
