package com.bdcmd.util;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;



public class PropertyUtil {
	private static Properties properties;
	private static Logger log = Logger.getLogger(PropertyUtil.class);
	static {
		// 加载属性文件
		InputStream inputStream = null;
		try {
			inputStream = PropertyUtil.class.getClassLoader().getResourceAsStream("resources.properties");	
			properties = new Properties();
			properties.load(inputStream);
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			try {
				if(inputStream!=null)inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 获得属性值
	 * @param key
	 * @return
	 */
	public static String getProperty(String key) {
		return properties.getProperty(key);
	}
}
