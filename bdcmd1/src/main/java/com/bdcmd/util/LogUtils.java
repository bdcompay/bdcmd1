package com.bdcmd.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
public class LogUtils {

	  private final static Logger logdebug = Logger.getLogger("fordebug");
	  private final static Logger loginfo = Logger.getLogger("forinfo");
	  private final static Logger logerror = Logger.getLogger("forerror");
	  private final static Logger logwarn = Logger.getLogger("forwarn");

	  public LogUtils(){
		  
	  }
	  
	  
	  /**
	   * 输出异常信息到log中
	   * @param t 
	   * @return
	   */
	  public static String getTrace(Throwable t) {
			StringWriter stringWriter = new StringWriter();
			PrintWriter writer = new PrintWriter(stringWriter);
			t.printStackTrace(writer);
			StringBuffer buffer = stringWriter.getBuffer();
			return buffer.toString();
	  }
	  
	  /**
	   * info
	   * @param message
	   */
	  public static void loginfo(String message)
	  {
		  loginfo.info(message);
	  }
	  
	  /**
	   * warn
	   * @param message
	   * @param e
	   */
	  public static void logwarn(String message, Exception e)
	  {
		  if(e==null) logwarn.warn(message);
		  else logwarn.warn(message+",详细信息："+getTrace(e));
	  }
	  
	  /**
	   * error
	   * @param message
	   * @param e
	   */
	  public static void logerror(String message, Exception e)
	  {
		  if(e==null) logerror.error(message);
		  else logerror.error(message+",详细信息："+getTrace(e));
	  }
	  
	  /**
	   * debug
	   * @param message
	   * @param e
	   */
	  public static void logdebug(String message, Exception e)
	  {
		  if(e==null) logdebug.debug(message);
		  else logdebug.debug(message+",详细信息："+getTrace(e));
	  }
	 

}
