package com.bdcmd.web.handler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.bdcmd.authority.AuthorityInterceptor;
import com.bdcmd.entity.Card;
import com.bdcmd.entity.User;
import com.bdcmd.service.ICardService;
import com.bdcmd.service.UserService;
import com.bdcmd.service.com.BpcComStatusController;
import com.bdcmd.web.dto.CardDto;
import com.bdcmd.web.dto.ListParam;
import com.bdcmd.web.dto.UserDto;

@Controller
@RequestMapping("/")
public class CardHandler {
	@Autowired
	private ICardService cardService;
	@Autowired
	private UserService userService;
	/*
	 * com口控制类
	 */
	@Autowired
	private BpcComStatusController bpcComStatusController;

	// 主页面
	/*
	 * @RequestMapping(value={"/main.do","/main.jsp","/main.html","/index.do",
	 * "/index.jsp","/index.html"},method = RequestMethod.GET) public String
	 * main(HttpSession session){ User user = this.bySession(session); return
	 * "main"; }
	 */

	// 登录页面
	@RequestMapping(value = { "/login.do", "/login.jsp", "/login.html" }, method = RequestMethod.GET)
	public ModelAndView indexDo(String tipcontent) {
		ModelAndView mav = new ModelAndView();
		if (tipcontent != null && !"".equals("tipcontent")) {
			mav.addObject("tipcontent", tipcontent);
		}
		mav.setViewName("login");
		return mav;
	}

	// 登录业务
	@RequestMapping(value = "/login1.do")
	public ModelAndView login(@RequestParam(required = false) String username,
			@RequestParam(required = false) String password, HttpSession session) {
		ModelAndView mav = new ModelAndView();
		String errormessage = null;
		UserDto obj = (UserDto) session
				.getAttribute(AuthorityInterceptor.UserHandler);
		if (obj != null) {
			username = obj.getUserneam();
			password = obj.getPassword();
		}
		if (username != null && password != null) {
			UserDto userDto = userService.getUserDto(username, password);
			if (userDto != null) {
				if (username.equals(userDto.getUserneam())
						&& password.equals(userDto.getPassword())) {
					session.setAttribute(AuthorityInterceptor.UserHandler,
							userDto);
					mav.setViewName("redirect:index.html");
				} else {
					errormessage = "用户名或密码错误！";
				}
			} else {
				errormessage = "用户名或密码不存在！";
			}
			if (errormessage != null) {
				try {
					mav.addObject("tipcontent", errormessage);
					mav.setViewName("redirect:login.html");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} else {
			try {
				errormessage = "你还没登录，请先登录！";
				mav.addObject("tipcontent", errormessage);
				mav.setViewName("redirect:login.html");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return mav;
	}

	// RD通道（COM）管理页面--默认首页
	@RequestMapping(value = { "/main.do", "/main.jsp", "/main.html",
			"/index.do", "/index.jsp", "/index.html" }, method = RequestMethod.GET)
	public String index(HttpSession session) {
		return "manager-comlist";
	}

	/*
	 * //comOpen
	 * 
	 * @RequestMapping("comOpen.do") public @ResponseBody String comOpen(){
	 * return bpcComStatusController.openPort("com3"); } //comOpen
	 * 
	 * @RequestMapping("comClose.do") public @ResponseBody String comClose(){
	 * return bpcComStatusController.closePort("com3"); }
	 * 
	 * //comlist
	 * 
	 * @RequestMapping("comlist.do") public String comlist(){ return "comlist";
	 * }
	 */

	// comdetail页面
	@RequestMapping(value = { "/comdetail.do", "/comdetail.jsp",
			"/comdetail.html" }, method = RequestMethod.GET)
	public @ResponseBody ModelAndView comDetail(
			@RequestParam(required = false) String comName) {
		ModelAndView mav = new ModelAndView();
		if (comName != null) {
			mav.addObject("comName", comName);
			mav.setViewName("manager-com-detail");
		} else {
			mav.setViewName("redirect:index.html");
		}
		return mav;
	}

	/*
	 * //comdetail页面
	 * 
	 * @RequestMapping(value="/comdetail.do",method = RequestMethod.GET) public
	 * String comDetail(HttpSession session,String comName,Model model){ User
	 * user = this.bySession(session); if(user!=null){
	 * model.addAttribute(comName); return "comDetail"; }else{ return "login"; }
	 * 
	 * }
	 */

	// 查询全部com
	@RequestMapping(value = "/queryCards.do", method = RequestMethod.GET)
	public @ResponseBody ListParam queryUsers(String id) {

		int id1 = 0;
		if (id != null) {
			id1 = Integer.parseInt(id);
		}
		// List<CardDto> cardDtos = cardService.getCardList(id1);
		// 暂时 屏蔽下面这句
		List<CardDto> cardDtos = CardDto.getCardDtos();

		/* 暂时假数据 开始 */
		// List<CardDto> cardDtos=new ArrayList<CardDto>();
		// CardDto cardDto=new CardDto();
		// cardDto.setCom("com1");
		// cardDto.setServerCard("1111");
		// cardDto.setApply(true);
		// cardDtos.add(cardDto);
		/* 暂时假的 结束 */

		ListParam listParam = new ListParam();
		listParam.setAaData(cardDtos);
		return listParam;
	}

	// 根据comname开启com
	@RequestMapping(value = "/startup.do", method = RequestMethod.GET)
	public @ResponseBody Map<String, String> startupCom(String comName) {
		String result = bpcComStatusController.openPort(comName);
		Map<String, String> map = new HashMap<String, String>();
		map.put("message", result);
		return map;
	}

	// 根据comname关闭com
	@RequestMapping(value = "/closecom.do", method = RequestMethod.GET)
	public @ResponseBody CardDto closeCom(String comName) {
		bpcComStatusController.closePort(comName);
		CardDto dto = new CardDto();
		dto = CardDto.getCardDtoByCom(comName);
		return dto;
	}

	// 根据comname查询com
	@RequestMapping(value = "/findcom.do", method = RequestMethod.GET)
	public @ResponseBody CardDto findCOm(String comName) {
		CardDto dto = new CardDto();
		dto = CardDto.getCardDtoByCom(comName);
		return dto;
	}

	// 开启全部com
	@RequestMapping(value = "/startupAll.do", method = RequestMethod.GET)
	public @ResponseBody ListParam startupAllCom(String[] comNames) {

		for (int i = 0; i < comNames.length; i++) {
			bpcComStatusController.openPort(comNames[i]);
		}

		List<CardDto> cardDtos = CardDto.getCardDtos();
		ListParam listParam = new ListParam();
		listParam.setAaData(cardDtos);
		return listParam;
	}

	// 关闭全部com
	@RequestMapping(value = "/closeAll.do", method = RequestMethod.GET)
	public @ResponseBody ListParam closeAllCOm(String[] comNames) {
		for (int i = 0; i < comNames.length; i++) {
			bpcComStatusController.closePort(comNames[i]);
		}

		List<CardDto> cardDtos = CardDto.getCardDtos();
		ListParam listParam = new ListParam();
		listParam.setAaData(cardDtos);
		return listParam;
	}

	// 根据comname查询com运行状态
	@RequestMapping(value = "/runmsg.do", method = RequestMethod.GET)
	public @ResponseBody CardDto getComStatus(String comName) {
		CardDto dto = new CardDto();
		dto = CardDto.getCardDtoByCom(comName);
		return dto;
	}

	@RequestMapping("testsave.do")
	public @ResponseBody ListParam testsave() {
		List<Card> cardDtos = cardService.getCardList();
		ListParam listParam = new ListParam();
		listParam.setAaData(cardDtos);
		return listParam;
	}

	// 根据HttpSession获取当前用户实体
	private User bySession(HttpSession session) {
		User user = null;
		Object obj = session.getAttribute(AuthorityInterceptor.UserHandler);
		if (obj instanceof UserDto) {
			UserDto userDto = (UserDto) obj;
			user = userService.getUser(userDto.getUserneam());
		}
		return user;
	}
	
	//收发状态控制
	@RequestMapping(value="/controlSendRecieve.do",method = RequestMethod.GET)
	public @ResponseBody CardDto controlSendRecieve(String comName,String sendStatus){
		bpcComStatusController.controlSendRecieve(comName,sendStatus);
		CardDto dto=new CardDto();
		dto=CardDto.getCardDtoByCom(comName);
		return dto;
	}
	
	//修改企业用户名
	@RequestMapping(value="/updateCommEntName.do")
	public @ResponseBody CardDto updateCommEntName(String comName,String entName){
		bpcComStatusController.updateCommentName(comName, entName);
		CardDto dto=new CardDto();
		dto=CardDto.getCardDtoByCom(comName);
		return dto;
	}

	// 收发状态控制
	@RequestMapping(value = "/controldeviceTypeChange.do", method = RequestMethod.GET)
	public @ResponseBody CardDto controldeviceTypeChange(String comName,
			String deviceType) {
		bpcComStatusController.controldeviceTypeChange(comName, deviceType);
		CardDto dto = new CardDto();
		dto = CardDto.getCardDtoByCom(comName);
		return dto;
	}
}
