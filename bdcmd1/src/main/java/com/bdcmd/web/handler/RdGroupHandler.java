package com.bdcmd.web.handler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.bdcmd.entity.RdGroup;
import com.bdcmd.service.ICommService;
import com.bdcmd.service.IRdGroupService;
import com.bdcmd.service.UserService;
import com.bdcmd.web.dto.CommDto;

import com.bdsdk.json.ListParam;
import com.bdsdk.json.ObjectResult;
import com.bdsdk.json.ResultStatus;

@Controller
public class RdGroupHandler {
	@Autowired
	private UserService userService;
	@Autowired
	private IRdGroupService rdGroupService;
	@Autowired
	private ICommService commService;
	
	/**
	 * rd通道组管理页面
	 */
	@RequestMapping(value={"/manager-group.html","/manager-group.jsp","/manager-group.do"},method = RequestMethod.GET)
	public String managerGroup(){
		return "manager-group";
	}
	
	//查询通道是否已存在
	@RequestMapping(value="rdGroupIsExist.do",method = RequestMethod.POST)
	public @ResponseBody ObjectResult rdGroupNameIsExist(@RequestParam(required = false) String rdGroupName){
		ObjectResult objectResult = new ObjectResult();
		RdGroup rdGroup = rdGroupService.getRroupByName(rdGroupName);
		if(rdGroup==null){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("Rd通道组不存在");
		}else{
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("Rd通道组已存在");
		}
		return objectResult;
	}
	
	//添加RD组别
	@RequestMapping(value="saveRdGroup.do",method = RequestMethod.POST)
	public @ResponseBody ObjectResult addRdGroup(@RequestParam(required = false) String rdgroupname,
			long groupSendFreq){
		ObjectResult objectResult = new ObjectResult();
		
		boolean b = rdGroupService.save(rdgroupname,groupSendFreq);
		if(b){
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(b);
			objectResult.setMessage("添加rd组成功");
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(b);
			objectResult.setMessage("添加rd组失败");
		}
		
		return objectResult;
	} 
	
	//获取所有没被绑定的RD通道
	@RequestMapping(value="getAllNobindComs.do",method = RequestMethod.POST)
	public @ResponseBody List<CommDto> getAllNobindComs(){
		List<CommDto> comDtos = commService.getNoBindComsDto();
		return comDtos;
	}
	
	//为组单元绑定RD通道
	@RequestMapping(value="cellBindCom.do",method = RequestMethod.POST)
	public @ResponseBody ObjectResult groupCellBindCom(int id,String comName,int cellNumber){
		ObjectResult objectResult = new ObjectResult();
		
		boolean flag = true;
		String errormessage = "";
		if(id<1){
			errormessage+="参数id不能小于1。<br />";
			flag = false;
		}
		if(comName==null || comName.equals("")){
			errormessage+="参数comName不能为空。<br />";
			flag = false;
		}
		if(cellNumber<1 || cellNumber>8){
			errormessage+="参数cellNumber必须是1~8的整数。<br />";
			flag = false;
		}
		if(!flag){
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage(errormessage);
			return objectResult;
		}
		
		boolean b = rdGroupService.groupCellBindCom(id, comName, cellNumber);
		if(b){
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setMessage("操作成功");
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setMessage("系统错误");
		}
		return objectResult;
	}
	
	//通过id查询通道单元绑定rd通道情况
	@RequestMapping(value="getRdGroup.do",method = RequestMethod.POST)
	public @ResponseBody RdGroup queryCellBind(int id){
		RdGroup rdGroup = rdGroupService.get(id);
		return rdGroup;
	}
	
	//通过id删除rd通道
	@RequestMapping(value="deleteRdGroup.do")
	public @ResponseBody ObjectResult deleteRdgroup(int id){
		ObjectResult objectResult = new ObjectResult();
		boolean b = rdGroupService.delete(id);
		if(b){
			objectResult.setResult(b);
			objectResult.setStatus(ResultStatus.OK);
		}else{
			objectResult.setResult(b);
			objectResult.setStatus(ResultStatus.FAILED);
		}
		return objectResult;
	}
	
	//修改rd组名称
	@RequestMapping(value="updateRdGroup.do",method = RequestMethod.POST)
	public @ResponseBody ObjectResult updateRdGroup(int id,
			@RequestParam(required = false) String rdgroupname,
			long groupSendFreq){
		
		ObjectResult objectResult = new ObjectResult();
		RdGroup rg = rdGroupService.get(id);
		String oldGroupName = rg.getName();
		rg.setName(rdgroupname);
		rg.setSendfreq(groupSendFreq);
		
		boolean b = rdGroupService.update(rg,oldGroupName);
		if(b){
			objectResult.setStatus(ResultStatus.OK);
			objectResult.setResult(b);
			objectResult.setMessage("添加rd组成功");
		}else{
			objectResult.setStatus(ResultStatus.FAILED);
			objectResult.setResult(b);
			objectResult.setMessage("添加rd组失败");
		}
		
		return objectResult;
	}
	
	//查询rd组信息（jquery.datatable）
	@RequestMapping(value = "listRdGroup.do",method = RequestMethod.POST)
	public @ResponseBody
	ListParam listReturns(@RequestParam int iDisplayStart,
			@RequestParam int iDisplayLength, @RequestParam int iColumns,
			@RequestParam String sEcho, @RequestParam Integer iSortCol_0,
			@RequestParam String sSortDir_0
			) {
		int page = iDisplayStart == 0 ? 1 : iDisplayStart / iDisplayLength + 1;
		int pageSize = iDisplayLength;
		
		ListParam listParam = rdGroupService.queryRdGroups(page, pageSize);
		listParam.setiDisplayStart(iDisplayStart);
		listParam.setiDisplayLength(iDisplayLength);
		listParam.setsEcho(sEcho);
		return listParam;
	}
	
}
