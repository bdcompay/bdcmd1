package com.bdcmd.web.dto;

import java.lang.reflect.Field;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.bdcmd.entity.Card;
import com.bdcmd.service.com.BpcComStatusController;
import com.bdcmd.service.com.ComStatusBean;
import com.bdcmd.service.com.CommServerService;
import com.bdcmd.service.com.DataShowCahe;
import com.bdcmd.util.BeansUtil;

public class CardDto/* extends Card */{
	private boolean isOpen;// 是否打开
	private boolean isBusy; // 是否忙碌
	private boolean isApply;// 是否在申请下发数据
	private boolean isNormal;//
	private Timestamp lastGlTime = new Timestamp(System.currentTimeMillis());// 功率最后更新时间
	private int glFreq;// 功率监测频度 默认10s
	private int comPort;// com号序号
	private String com;// com口名称
	private String deviceType;// 协议
	private String serverCard;// 服务卡号
	private String freq;// 服务频度
	private String busyfreq;// 剩余忙碌时间(倒计时)
	private String openStatus;
	private String status;
	private String sentCount;
	private String revCount;
	private int gleast;// 东星信号强度
	private int glwest;// 西星信号强度
	private int glback;// 备用星信号强度
	private DataShowCahe comSendMassage;// 发给串口的信息列表
	private DataShowCahe comRecieveMessage;// 接收串口信息列表
	private DataShowCahe httpSendMassage;// 发给http信息列表
	private DataShowCahe httpRecieveMessage;// 接收http信息列表
	private String channelGroupName;
	private boolean isBindGroup;
	private Integer rdGroupCell;
	private String entName;

	public CardDto() {

	}
	
	//获取所有串口信息
	public static List<CardDto> getCardDtos() {
		List<CardDto> l = new ArrayList<CardDto>();
		Collection<CommServerService> lt = BpcComStatusController.commServerServiceMap
				.values();
		for (CommServerService c : lt) {
			ComStatusBean comStatusBean = c.getComStatusBean();
			CardDto cardDto = new CardDto();
			BeansUtil.copyBean(cardDto, comStatusBean, true);
			l.add(cardDto);
		}
		return l;
	}
	//获取根据串口号获取该串口信息
	public static CardDto getCardDtoByCom(String com) {
		CardDto cardDto = new CardDto();
		CommServerService c = BpcComStatusController.commServerServiceMap
				.get(com);
		if (c != null) {
			ComStatusBean comStatusBean = c.getComStatusBean();
			BeansUtil.copyBean(cardDto, comStatusBean, true);
			BeansUtil.copyBean(cardDto, c, true);
		}
		return cardDto;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public boolean isBusy() {
		return isBusy;
	}

	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

	public boolean isApply() {
		return isApply;
	}

	public void setApply(boolean isApply) {
		this.isApply = isApply;
	}

	public boolean isNormal() {
		return isNormal;
	}

	public void setNormal(boolean isNormal) {
		this.isNormal = isNormal;
	}

	public Timestamp getLastGlTime() {
		return lastGlTime;
	}

	public void setLastGlTime(Timestamp lastGlTime) {
		this.lastGlTime = lastGlTime;
	}

	public int getGlFreq() {
		return glFreq;
	}

	public void setGlFreq(int glFreq) {
		this.glFreq = glFreq;
	}

	public int getComPort() {
		return comPort;
	}

	public void setComPort(int comPort) {
		this.comPort = comPort;
	}

	public String getCom() {
		return com;
	}

	public void setCom(String com) {
		this.com = com;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public String getServerCard() {
		return serverCard;
	}

	public void setServerCard(String serverCard) {
		this.serverCard = serverCard;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getBusyfreq() {
		return busyfreq;
	}

	public void setBusyfreq(String busyfreq) {
		this.busyfreq = busyfreq;
	}

	public String getOpenStatus() {
		return openStatus;
	}

	public void setOpenStatus(String openStatus) {
		this.openStatus = openStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getSentCount() {
		return sentCount;
	}

	public void setSentCount(String sentCount) {
		this.sentCount = sentCount;
	}

	public String getRevCount() {
		return revCount;
	}

	public void setRevCount(String revCount) {
		this.revCount = revCount;
	}

	public int getGleast() {
		return gleast;
	}

	public void setGleast(int gleast) {
		this.gleast = gleast;
	}

	public int getGlwest() {
		return glwest;
	}

	public void setGlwest(int glwest) {
		this.glwest = glwest;
	}

	public int getGlback() {
		return glback;
	}

	public void setGlback(int glback) {
		this.glback = glback;
	}

	public DataShowCahe getComSendMassage() {
		return comSendMassage;
	}

	public void setComSendMassage(DataShowCahe comSendMassage) {
		this.comSendMassage = comSendMassage;
	}

	public DataShowCahe getComRecieveMessage() {
		return comRecieveMessage;
	}

	public void setComRecieveMessage(DataShowCahe comRecieveMessage) {
		this.comRecieveMessage = comRecieveMessage;
	}

	public DataShowCahe getHttpSendMassage() {
		return httpSendMassage;
	}

	public void setHttpSendMassage(DataShowCahe httpSendMassage) {
		this.httpSendMassage = httpSendMassage;
	}

	public DataShowCahe getHttpRecieveMessage() {
		return httpRecieveMessage;
	}

	public void setHttpRecieveMessage(DataShowCahe httpRecieveMessage) {
		this.httpRecieveMessage = httpRecieveMessage;
	}

	public String getChannelGroupName() {
		return channelGroupName;
	}

	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}

	public boolean isBindGroup() {
		return isBindGroup;
	}

	public void setBindGroup(boolean isBindGroup) {
		this.isBindGroup = isBindGroup;
	}

	public Integer getRdGroupCell() {
		return rdGroupCell;
	}

	public void setRdGroupCell(Integer rdGroupCell) {
		this.rdGroupCell = rdGroupCell;
	}

	public String getEntName() {
		return entName;
	}

	public void setEntName(String entName) {
		this.entName = entName;
	}

	
}
