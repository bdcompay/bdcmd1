package com.bdcmd.web.dto;

import com.bdcmd.entity.RdGroup;

public class RdGroupDto extends RdGroup {
	private String createdTimeStr;
	/**
	 * 绑定在rd组单元1的通道类型
	 */
	private boolean cell1ChannelType;
	private boolean cell2ChannelType;
	private boolean cell3ChannelType;
	private boolean cell4ChannelType;
	private boolean cell5ChannelType;
	private boolean cell6ChannelType;
	private boolean cell7ChannelType;
	private boolean cell8ChannelType;

	public String getCreatedTimeStr() {
		return createdTimeStr;
	}

	public void setCreatedTimeStr(String createdTimeStr) {
		this.createdTimeStr = createdTimeStr;
	}

	public boolean isCell1ChannelType() {
		return cell1ChannelType;
	}

	public void setCell1ChannelType(boolean cell1ChannelType) {
		this.cell1ChannelType = cell1ChannelType;
	}

	public boolean isCell2ChannelType() {
		return cell2ChannelType;
	}

	public void setCell2ChannelType(boolean cell2ChannelType) {
		this.cell2ChannelType = cell2ChannelType;
	}

	public boolean isCell3ChannelType() {
		return cell3ChannelType;
	}

	public void setCell3ChannelType(boolean cell3ChannelType) {
		this.cell3ChannelType = cell3ChannelType;
	}

	public boolean isCell4ChannelType() {
		return cell4ChannelType;
	}

	public void setCell4ChannelType(boolean cell4ChannelType) {
		this.cell4ChannelType = cell4ChannelType;
	}

	public boolean isCell5ChannelType() {
		return cell5ChannelType;
	}

	public void setCell5ChannelType(boolean cell5ChannelType) {
		this.cell5ChannelType = cell5ChannelType;
	}

	public boolean isCell6ChannelType() {
		return cell6ChannelType;
	}

	public void setCell6ChannelType(boolean cell6ChannelType) {
		this.cell6ChannelType = cell6ChannelType;
	}

	public boolean isCell7ChannelType() {
		return cell7ChannelType;
	}

	public void setCell7ChannelType(boolean cell7ChannelType) {
		this.cell7ChannelType = cell7ChannelType;
	}

	public boolean isCell8ChannelType() {
		return cell8ChannelType;
	}

	public void setCell8ChannelType(boolean cell8ChannelType) {
		this.cell8ChannelType = cell8ChannelType;
	}
	
	


}
