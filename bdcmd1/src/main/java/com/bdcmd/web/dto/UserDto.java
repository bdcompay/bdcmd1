package com.bdcmd.web.dto;

public class UserDto {
	private long id;
	private String userneam;
	private String password;
	public String getUserneam() {
		return userneam;
	}
	public void setUserneam(String userneam) {
		this.userneam = userneam;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
}
