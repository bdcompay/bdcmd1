package com.bdcmd.service;

import java.util.List;

import com.bdcmd.entity.Message;

public interface IMessageService {
	public void save(Message message);
	public void delete(Message message);
	public List<Message> getMessageList(int qutaty);

}
