package com.bdcmd.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdcmd.dao.ICardDao;
import com.bdcmd.entity.Card;
import com.bdcmd.entity.Message;
import com.bdcmd.service.ICardService;
import com.bdcmd.service.IMessageService;
import com.bdcmd.util.BeansUtil;
import com.bdcmd.web.dto.CardDto;

@Service("cardService")
public class CardServiceImpl implements ICardService{
	@Autowired
	private ICardDao cardDaoImpl;
	@Autowired
	private IMessageService messageServiceImpl;
	
	public List<Card> getCardList(){
		return cardDaoImpl.listAll();
	}
	
	public List<CardDto> getCardList(int id){
		List<CardDto> dtos=new ArrayList<CardDto>();
		if(id==0){
			List<Card> cards=cardDaoImpl.listAll();
			for(Card card:cards){
				CardDto dto=new CardDto();
				BeansUtil.copyBean(dto, card, true);
//				dto.setBm_signal(3);
//				dto.setBm_status(true);
				Message message=new Message();
//				message.setSend_card(card);
				message.setContent("内容");
				message.setReceive_card("306608");
				System.out.println(messageServiceImpl);
				messageServiceImpl.save(message);
				dtos.add(dto);
			}
		}
		if(id!=0){
			Card card=cardDaoImpl.get(id);
			CardDto dto=new CardDto();
			BeansUtil.copyBean(dto, card, true);
//			dto.setBm_signal(3);
//			dto.setBm_status(true);
			dtos.add(dto);
		}
		return dtos;
	}

	public CardDto querycom(String comName) {
		// TODO Auto-generated method stub
		String hql = "from Card where bm_com='"+comName+"'";
		List<Card> list = cardDaoImpl.getList(hql, 1, 1);
		
		Card card = list.get(0);
		CardDto dto=new CardDto();
		BeansUtil.copyBean(dto, card, true);
//		dto.setBm_signal(4);
//		dto.setBm_status(true);
		
		System.out.println(dto.toString());
		
		return dto;
	}

	public CardDto startCOm(String comName) {
		// TODO Auto-generated method stub
		String hql = "from Card where bm_com='"+comName+"'";
		List<Card> list = cardDaoImpl.getList(hql, 1, 1);
		
		Card card = list.get(0);
		CardDto dto=new CardDto();
		BeansUtil.copyBean(dto, card, true);
//		dto.setBm_signal(2);
//		dto.setBm_status(true);
		return dto;
	}

	public CardDto closeCom(String comName) {
		// TODO Auto-generated method stub
		String hql = "from Card where bm_com='"+comName+"'";
		List<Card> list = cardDaoImpl.getList(hql, 1, 1);
		
		Card card = list.get(0);
		CardDto dto=new CardDto();
		BeansUtil.copyBean(dto, card, true);
//		dto.setBm_signal(0);
//		dto.setBm_status(false);
		return dto;
	}

}
