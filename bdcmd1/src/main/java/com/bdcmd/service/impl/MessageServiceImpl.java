package com.bdcmd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdcmd.dao.IMessageDao;
import com.bdcmd.entity.Message;
import com.bdcmd.service.IMessageService;

@Service("messageService")
public class MessageServiceImpl implements IMessageService{
	@Autowired
	private IMessageDao messageDaoImpl;
	
	public void save(Message message){
		messageDaoImpl.save(message);
	}
	
	public List<Message> getMessageList(int qutaty){
		return messageDaoImpl.list(0, qutaty);
	}

	public void delete(Message message) {
		 messageDaoImpl.deleteObject(message);
		// TODO Auto-generated method stub
		
	}

}
