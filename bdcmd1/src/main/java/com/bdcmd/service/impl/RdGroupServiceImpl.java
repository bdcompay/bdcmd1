package com.bdcmd.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.log4j.pattern.LogEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdcmd.dao.ICommDao;
import com.bdcmd.dao.IRdGroupDao;
import com.bdcmd.entity.Comm;
import com.bdcmd.entity.RdGroup;
import com.bdcmd.service.IRdGroupService;
import com.bdcmd.service.com.BpcComStatusController;
import com.bdcmd.util.LogUtils;
import com.bdcmd.web.dto.RdGroupDto;
import com.bdsdk.json.ListParam;
import com.bdsdk.util.BeansUtil;
import com.bdsdk.util.CommonMethod;

/**
 * RD通道组业务处理
 * 
 * @author jlj
 *
 */
@Service("rdGroupService")
public class RdGroupServiceImpl  implements IRdGroupService {
	/**
	 * RD通道组Dao
	 */
	@Autowired
	private IRdGroupDao rdGroupDao;
	@Autowired
	private ICommDao commDao;
	
	/*
	 * (non-Javadoc)
	 * @see com.bdcmd.service.IRdGroupService#save(com.bdcmd.entity.RdGroup)
	 */
	@Override
	public boolean save(RdGroup rdGroup) {
		try{
			rdGroupDao.save(rdGroup);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过RD对象增加RD通道组错误", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdcmd.service.IRdGroupService#save(java.lang.String)
	 */
	@Override
	public boolean save(String name,long sendfreq) {
		try{
			RdGroup rdGroup = new RdGroup();
			rdGroup.setName(name);
			rdGroup.setSendfreq(sendfreq);
			rdGroup.setCreatedTime(Calendar.getInstance());
			rdGroupDao.save(rdGroup);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过RD组名称增加RD通道组错误", e);
		}
		return false;
	}
	
	@Override
	public boolean update(RdGroup rdGroup) {
		try{
			rdGroupDao.update(rdGroup);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过rd通道组对象修改rd记录错误", e);
		}
		return false;
	}

	@Override
	public boolean update(RdGroup rdGroup,String groupName) {
		try{
			List<Comm> coms = commDao.query(null, null, null, null, null, null, null, null, null,groupName, 1, 99999);
			for(Comm com:coms){
				com.setChannelGroupName(rdGroup.getName());
				commDao.update(com);
				
				//修改内存中的组名称
				if(BpcComStatusController.
						commServerServiceMap.containsKey(com.getCom())){
					BpcComStatusController
						.commServerServiceMap
						.get(com.getCom())
						.getComStatusBean()
						.setChannelGroupName(rdGroup.getName());
				}
			}
			
			rdGroupDao.update(rdGroup);
			return true;
		}catch(Exception e){
			LogUtils.logerror("修改rd组，以及相关的rd通道错误", e);
		}
		return false;
	}

	@Override
	public boolean update(int id, String name, String cell1, String cell2,
			String cell3, String cell4, String cell5, String cell6,
			String cell7, String cell8) {
		try{
			
			
			
		}catch(Exception e){
			LogUtils.logerror("通过rd通道组属性修改rd记录错误", e);
		}
		return false;
	}

	@Override
	public boolean delete(int id) {
		try{
			rdGroupDao.delete(id);
			return true;
		}catch(Exception e){
			LogUtils.logerror("通过id删除rd通道组错误", e);
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * @see com.bdcmd.service.IRdGroupService#queryRdGroups(int, int)
	 */
	@Override
	public ListParam queryRdGroups(int page, int pageSize) {
		List<RdGroup> listDto = new ArrayList<RdGroup>();
		int count = 0;
		try{
			String hql = "from RdGroup where 1=1 ";
			List<RdGroup> list = rdGroupDao.getList(hql, page, pageSize);
			for(RdGroup rg:list){
				RdGroupDto dto =castBy(rg);
				listDto.add(dto);
			}
			count  = rdGroupDao.countAll();
		}catch(Exception e){
			LogUtils.logerror("查询RD组别(符合jquery.datatable格式)异常", e);
		}
	
		ListParam listParam = new ListParam();
		listParam.setiTotalDisplayRecords(count);
		listParam.setiTotalRecords(count);
		listParam.setAaData(listDto); 
  		return listParam;
	}
	
	/**
	 * 把 RdGroup 转 RdGroupDto
	 * 
	 * @param rg
	 * @return RdGroupDto
	 */
	private RdGroupDto castBy(RdGroup rg){
		RdGroupDto rgdto = new RdGroupDto();
		BeansUtil.copyBean(rgdto, rg, true);
		if(rg.getCreatedTime()!=null){
			rgdto.setCreatedTimeStr(CommonMethod.CalendarToString(rg.getCreatedTime(), "yyyy-MM-dd HH:mm:ss"));
		}
		
		if(rg.getCell1()!=null && !"".equals(rg.getCell1())){
			rgdto.setCell1ChannelType(getComm(rg.getCell1()).isApply());
		}
		if(rg.getCell2()!=null && !"".equals(rg.getCell2())){
			rgdto.setCell2ChannelType(getComm(rg.getCell2()).isApply());
		}
		if(rg.getCell3()!=null && !"".equals(rg.getCell3())){
			rgdto.setCell3ChannelType(getComm(rg.getCell3()).isApply());
		}
		if(rg.getCell4()!=null && !"".equals(rg.getCell4())){
			rgdto.setCell4ChannelType(getComm(rg.getCell4()).isApply());
		}
		if(rg.getCell5()!=null && !"".equals(rg.getCell5())){
			rgdto.setCell5ChannelType(getComm(rg.getCell5()).isApply());
		}
		if(rg.getCell6()!=null && !"".equals(rg.getCell6())){
			rgdto.setCell6ChannelType(getComm(rg.getCell6()).isApply());
		}
		if(rg.getCell7()!=null && !"".equals(rg.getCell7())){
			rgdto.setCell7ChannelType(getComm(rg.getCell7()).isApply());
		}
		if(rg.getCell8()!=null && !"".equals(rg.getCell8())){
			rgdto.setCell8ChannelType(getComm(rg.getCell8()).isApply());
		}
		return rgdto;
	}
	
	private Comm getComm(String comName){
		String hql1="from Comm where com='"+comName+"'";
		List<Comm> comlist=commDao.getList(hql1, 1, 1);
		Comm comm = comlist.get(0);
		return comm;
	}

	@Override
	public boolean groupCellBindCom(int id, String comName, int cellNumber) {
		try{
			RdGroup rdGroup = rdGroupDao.get(id);
			/*
			 * 解除这个单元与之前绑定的rd通道的关系 
			 */
			String cname = "";
			//获取绑定单元的rd通道名,并在数据库中解除绑定关系
			switch (cellNumber) {
				case 1:
					cname = rdGroup.getCell1();
					rdGroup.setCell1(null);
					break;
				case 2:
					cname = rdGroup.getCell2();
					rdGroup.setCell2(null);
					break;
				case 3:
					cname = rdGroup.getCell3();
					rdGroup.setCell3(null);
					break;
				case 4:
					cname = rdGroup.getCell4();
					rdGroup.setCell4(null);
					break;
				case 5:
					cname = rdGroup.getCell5();
					rdGroup.setCell5(null);
					break;
				case 6:
					cname = rdGroup.getCell6();
					rdGroup.setCell6(null);
					break;
				case 7:
					cname = rdGroup.getCell7();
					rdGroup.setCell7(null);
					break;
				case 8:
					cname = rdGroup.getCell8();
					rdGroup.setCell8(null);
					break;
	
				default:
					LogUtils.loginfo("错误参数cellNumber："+cellNumber);
					return false;
			}
			if(cname!=null && !cname.equals("")){
				String hql1="from Comm where com='"+cname+"'";
				List<Comm> comlist=commDao.getList(hql1, 1, 1);
				Comm comm = comlist.get(0);
				
				comm.setBindGroup(false);
				comm.setChannelGroupName(null);
				comm.setRdGroupCell(null);
				commDao.update(comm);
				
				rdGroupDao.update(rdGroup);
			
				//判断内存中是否存在
				if(BpcComStatusController.
						commServerServiceMap.containsKey(cname)){
					BpcComStatusController
						.commServerServiceMap
						.get(cname)
						.getComStatusBean()
						.setBindGroup(false);
					BpcComStatusController
						.commServerServiceMap
						.get(cname)
						.getComStatusBean()
						.setChannelGroupName(null);
					BpcComStatusController
						.commServerServiceMap
						.get(cname)
						.getComStatusBean()
						.setRdGroupCell(null);
				}
			}
			if(comName.equals("0")) return true;
			
			
			//通道组单元绑定rd通道
			String hql="from Comm where com='"+comName+"'";
			List<Comm> coms=commDao.getList(hql, 1, 1);
			Comm com = null;
			if(coms.size()==1){
				com = coms.get(0); 
			}else{
				//系统错误
				LogUtils.loginfo("系统错误，传入的COM名称不存在");
				return false;
			}
			
			if(com.getBindGroup()){
				//rd已绑定用户
				LogUtils.loginfo("操作错误，RD通道已绑定");
				return false;
			}else{
				if(comName.equals("0")){
					comName = null;
				}
				// 绑定rd通道
				switch (cellNumber) {
					case 1:
						rdGroup.setCell1(comName);
						break;
					case 2:
						rdGroup.setCell2(comName);
						break;
					case 3:
						rdGroup.setCell3(comName);
						break;
					case 4:
						rdGroup.setCell4(comName);
						break;
					case 5:
						rdGroup.setCell5(comName);
						break;
					case 6:
						rdGroup.setCell6(comName);
						break;
					case 7:
						rdGroup.setCell7(comName);
						break;
					case 8:
						rdGroup.setCell8(comName);
						break;
		
					default:
						LogUtils.loginfo("错误参数cellNumber："+cellNumber);
						return false;
				}
				
				//修改rd通道信息
				com.setBindGroup(true);
				com.setChannelGroupName(rdGroup.getName());
				com.setRdGroupCell(cellNumber);
				
				//保存更新数据
				rdGroupDao.update(rdGroup);
				commDao.update(com);
				
				if(BpcComStatusController.
						commServerServiceMap.containsKey(comName)){
					//修改内存中的内容
					BpcComStatusController
						.commServerServiceMap
						.get(comName)
						.getComStatusBean()
						.setBindGroup(true);
					BpcComStatusController
						.commServerServiceMap
						.get(comName)
						.getComStatusBean()
						.setChannelGroupName(rdGroup.getName());
					BpcComStatusController
						.commServerServiceMap
						.get(comName)
						.getComStatusBean()
						.setRdGroupCell(cellNumber);
				}
				return true;
			}
		}catch(Exception e){
			LogUtils.logerror("RD通道组单元绑定rd通道错误", e);
		}
		return false;
	}

	@Override
	public RdGroup getRroupByName(String rdGroupName) {
		RdGroup rdGroup = null;
		try{
			String hql="from RdGroup where name='"+rdGroupName+"'";
			List<RdGroup> rdGroups = rdGroupDao.getList(hql, 1, 1);
			if(rdGroups.size()==1){
				rdGroup = rdGroups.get(0);
			}
		}catch(Exception e){
			LogUtils.logerror("通过rd通道组名称查询rd通道组错误", e);
		}
		return rdGroup;
	}

	@Override
	public RdGroup get(int id) {
		return rdGroupDao.get(id);
	}
}
