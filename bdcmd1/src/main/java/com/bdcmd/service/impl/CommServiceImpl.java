package com.bdcmd.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdcmd.dao.ICommDao;
import com.bdcmd.entity.Comm;
import com.bdcmd.service.ICommService;
import com.bdcmd.util.LogUtils;
import com.bdcmd.web.dto.CommDto;
import com.bdsdk.util.BeansUtil;

@Service("commService")
public class CommServiceImpl implements ICommService{
	@Autowired
	private ICommDao CommDao;

	@Override
	public Comm getComm(String com) {
		// TODO Auto-generated method stub
		Comm comm=null;
		String hql="from Comm where com='"+com+"'";
		List<Comm> comms=CommDao.getList(hql, 1, 1, null);
		if(comms!=null&&comms.size()>0){
			comm=comms.get(0);
		}
		return comm;
	}

	@Override
	public void saveComm(Comm comm) {
		// TODO Auto-generated method stub
		 CommDao.save(comm);
	}

	@Override
	public void updateComm(Comm comm) {
		// TODO Auto-generated method stub
		 CommDao.update(comm);
	}

	@Override
	public void deleComm(Comm comm) {
		// TODO Auto-generated method stub
		CommDao.delete(comm.getId());
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.bdcmd.service.ICommService#getNoBindComsDto()
	 */
	@Override
	public List<CommDto> getNoBindComsDto(){
		List<CommDto> dtos = new ArrayList<CommDto>();
		try{
			List<Comm> coms = CommDao.query(null, null, null, null, null, null, null, null, false,null, 1, 99999);
			for(Comm com:coms){
				CommDto dto = castFrom(com);
				dtos.add(dto);
			}
		}catch(Exception e){
			LogUtils.logerror("查询没绑定rd组的COM错误", e);
		}
		return dtos;	
	}
	
	/**
	 * 把 Comm 类型对象转为 CommDto 类型对象
	 * 
	 * @param com
	 * @return
	 */
	private CommDto castFrom(Comm com){
		CommDto dto = new CommDto();
		BeansUtil.copyBean(dto, com, true);
		return dto;
	}
	
}
