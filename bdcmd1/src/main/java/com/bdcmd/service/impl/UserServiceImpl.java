package com.bdcmd.service.impl;

 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdcmd.dao.ICardDao;
import com.bdcmd.dao.UserDao;
import com.bdcmd.entity.User;
import com.bdcmd.service.UserService;
import com.bdcmd.util.BeansUtil;
import com.bdcmd.web.dto.UserDto;
 @Service("")
public class UserServiceImpl implements UserService {
	
	@Autowired
	private UserDao userDao; 
	public UserDto getUserDto(String username,String password ){
		 User user=userDao.query(username, password);
		 if(user!=null){
				return castUserToUserDto(user); 
		 }else{
			 return null;
		 }  
	}
	
	private UserDto castUserToUserDto(User user) {
		UserDto userDto = new UserDto();
		if(user.getUsername()!=null&&user.getPassword()!=null){
		userDto.setPassword(user.getPassword());
		userDto.setUserneam(user.getUsername());
		userDto.setId(user.getId());
		//BeansUtil.copyBean(userDto, user, true); 
		return userDto;
		}else{
			return null;
		}
	}
	
	public User getUser(String userName){
		 User user=userDao.query2(userName); 
		 if(user!=null){
			 return user;
		 }else{
			 return null;
		 }
	}


}
