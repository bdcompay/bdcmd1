package com.bdcmd.service.com;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.bdcmd.entity.Card;
import com.bdcmd.entity.Comm;
import com.bdcmd.entity.RdGroup;
import com.bdcmd.service.ICommService;
import com.bdcmd.service.IRdGroupService;
import com.bdcmd.service.application.AppContext;
import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.http.HttpToBPC;
import com.core.Public;
import com.pro.Analysis;

/**
 * 桥接com口和http通信类
 * 
 * @author ASUS
 *
 */
public class CommServerService extends Thread {
	private SerialDevice serialDevice;// 桥接串口驱动类
	private HttpToBPC httpToBPC;// 桥接平台类
	private Card card;// 北斗卡实体
	private ComStatusBean comStatusBean;// 串口信息bean类
	private DataShowCahe comSendMassage;// 发给串口的信息列表
	private DataShowCahe comRecieveMessage;// 接收串口信息列表
	private DataShowCahe httpSendMassage;// 发给http信息列表
	private DataShowCahe httpRecieveMessage;// 接收http信息列表
	private String errorMessage;// 错误信息
	private ApplicationConfig applicationConfig;
	private boolean postGl;// 是否上传功率到BPC
	private boolean showGl;// 列表是否显示功率输出信息
	
	private ICommService commService;
	private IRdGroupService rdGroupService;
	/**
	 * 监控标记
	 * 用以监视申请发送任务线程
	 */
	public static Map<String,Long> applySendTaskTimeMap = new HashMap<String,Long>();

	public CommServerService(SerialDevice serialDevice) {
		this.serialDevice = serialDevice;
		// 注入spring管理的实例
		ApplicationContext ctx = AppContext.getApplicationContext();
		httpToBPC = (HttpToBPC) ctx.getBean("httpToBPC");
		applicationConfig = (ApplicationConfig) ctx
				.getBean("applicationConfig");
		commService=(ICommService) ctx
				.getBean("commService");
		rdGroupService = (IRdGroupService) ctx
				.getBean("rdGroupService");
		
		postGl = applicationConfig.isPostGl();
		int outputSize=applicationConfig.getOutputSize();
		comSendMassage=new DataShowCahe(outputSize);
		comRecieveMessage=new DataShowCahe(outputSize);
		httpSendMassage=new DataShowCahe(outputSize);
		httpRecieveMessage=new DataShowCahe(outputSize);
	}
	
	/**
	 * 测试
	 */
	private void testSendIntervalHandler(){
		String[] ss = {"COM27","COM28","COM29","COM30","COM31","COM32","COM33","COM34",};
		while(true){
			for(String s:ss){
				//控制rd组内部通道的发射间隔
				if(!sendIntervalHandler(s)) continue ;
				System.out.println(s+"    "+System.currentTimeMillis());
			}
			
		}
	}

	/**
	 * 申请平台数据 发送到串口方法
	 */
	public void send() {
		if (!comStatusBean.isBusy() && comStatusBean.isOpen()
				&& comStatusBean.isNormal()&&comStatusBean.isApply()) {
			applySendTaskTimeMap.put(comStatusBean.getCom(), System.currentTimeMillis());
			//控制rd组内部通道的发射间隔
			if(!sendIntervalHandler(comStatusBean.getCom())) return ;
			
			byte[] bdata = httpToBPC.SendTaskApply(this);
			if (bdata != null) {
				try {
					listWrite(bdata);
					comStatusBean.addSendCount();// 增加发送数量
					comStatusBean.setBusy(true);
					comStatusBean.startBusyfreq();
					Comm comm=commService.getComm(comStatusBean.getCom());
					if(comm!=null){
						comm.setSentCount(comStatusBean.getSentCount());
						commService.updateComm(comm);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}

	/**
	 * 接收串口数据方法
	 * 
	 * @param proData
	 */
	public void recieve(byte[] proData) {
		if (proData != null) {
			// comStatusBean card.getBm_com().substring(3)
			int comid = comStatusBean.getComPort();
			String strData = Analysis.ByteHexToString(proData, proData.length);
			// 判断是否为功率信息
			if (strData.indexOf("245369675F") != -1
					|| strData.indexOf("24474C5A4B") != -1) {
				try{
					setGLStatus(strData, comid);
				}catch(Exception e){
					e.printStackTrace();
				}
				if (isPostGl()) {// 是否将功率信息上传到BPC服务器
					try {
						// 封装发送到BPC
						httpToBPC.SendBDdata(this, proData);
					} catch (Exception e) {
						String logString = "[" + Public.getsystim()
								+ " 监控串口接收的缓存区数据时，发送到BPC错误]";
						applicationConfig.getForerror().error(logString);
						this.httpSendMassage.add(logString);
					}
				}
				if (applicationConfig.isShowGl()) {// 是否在列表显示功率信息
					String revLogString = "[收到功率反馈：" + Public.getsystim() + " Com"
							+ comid + "]  [Data:" + strData + "]";
					applicationConfig.getForinfo().info(revLogString);
					this.comRecieveMessage.add(revLogString);
				}
			} else if (strData.indexOf("245273635F") != -1
					|| strData.indexOf("24534A5858") != -1
					|| strData.indexOf("24464B5858") != -1) {// 回执,自检
				String revLogString = "[收：" + Public.getsystim() + " Com"
						+ comid + "]  [Data:" + strData + "]";
				applicationConfig.getForinfo().info(revLogString);
				this.comRecieveMessage.add(revLogString);
			} else if(strData.indexOf("2449435858") != -1){//收到IC查询指令
				String Dexcard=	strData.substring(14, 20);//16进制卡号字段				   
				String Dexfreq=strData.substring(30, 34);//16进制频度字段
				String card=Integer.toString(Integer.parseInt(Dexcard, 16));
				String freq=Integer.toString(Integer.parseInt(Dexfreq, 16));
				comStatusBean.setServerCard(card);
				comStatusBean.setFreq(freq);
				String revLogString = "[收IC卡信息：" + Public.getsystim() + " Com"
						+ comid + "]  [Data:" + strData + "] 服务卡号："+card+" 频度为"+freq;
				applicationConfig.getForinfo().info(revLogString);
				this.comRecieveMessage.add(revLogString);
			}else {
				String revLogString = "[收：" + Public.getsystim() + " Com"
						+ comid + "]  [Data:" + strData + "]";
				applicationConfig.getForinfo().info(revLogString);
				this.comRecieveMessage.add(revLogString);
				if (comStatusBean == null) {
					comStatusBean = new ComStatusBean();
				}

				comStatusBean.addRevCount();// 增加接收数量
				Comm comm=commService.getComm(comStatusBean.getCom());
				if(comm!=null){
					comm.setRevCount(comStatusBean.getRevCount());
					commService.updateComm(comm);
				}
				try {
					// // 封装发送到BPC
					httpToBPC.SendBDdata(this, proData);
				} catch (Exception e) {
					String logString = "[" + Public.getsystim()
							+ " 监控串口接收的缓存区数据时，发送到BPC错误]";
					applicationConfig.getForerror().error(logString);
					String logstr = "[BPC异常：" + Public.getsystim()
							+ " 监控串口接收的缓存区数据时，发送到BPC错误]";
					this.comSendMassage.add(logstr);
				}
			}
		}
	}
	

	public void run() {
		while (comStatusBean.isOpen()) {
			this.send();
			try {
				Thread.sleep(applicationConfig.getCheckBpcDataTime());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	/**
	 * @Title: setGLStatus
	 * @Description: TODO(更新串口功率状态信息)
	 * @param @param strData
	 * @param @param comid
	 * @return void 返回类型
	 */
	public void setGLStatus(String strData, int comid) {
		String bserverCard = "";
		int eastgl1, eastgl2 = 0, eastgl = 0, westgl1, westgl2 = 0, westgl = 0, backgl1 = 0, backgl2 = 0, backgl = 0;
		if (strData.indexOf("245369675F") != -1) {
			bserverCard = strData.substring(10, 16);
			eastgl1 = Integer.valueOf(strData.substring(16, 18));
			eastgl2 = Integer.valueOf(strData.substring(18, 20));
			eastgl = eastgl1 > eastgl2 ? eastgl1 : eastgl2;
			westgl1 = Integer.valueOf(strData.substring(20, 22));
			westgl2 = Integer.valueOf(strData.substring(22, 23));
			westgl = westgl1 > westgl2 ? westgl1 : westgl2;
			backgl1 = Integer.valueOf(strData.substring(24, 26));
			backgl2 = Integer.valueOf(strData.substring(26, 28));
			backgl = backgl1 > backgl2 ? backgl1 : backgl2;
		} else if (strData.indexOf("24474C5A4B") != -1) {
			bserverCard = strData.substring(14, 20);
			eastgl1 = Integer.valueOf(strData.substring(20, 22));
			eastgl2 = Integer.valueOf(strData.substring(22, 24));
			eastgl = eastgl1 > eastgl2 ? eastgl1 : eastgl2;
			westgl1 = Integer.valueOf(strData.substring(24, 26));
			westgl2 = Integer.valueOf(strData.substring(26, 28));
			westgl = westgl1 > westgl2 ? westgl1 : westgl2;
			backgl1 = Integer.valueOf(strData.substring(28, 30));
			backgl2 = Integer.valueOf(strData.substring(30, 32));
			backgl = backgl1 > backgl2 ? backgl1 : backgl2;
		} else {
			String logString = "解析功率时，无法正确解析命令头";
			applicationConfig.getForerror().error(logString);
			comRecieveMessage.add(logString);
			return;
		}
		if (comStatusBean == null) {
			comStatusBean = new ComStatusBean();
		}
		comStatusBean.setGleast(eastgl);
		comStatusBean.setEastStatus(getGLIcon(eastgl, eastgl2));
		comStatusBean.setGlwest(westgl);
		comStatusBean.setWestStatus(getGLIcon(westgl, westgl2));
		comStatusBean.setGlback(backgl);
		comStatusBean.setBackStatus(getGLIcon(backgl1, backgl2));
		//是否没信号
		if(comStatusBean.isGlAlert()){
			comStatusBean.setNormal(false);	
		}
		else{
			comStatusBean.setNormal(true);
		}
		comStatusBean.setLastGlTime(new Timestamp(System.currentTimeMillis()));

	}

	private String getGLIcon(int gl1, int gl2) {
		Integer glCount = gl1 > gl2 ? gl1 : gl2;
		String tailString = "(0" + gl1 + "0" + gl2 + ")";

		switch (glCount) {
		case 0:
			return "□□□□" + tailString;
		case 1:
			return "■□□□" + tailString;
		case 2:
			return "■■□□" + tailString;
		case 3:
			return "■■■□" + tailString;
		case 4:
			return "■■■■" + tailString;
		default:
			return "异常值" + tailString;
		}
	}
	/**
	 * 数据发送 直接发送，不相互排斥
	 * @param bdata
	 */
	public void write(byte[] bdata){
		try {
			serialDevice.getOutputStream().write(bdata);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				Thread.sleep(1);
				//串口状态设为关闭
				this.comStatusBean.setOpen(false);
				Thread.sleep(1);
				//串口关闭
				this.getApplicationConfig().getForinfo().info(comStatusBean.getCom()+"关闭串口");
				this.getComSendMassage().add(comStatusBean.getCom()+"关闭串口");
				serialDevice.closePort();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	/**
	 * 数据发送 串口相互排斥
	 * @param bdata
	 */
	public void listWrite(byte[] bdata){
		try {
			sendInterval(applicationConfig.getIntervalTime());//串口相互排斥发送最少时间间隔
			serialDevice.getOutputStream().write(bdata);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			try {
				Thread.sleep(1);
				//串口状态设为关闭
				this.comStatusBean.setOpen(false);
				Thread.sleep(1);
				//串口关闭
				this.getApplicationConfig().getForinfo().info(comStatusBean.getCom()+"关闭串口");
				this.getComSendMassage().add(comStatusBean.getCom()+"关闭串口");
				serialDevice.closePort();
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
	}
	/*
	 * intervalTime 时间间隔
	 */
	static synchronized void sendInterval(int intervalTime){
		try {
			Thread.sleep(intervalTime);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * 控制同一个rd组在组发射频度内只有一个rd通道发射短报文
	 * 判断rd通道所在的组是否为发射空闲
	 * 
	 * @param comname 
	 * 		rd通道com名称
	 * @return
	 */
	private synchronized boolean sendIntervalHandler(String comname){
		Comm com = commService.getComm(comname);
		if(com == null || !com.getBindGroup()) return false;
		RdGroup rg = rdGroupService.getRroupByName(com.getChannelGroupName());
		long time = System.currentTimeMillis();
		long lastUpdateTime = rg.getLastUpdateTime();
		long sendFreq = rg.getSendfreq();
		//当前时间大于或等于最后更新时间+rd组发射频度
		if(time>=(lastUpdateTime+sendFreq)){
			rg.setLastUpdateTime(time);
			rdGroupService.update(rg);
			return true;
		}
		return false;
	}
	
/************************************ 分割线    ************************************************/
	
	public SerialDevice getSerialDevice() {
		return serialDevice;
	}

	public void setSerialDevice(SerialDevice serialDevice) {
		this.serialDevice = serialDevice;
	}

	public HttpToBPC getHttpToBPC() {
		return httpToBPC;
	}

	public void setHttpToBPC(HttpToBPC httpToBPC) {
		this.httpToBPC = httpToBPC;
	}

	public Card getCard() {
		return card;
	}

	public void setCard(Card card) {
		this.card = card;
	}

	public DataShowCahe getComSendMassage() {
		return comSendMassage;
	}

	public void setComSendMassage(DataShowCahe comSendMassage) {
		this.comSendMassage = comSendMassage;
	}

	public DataShowCahe getComRecieveMessage() {
		return comRecieveMessage;
	}

	public void setComRecieveMessage(DataShowCahe comRecieveMessage) {
		this.comRecieveMessage = comRecieveMessage;
	}

	public DataShowCahe getHttpSendMassage() {
		return httpSendMassage;
	}

	public void setHttpSendMassage(DataShowCahe httpSendMassage) {
		this.httpSendMassage = httpSendMassage;
	}

	public DataShowCahe getHttpRecieveMessage() {
		return httpRecieveMessage;
	}

	public void setHttpRecieveMessage(DataShowCahe httpRecieveMessage) {
		this.httpRecieveMessage = httpRecieveMessage;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public ApplicationConfig getApplicationConfig() {
		return applicationConfig;
	}

	public void setApplicationConfig(ApplicationConfig applicationConfig) {
		this.applicationConfig = applicationConfig;
	}

	public boolean isPostGl() {
		return postGl;
	}

	public void setPostGl(boolean postGl) {
		this.postGl = postGl;
	}

	public boolean isShowGl() {
		return showGl;
	}

	public void setShowGl(boolean showGl) {
		this.showGl = showGl;
	}

	public ComStatusBean getComStatusBean() {
		return comStatusBean;
	}

	public void setComStatusBean(ComStatusBean comStatusBean) {
		this.comStatusBean = comStatusBean;
	}

}
