/**  
 * @Title: CommSerialConfig.java 
 * @Package com.bdcmd.service.com 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author A18ccms A18ccms_gmail_com  
 * @date 2013-6-21 上午11:02:09 
 * @version V1.0  
 */

package com.bdcmd.service.com.bean;

/**
 * 项目名称：bdcmdCom 类名称：CommSerialConfig 类描述： 创建人：will.yan 创建时间：2013-6-21
 * 上午11:02:09
 * 
 * @version
 */

public class CommSerialConfigBean {
	private int baudrate;// 波特率
	private int databits;// 数据位
	private int stopbits;// 停止位
	private int parity;// 奇偶校验
	private String cardId;// 服务卡号.
	private String channelType;// 设备类型

	private String freq;// 频度
	private int comPort;// 端口号
	private int glFreq;// 功率监测频度
	private int starCountAlert;// 报警机制：最少多少颗星有信号
	private int starGlAlert;// 报警机制：每颗星低于多少时报警
	
	public int getGlFreq() {
		return glFreq;
	}

	public void setGlFreq(int glFreq) {
		this.glFreq = glFreq;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public int getComPort() {
		return comPort;
	}

	public void setComPort(int comPort) {
		this.comPort = comPort;
	}

	public int getBaudrate() {
		return baudrate;
	}

	public void setBaudrate(int baudrate) {
		this.baudrate = baudrate;
	}

	public int getDatabits() {
		return databits;
	}

	public void setDatabits(int databits) {
		this.databits = databits;
	}

	public int getStopbits() {
		return stopbits;
	}

	public void setStopbits(int stopbits) {
		this.stopbits = stopbits;
	}

	public int getParity() {
		return parity;
	}

	public void setParity(int parity) {
		this.parity = parity;
	}

	public int getStarCountAlert() {
		return starCountAlert;
	}

	public void setStarCountAlert(int starCountAlert) {
		this.starCountAlert = starCountAlert;
	}

	public int getStarGlAlert() {
		return starGlAlert;
	}

	public void setStarGlAlert(int starGlAlert) {
		this.starGlAlert = starGlAlert;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	



}
