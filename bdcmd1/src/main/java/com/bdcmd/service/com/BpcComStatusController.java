package com.bdcmd.service.com;

import java.io.IOException;
import java.util.Map;
import java.util.TooManyListenersException;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.Resource;
import javax.comm.NoSuchPortException;
import javax.comm.PortInUseException;
import javax.comm.UnsupportedCommOperationException;

import org.springframework.stereotype.Repository;

import com.bdcmd.entity.Comm;
import com.bdcmd.service.ICommService;
import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.com.bean.CommSerialConfigBean;
import com.bdcmd.util.BeansUtil;

@Repository
public class BpcComStatusController {
	// 串口服务类集合ConcurrentHashMap<com口号，服务类>
	public static ConcurrentHashMap<String, CommServerService> commServerServiceMap = new ConcurrentHashMap<String, CommServerService>();
	// public static ConcurrentHashMap<String, CommPortIdentifier>
	// CommPortIdentifierMap = new ConcurrentHashMap<String,
	// CommPortIdentifier>();
	@Resource
	private ApplicationConfig applicationConfig;
	@Resource
	private CommSerialConfigBean commSerialConfigBean;// 串口全局设置
	@Resource
	private ICommService commService;
	/**
	 * @方法名称 :openPort
	 * @功能描述 :手动打开串口
	 * @返回值类型 :void
	 */
	public String openPort(String com) {
		com = com.toUpperCase();
		String error = null;
		// if (!commServerServiceMap.containsKey(com)) {
		// 串口驱动类
		SerialDevice sd = new SerialDevice(commSerialConfigBean);
		// 串口状态类
		ComStatusBean comStatusBean = new ComStatusBean();
		// 设置串口号
		comStatusBean.setCom(com);
		// 设置串口序号
		comStatusBean.setComPort(Integer.parseInt(com.substring(3)));
		// 设置串口驱动协议 DEVICE_40
		comStatusBean.setDeviceType("DEVICE_40");
		// 串口状态设为打开
		comStatusBean.setOpen(true);
		
		//获取数据库中保存的com口资料
		Comm comm=commService.getComm(com);
		
		if(comm!=null){
			BeansUtil.copyBean(comStatusBean, comm, true);
			comStatusBean.setOpen(true);
			comm.setOpen(true);//手动打开串口，设置为True
			commService.updateComm(comm);
		}else{
			
		}
		// 串口服务类
		CommServerService commServerService = new CommServerService(sd);
		commServerService.setComStatusBean(comStatusBean);
		sd.setCommServerService(commServerService);
		comStatusBean.setCommServerService(commServerService);
		if(commSerialConfigBean.getGlFreq()>0){
			comStatusBean.setGlFreq(commSerialConfigBean.getGlFreq());
		}
		boolean flag = true;
		try {
			sd.openPort();
		} catch (NoSuchPortException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "不存在";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "打开失败";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (PortInUseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "已被使用";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (TooManyListenersException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "打开失败，太多监听器";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (UnsupportedCommOperationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "打开失败，不支持";
			applicationConfig.getForerror().error(error);
			flag = false;
		}
		if (flag) {
			// 读取卡号和频度
			comStatusBean.startICMonitor();
			commServerService.start();
			commServerServiceMap.put(com, commServerService);
			comm=commService.getComm(com);
			if(comm==null){
				comm=new Comm();
				BeansUtil.copyBean(comm,comStatusBean,true);
				commService.saveComm(comm);
			}
		}
		// }
		return error;
	}
	
	/**
	 * @方法名称 :atuoOpenPort
	 * @功能描述 :程序启动自动打开串口
	 * @返回值类型 :void
	 */
	public String atuoOpenPort(String com) {
		com = com.toUpperCase();
		String error = null;
		// if (!commServerServiceMap.containsKey(com)) {
		// 串口驱动类
		SerialDevice sd = new SerialDevice(commSerialConfigBean);
		// 串口状态类
		ComStatusBean comStatusBean = new ComStatusBean();
		// 设置串口号
		comStatusBean.setCom(com);
		// 设置串口序号
		comStatusBean.setComPort(Integer.parseInt(com.substring(3)));
		// 设置串口驱动协议 DEVICE_40
		comStatusBean.setDeviceType("DEVICE_40");
		// 串口状态设为打开
		comStatusBean.setOpen(true);
		
		//获取数据库中保存的com口资料
		Comm comm=commService.getComm(com);
		if(comm!=null){
			BeansUtil.copyBean(comStatusBean, comm, true);
		}
		// 串口服务类
		CommServerService commServerService = new CommServerService(sd);
		commServerService.setComStatusBean(comStatusBean);
		sd.setCommServerService(commServerService);
		comStatusBean.setCommServerService(commServerService);
		if(commSerialConfigBean.getGlFreq()>0){
			comStatusBean.setGlFreq(commSerialConfigBean.getGlFreq());
		}
		boolean flag = true;
		try {
			if(comStatusBean.isOpen()){
				sd.openPort();
			}
		} catch (NoSuchPortException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "不存在";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "打开失败";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (PortInUseException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "已被使用";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (TooManyListenersException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "打开失败，太多监听器";
			applicationConfig.getForerror().error(error);
			flag = false;
		} catch (UnsupportedCommOperationException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			error = com + "打开失败，不支持";
			applicationConfig.getForerror().error(error);
			flag = false;
		}
		if (flag) {
			// 读取卡号和频度
			comStatusBean.startICMonitor();
			commServerService.start();
			commServerServiceMap.put(com, commServerService);
			comm=commService.getComm(com);
			if(comm==null){
				comm=new Comm();
				BeansUtil.copyBean(comm,comStatusBean,true);
				commService.saveComm(comm);
			}
		}
		// }
		return error;
	}

	
	
	/**
	 * @方法名称 :closePort
	 * @功能描述 :关闭串口
	 * @返回值类型 :void
	 */
	public String closePort(String com) {
		com = com.toUpperCase();
		String info = null;
		if (!commServerServiceMap.containsKey(com)) {
			info = com + "不存在,或则没打开";
		} else {
			commServerServiceMap.get(com).getComStatusBean().setOpen(false);
			if (commServerServiceMap.get(com).getSerialDevice().closePort()) {
				Comm comm=commService.getComm(com);
				if(comm!=null){
					comm.setOpen(false);
					commService.updateComm(comm);
				}
				info = com + "成功关闭";
				//com口已关闭，无需监控，移除当前COM的申请发送任务线程标志
				Map<String,Long> applySendTaskTimeMap = CommServerService.applySendTaskTimeMap;
				if(applySendTaskTimeMap.containsKey(com)){
					applySendTaskTimeMap.remove(com);
				}
			} else {
				info = com + "关闭异常";
			}
		}
		return info;

	}

	/**
	 * 
	 * @param comName comm口名称
	 * @param sendStatus 状态  1且收且发  0只收不发
	 */
	public void controlSendRecieve(String comName, String sendStatus) {
		// TODO Auto-generated method stub
		if(sendStatus!=null&&sendStatus.equals("1")){
			commServerServiceMap.get(comName).getComStatusBean().setApply(true);
			Comm comm=commService.getComm(comName);
			if(comm!=null){
				comm.setApply(true);
				commService.updateComm(comm);
			}
			
		}
		if(sendStatus!=null&&sendStatus.equals("0")){
			//com口只收不发，无需监控，移除当前COM的申请发送任务线程标志
			Map<String,Long> applySendTaskTimeMap = CommServerService.applySendTaskTimeMap;
			if(applySendTaskTimeMap.containsKey(comName)){
				applySendTaskTimeMap.remove(comName);
			}
			
			commServerServiceMap.get(comName).getComStatusBean().setApply(false);
			Comm comm=commService.getComm(comName);
			if(comm!=null){
				comm.setApply(false);
				commService.updateComm(comm);
			}
		}
		
	}
	
	/**
	 * 修改com企业名称
	 * @param comName
	 * @param entName
	 */
	public void updateCommentName(String comName,String entName){
		commServerServiceMap.get(comName).getComStatusBean().setEntName(entName);
		Comm comm=commService.getComm(comName);
		if(comm!=null){
			comm.setEntName(entName);
			commService.updateComm(comm);
		}
	}
	
	public void init() {
		String comlist = applicationConfig.getAvailableCom();
		String[] comArray = comlist.split(",");
		for (int i = 0; i < comArray.length; i++) {
			String com = comArray[i];
			atuoOpenPort(com);
		}

	}

	public ApplicationConfig getApplicationConfig() {
		return applicationConfig;
	}

	public void setApplicationConfig(ApplicationConfig applicationConfig) {
		this.applicationConfig = applicationConfig;
	}

	public CommSerialConfigBean getCommSerialConfigBean() {
		return commSerialConfigBean;
	}

	public void setCommSerialConfigBean(
			CommSerialConfigBean commSerialConfigBean) {
		this.commSerialConfigBean = commSerialConfigBean;
	}

	/**
	 * 
	 * @param comName com口名称
	 * @param deviceType 设备协议
	 */
	public void controldeviceTypeChange(String comName, String deviceType) {
		// TODO Auto-generated method stub
		if(deviceType!=null&&!deviceType.equals("")){
			commServerServiceMap.get(comName).getComStatusBean().setDeviceType(deviceType);;
			Comm comm=commService.getComm(comName);
			if(comm!=null){
				comm.setDeviceType(deviceType);
				commService.updateComm(comm);
			}
			
		}
	}

}