package com.bdcmd.service.com;

import java.util.ArrayList;

/**
 * 缓存要显示的消息
 * @author ASUS
 *
 */
public class DataShowCahe{
	//缓存消息条数
	public int size=20;
	//消息集合
	public ArrayList messageList=new ArrayList();
	
	public DataShowCahe(){
	}
	
	public DataShowCahe(int size){
		this.size=size;
	}
	/*
	 * 超过最大消息条数，删除第一条在增加
	 */
	public boolean add(Object e) {
		if(messageList.size()<size){
			messageList.add(e);
		}else{
			messageList.remove(0);
			messageList.add(e);
		}
		return true;
	}

	public int getSize() {
		return size;
	}

	public void setSize(int size) {
		this.size = size;
	}

	public ArrayList getMessageList() {
		return messageList;
	}

	public void setMessageList(ArrayList messageList) {
		this.messageList = messageList;
	}
	
	
}
