package com.bdcmd.service.com;

import java.util.Observable;
import java.util.Observer;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.bdcmd.service.application.AppContext;
import com.bdcmd.service.application.ApplicationConfig;
import com.core.Public;
import com.pro.Analysis;

public class CommDataObserver implements Observer {
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger forerror = Logger.getLogger("forerror");
	private ApplicationConfig applicationConfig;
	private int comPort;
	private String bdDataStr = "";
	private String cardId;
	private String bdChannelType;
	private String channelGroupName;
	/*
	 * "244963635F";//返回IC卡信息 "245273635F";//定位申请,发送信息成功回执 "24506F735F";//返回定位信息
	 * "245369675F";//返回功率信息 "244D73675F";//接收、发送信息头
	 */

	private String HEX_BD241 = "244963635F,245273635F,24506F735F,245369675F,244D73675F";

	/*
	 * "24474C4A43";//功率检测 "2444575858";//定位信息 "2454585858";//通信信息
	 * "2449435858";//IC卡信息 "245A4A5858";//自检信息 "24534A5858";//时间信息
	 * "2442425858";//版本信息 "24464B5858";//反馈信息 "245458485A";//短信回执
	 * "2458545A54";//未知协议,24474C5A4B//功率状况
	 */
	private String HEX_BD242 = "2444575858,2454585858,2449435858,245A4A5858,24534A5858,2442425858,24464B5858,245458485A,2458545A54,24474C5A4B";

	public CommDataObserver() {
		ApplicationContext ctx = AppContext.getApplicationContext();
		applicationConfig = (ApplicationConfig) ctx
				.getBean("applicationConfig");
	}

	public void update(Observable o, Object arg) {
		byte[] bdata = (byte[]) arg;
		String bdataString = Analysis.ByteHexToString(bdata, bdata.length);
		synchronized (this.bdDataStr) {
			bdDataStr = bdDataStr + bdataString;
		}

	}

	/**
	 * @Title: getProData
	 * @Description: TODO(获取需要处理的协议)
	 * @param @return
	 * @return byte[] 返回类型
	 */
	public byte[] getProData() {
		synchronized (this.bdDataStr) {
			if (this.bdDataStr.length() < 10) {
				return null;
			}
			byte[] bdData = this.getBdData();
			String hex = Analysis.ByteHexToString(bdData, 5);

			if (HEX_BD242.indexOf(hex) != -1) {
				byte[] blenth = new byte[2];
				blenth[0] = bdData[5];
				blenth[1] = bdData[6];
				int lenth = Analysis.TwoByteToInt(blenth);
				if (bdData.length >= lenth) {
					// 获取数据
					byte[] proData = new byte[lenth];
					proData = Analysis.StringToHexByte(bdDataStr.substring(0,
							lenth * 2));
					// 重置bdData的数据
					bdDataStr = bdDataStr.substring(lenth * 2);
					return proData;
				}
			} else if (HEX_BD241.indexOf(hex) != -1) {

				int tailLoc = this.bdDataStr.indexOf("5F0A");
				if (tailLoc != -1) {
					byte[] proData = new byte[tailLoc + 4];
					proData = Analysis.StringToHexByte(bdDataStr.substring(0,
							tailLoc + 4));
					// 重置bdData的数据
					bdDataStr = bdDataStr.substring(tailLoc + 4);
					return proData;
				} else {
					return null;
				}
				// 这里要处理，头尾

				// // 获取数据
				// byte[] proData = new byte[lenth];
				// proData = Analysis.StringToHexByte(bdDataStr.substring(0,
				// lenth * 2));
				// // 重置bdData的数据
				// bdDataStr = bdDataStr.substring(lenth * 2);
				// return proData;

			} else {
				// 缓存区的协议有误，清除
				String logString = "[收：" + Public.getsystim() + " Com"
						+ comPort + "] [缓冲区数据有误，找不到正确的协议头，现执行清除！][Data:"
						+ this.bdDataStr + "]";
				this.bdDataStr = "";
				forerror.error(logString);
//				applicationConfig.addData_ListModel_com_rev(logString);
			}
			return null;
		}
	}

	public byte[] getBdData() {
		return Analysis.StringToHexByte(bdDataStr);

	}

	public int getComPort() {
		return comPort;
	}

	public void setComPort(int comPort) {
		this.comPort = comPort;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public String getBdChannelType() {
		return bdChannelType;
	}

	public void setBdChannelType(String bdChannelType) {
		this.bdChannelType = bdChannelType;
	}

	public String getChannelGroupName() {
		return channelGroupName;
	}

	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}

}