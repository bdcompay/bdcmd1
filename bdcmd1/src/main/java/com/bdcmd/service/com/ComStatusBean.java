package com.bdcmd.service.com;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;

import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.com.bean.CommSerialConfigBean;
import com.bdcmd.service.mail.SendYeMail;
import com.core.Public;
import com.pro.Analysis;

public class ComStatusBean extends Thread {
	private boolean isOpen = false;// 是否打开
	private boolean isBusy = false;// 是否忙碌
	private boolean isApply = false;// 是否在申请下发数据
	private boolean isNormal = false;//
	private Timestamp lastGlTime = new Timestamp(System.currentTimeMillis());// 功率最后更新时间
	private int glFreq = 10;// 功率监测频度 默认10s
	private int comPort;// com号序号
	private String com;// com口名称
	private String deviceType = "";// 协议
	private String serverCard = "";// 服务卡号
	private String freq = "";// 服务频度
	private String busyfreq = "00";// 剩余忙碌时间
	private String openStatus = "×";
	private String status = "未知";
	private String eastStatus = "□□□□(0000)";
	private String westStatus = "□□□□(0000)";
	private String backStatus = "□□□□(0000)";
	private String sentCount = "0";
	private String revCount = "0";
	private int gleast = 0;// 东星信号强度
	private int glwest = 0;// 西星信号强度
	private int glback = 0;// 备用星信号强度
	private CommServerService commServerService;
	private String channelGroupName;
	private boolean isBindGroup;
	private Integer rdGroupCell;
	private String entName;

	public String getComStr() {

		return " 【COM:" + comPort + "】【串口状态：" + openStatus + "】【倒计时："
				+ busyfreq + "】【状态：" + status + "】【服务卡:" + serverCard + "，频度:"
				+ freq + "秒，设备协议类型:" + deviceType + "】【东星：" + eastStatus
				+ "】【 西星：" + westStatus + "】【备用星：" + backStatus + "】【发送数量："
				+ sentCount + "】【接收数量：" + revCount + "】";
	}

	/**
	 * 功率监控数据
	 * 
	 * @return
	 */
	private byte[] getGlPro() {
		if (getDeviceType().equals("DEVICE_4IN1")) {
			byte[] bProData = new byte[12];
			bProData[0] = (byte) 0x24;
			bProData[1] = (byte) 0x53;
			bProData[2] = (byte) 0x69;
			bProData[3] = (byte) 0x67;
			bProData[4] = (byte) 0x5F;
			byte[] bCardid = Analysis.intToByteArray1(Integer
					.valueOf(serverCard));
			bProData[5] = (byte) 0x00;
			bProData[6] = (byte) 0x00;
			bProData[7] = (byte) 0x00;
			bProData[8] = (byte) 0x00;
			bProData[9] = (byte) 0x00;
			bProData[10] = (byte) 0x5F;
			bProData[11] = (byte) 0x0A;
			return bProData;
		} else if (getDeviceType().equals("DEVICE_40")) {
			// 24474C5A4B
			byte[] bProData = new byte[12];
			bProData[0] = (byte) 0x24;
			bProData[1] = (byte) 0x47;
			bProData[2] = (byte) 0x4C;
			bProData[3] = (byte) 0x4A;
			bProData[4] = (byte) 0x43;
			bProData[5] = (byte) 0x00;
			bProData[6] = (byte) 0x0C;
			byte[] bCardid = Analysis.intToByteArray1(Integer
					.valueOf(serverCard));
			bProData[7] = bCardid[0];
			bProData[8] = bCardid[1];
			bProData[9] = bCardid[2];
			bProData[10] = (byte) 0x00;
			byte[] pcrc = new byte[11];
			System.arraycopy(bProData, 0, pcrc, 0, 11);
			bProData[11] = (byte) Analysis.getCRC(pcrc);
			return bProData;
		}
		return null;

	}

	/**
	 * IC卡查询信息指令
	 * 
	 * @return
	 */
	public byte[] getICcard() {
		// 24 49 43 4A 43 00 0C 00 00 00 00 2B
		byte[] bProData = new byte[12];
		bProData[0] = (byte) 0x24;
		bProData[1] = (byte) 0x49;
		bProData[2] = (byte) 0x43;
		bProData[3] = (byte) 0x4A;
		bProData[4] = (byte) 0x43;
		bProData[5] = (byte) 0x00;
		bProData[6] = (byte) 0x0C;
		bProData[7] = (byte) 0x00;
		bProData[8] = (byte) 0x00;
		bProData[9] = (byte) 0x00;
		bProData[10] = (byte) 0x00;
		bProData[11] = (byte) 0x2B;
		return bProData;
	}

	/**
	 * @Title: startBusyfreq
	 * @Description: TODO(串口开始倒计时)
	 * @param
	 * @return void 返回类型
	 */
	public void startBusyfreq() {
		Thread thread = new Thread() {
			public void run() {
				int busyfreq = Integer.valueOf(commServerService
						.getComStatusBean().getFreq());
				for (int i = 1; i <= busyfreq; i++) {
					String busyfreqsString = String.valueOf(busyfreq - i);
					setBusyfreq(busyfreqsString);
					try {
						sleep(1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				setBusy(false);
			}
		};
		thread.start();
	}

	/**
	 * @Title: startGlMonitor
	 * @Description: TODO(开始监控功率)
	 * @param
	 * @return void 返回类型
	 */
	public void startGlMonitor() {

		Thread thread = new Thread() {
			public void run() {
				while (isOpen) {
					// SerialDevice serialDevice =
					// commServerService.getSerialDevice();
					String log = "[ COM" + comPort + "发送功率检测";
					commServerService.getApplicationConfig()
							.getForinfo().info(log);
					commServerService.getComSendMassage().add(log);
					sendComData(commServerService, getGlPro());
					try {
						sleep(glFreq * 1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					if (!isOpen) {
						commServerService.getApplicationConfig().getForinfo()
								.info(com+"已关闭");
						commServerService.getComSendMassage().add(com+"已关闭");
						;
						break;
					}
					Timestamp currentTime = new Timestamp(
							System.currentTimeMillis());
					try {
						int diffSeconds = Public.getTimeDiff(lastGlTime,
								currentTime);
						if (diffSeconds > (glFreq+1)) {
							String logstr = "[信号异常：" + Public.getsystim()
									+ " COM" + comPort + " 超过:" + glFreq
									+ "秒未收到功率信息！";
							commServerService.getApplicationConfig()
									.getForinfo().info(logstr);
							commServerService.getComSendMassage().add(logstr);
							Thread t=new Thread(){
								public void run(){
									SendYeMail.sendmail("RD通道接口机异常提示", "[信号异常：" + Public.getsystim()
											+ " COM" + comPort + " 超过:" + glFreq
											+ "秒未收到功率信息！");
								}
							};
							t.start();
							// 超时没收到功率,报警
							setStatus("超过:" + glFreq + "秒未收到功率信息！");
							setNormal(false);
							setEastStatus("□□□□(0000)");
							setWestStatus("□□□□(0000)");
							setBackStatus("□□□□(0000)");

							// 报警
							commServerService.setErrorMessage(logstr);
							// Thread alertThread = new Thread() {
							// public void run() {
							// commServerService.playAlertMusic();
							// }
							// };
							// alertThread.start();
							// jlist_coms_status
							// .setCellRenderer(new CellRender(
							// comPort, Color.RED));
						} else {
							if (isGlAlert()) {// 没信号
								String logstr = "[信号异常：" + Public.getsystim()
										+ " COM" + comPort + " 信号太弱，请移动模组！";
								commServerService.getApplicationConfig()
										.getForinfo().info(logstr);
								commServerService.getComSendMassage().add(
										logstr);
								setStatus("信号太弱，请移动模组！");
								setNormal(false);
								
								//邮件提示系统
								Thread t=new Thread(){
									public void run(){
										SendYeMail.sendmail("RD通道接口机异常提示", "[信号异常：" + Public.getsystim()
												+ " COM" + comPort + " 信号太弱，请移动模组！");
									}
								};
								t.start();
								
								// 报警
								// Thread alertThread = new Thread() {
								// public void run() {
								// commServerService.playAlertMusic();
								// }
								// };
								// alertThread.start();
								// jlist_coms_status
								// .setCellRenderer(new CellRender(
								// comPort, Color.RED));
								commServerService.setErrorMessage(logstr);

							} else {
								if (!isBusy) {
									setStatus("正常");
									/*
									 * commServerService.stopAlertMusic();
									 * jlist_coms_status .setCellRenderer(new
									 * CellRender( comPort, Color.GREEN));
									 */
									commServerService.setErrorMessage("");
								} else {
									setStatus("忙碌");
									/*
									 * commServerService.stopAlertMusic();
									 * jlist_coms_status .setCellRenderer(new
									 * CellRender( comPort, Color.YELLOW));
									 */
								}

							}
						}
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		};
		thread.start();
	}

	/**
	 * @Title: startICMonitor
	 * @Description: TODO(开始读卡 读频度)
	 * @param
	 * @return void 返回类型
	 */
	public void startICMonitor() {

		Thread thread = new Thread() {
			public void run() {
				while (isOpen && (serverCard.equals("") || freq.equals(""))) {
					if (!isOpen) {
						commServerService.getComSendMassage().add("com已关闭");
						;
						break;
					}
					String element = com+"发送开始读卡  读频度指令";
					commServerService.getApplicationConfig().getForinfo()
							.info(element);
					commServerService.getComSendMassage().add(element);
					sendComData(commServerService, getICcard());
					try {
						sleep(3 * 1000);
						if (!serverCard.equals("") && !freq.equals("")) {
							break;
						}
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					try {
						sleep(10 * 1000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// 读到卡和频度后 开始监控功率
				if (isOpen) {
					startGlMonitor();
				}
			}
		};
		thread.start();
	}

	public void sendComData(CommServerService commServerService, byte[] proData) {
		commServerService.write(proData);
		String element = "[发：" + Public.getsystim() + " [Data:"
				+ Analysis.ByteHexToString(proData, proData.length) + "]";
		commServerService.getApplicationConfig().getForinfo().info(element);
		commServerService.getComSendMassage().add(element);
	}

	public boolean isApply() {
		return isApply;
	}

	public void setApply(boolean isApply) {
		this.isApply = isApply;
	}

	public int getGlFreq() {
		return glFreq;
	}

	public void setGlFreq(int glFreq) {
		this.glFreq = glFreq;
	}

	public int getComPort() {
		return comPort;
	}

	public String getOpenStatus() {
		return openStatus;
	}

	public void setOpenStatus(String openStatus) {
		this.openStatus = openStatus;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getEastStatus() {
		return eastStatus;
	}

	public void setEastStatus(String eastStatus) {
		this.eastStatus = eastStatus;
	}

	public String getWestStatus() {
		return westStatus;
	}

	public void setWestStatus(String westStatus) {
		this.westStatus = westStatus;
	}

	public String getBackStatus() {
		return backStatus;
	}

	public void setBackStatus(String backStatus) {
		this.backStatus = backStatus;
	}

	public String getSentCount() {
		return sentCount;
	}

	public void setSentCount(String sentCount) {
		this.sentCount = sentCount;
	}

	public String getRevCount() {
		return revCount;
	}

	public void setRevCount(String revCount) {
		this.revCount = revCount;
	}

	public String getServerCard() {
		return serverCard;
	}

	public void setServerCard(String serverCard) {
		this.serverCard = serverCard;
	}

	public String getFreq() {
		return freq;
	}

	public void setFreq(String freq) {
		this.freq = freq;
	}

	public String getBusyfreq() {
		return busyfreq;
	}

	public void setBusyfreq(String busyfreq) {
		this.busyfreq = busyfreq;
	}

	public boolean isOpen() {
		return isOpen;
	}

	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}

	public void setComPort(int comPort) {
		this.comPort = comPort;
	}

	public String getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	public boolean isBusy() {
		return isBusy;
	}

	public void setBusy(boolean isBusy) {
		this.isBusy = isBusy;
	}

	public boolean isNormal() {
		return isNormal;
	}

	public void setNormal(boolean isNormal) {
		this.isNormal = isNormal;
	}

	public Timestamp getLastGlTime() {
		return lastGlTime;
	}

	public void setLastGlTime(Timestamp lastGlTime) {
		this.lastGlTime = lastGlTime;
	}

	public int getGleast() {
		return gleast;
	}

	public void setGleast(int gleast) {
		this.gleast = gleast;
	}

	public int getGlwest() {
		return glwest;
	}

	public void setGlwest(int glwest) {
		this.glwest = glwest;
	}

	public int getGlback() {
		return glback;
	}

	public void setGlback(int glback) {
		this.glback = glback;
	}
	//增加接收数量
	public void addRevCount() {
		int revsCount = Integer.valueOf(this.revCount);
		revsCount++;
		this.revCount = String.valueOf(revsCount);
	}
	//增加发送数量
	public void addSendCount() {
		int sendCount = Integer.valueOf(this.sentCount);
		sendCount++;
		this.sentCount = String.valueOf(sendCount);
	}

	/**
	 * @Title: isGlAlert
	 * @Description: TODO(查看是否满足)
	 * @param @return
	 * @return boolean 返回类型
	 */
	public boolean isGlAlert() {
		int starsCount = 0;
		CommSerialConfigBean commSerialConfigBean = commServerService
				.getSerialDevice().getCommSerialConfigBean();
		if (gleast >= commSerialConfigBean.getStarGlAlert()) {
			starsCount++;
		}
		if (glwest >= commSerialConfigBean.getStarGlAlert()) {
			starsCount++;
		}
		if (glback >= commSerialConfigBean.getStarGlAlert()) {
			starsCount++;
		}
		if (starsCount < commSerialConfigBean.getStarCountAlert()) {
			return true;
		} else {
			return false;
		}
	}

	public CommServerService getCommServerService() {
		return commServerService;
	}

	public void setCommServerService(CommServerService commServerService) {
		this.commServerService = commServerService;
	}

	public String getCom() {
		return com;
	}

	public void setCom(String com) {
		this.com = com;
	}

	public String getChannelGroupName() {
		return channelGroupName;
	}

	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}

	public boolean isBindGroup() {
		return isBindGroup;
	}

	public void setBindGroup(boolean isBindGroup) {
		this.isBindGroup = isBindGroup;
	}

	public Integer getRdGroupCell() {
		return rdGroupCell;
	}

	public void setRdGroupCell(Integer rdGroupCell) {
		this.rdGroupCell = rdGroupCell;
	}

	public String getEntName() {
		return entName;
	}

	public void setEntName(String entName) {
		this.entName = entName;
	}
	
	
}