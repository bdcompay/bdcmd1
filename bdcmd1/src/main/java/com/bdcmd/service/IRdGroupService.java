package com.bdcmd.service;

import com.bdcmd.entity.RdGroup;
import com.bdsdk.json.ListParam;

public interface IRdGroupService {
	/**
	 * 通过对象保存
	 */
	public boolean save(RdGroup rdGroup);
	
	/**
	 * 通过对象值保存
	 */
	public boolean save(String name,long sendfreq);
	
	/**
	 * 通过对象修改
	 */
	public boolean update(RdGroup rdGroup);
	
	/**
	 * 修改rd组，以及相关的rd通道
	 */
	public boolean update(RdGroup rdGroup,String groupName);
	
	/**
	 * 通过属性修改
	 */
	public boolean update(int id,String name,String cell1,String cell2,String cell3,
			String cell4,String cell5,String cell6,String cell7,String cell8);
	
	/**
	 * 通过id删除
	 */
	public boolean delete(int id);
	
	/**
	 * 通过id删除
	 */
	public RdGroup get(int id);
	
	/**
	 * 分页查询rd通道组
	 * 
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public ListParam queryRdGroups(int page,int pageSize);
	
	/**
	 * rd组单元绑定RD通道,把comname填入对应的RD通道组单元，修改rd通道相关的信息
	 * 
	 * @param id
	 * 			rd组id
	 * @param comName
	 * 			RD通道名
	 * @param cellNumber
	 * 			rd单元号
	 * @return
	 */
	public boolean groupCellBindCom(int id,String comName,int cellNumber);
	
	/**
	 * 通过rd组名称查询
	 * 
	 * @return
	 */
	public RdGroup getRroupByName(String rdGroupName);
	
	
}
