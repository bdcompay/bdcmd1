package com.bdcmd.service.application;

import javax.annotation.Resource;

import org.apache.log4j.Logger;

import com.bdcmd.service.com.BpcComStatusController;
import com.bdcmd.service.http.HttpSendCache;
import com.bdcmd.service.http.HttpUploadBdcard;

public class InitSystemService {
	@Resource
	private ApplicationConfig applicationConfig;
	@Resource
	private BpcComStatusController bpcComStatusController;
	@Resource
	private HttpSendCache httpSendCache;
	@Resource
	private HttpUploadBdcard httpUploadBdcard;

	public void init() {
		String filename = System.getProperty("user.dir");
		System.setProperty("WORKDIR", filename);
		applicationConfig.setFordebug(Logger.getLogger("fordebug"));
		applicationConfig.setForerror(Logger.getLogger("forerror"));
		applicationConfig.setForinfo(Logger.getLogger("forinfo"));
		Logger forinfo = applicationConfig.getForinfo();
		Logger fordebug = applicationConfig.getFordebug();
		Logger forerror = applicationConfig.getForerror();

		// 通道启动
		bpcComStatusController.init();
		//发送缓存启动
		httpSendCache.start();
		//发送卡号
//		httpUploadBdcard.start();
	}

	public void destory() {
		System.out.println("destory");
	}

}
