package com.bdcmd.service.application;


import javax.annotation.Resource;

import org.apache.log4j.Logger;


/*
 * 
 * 全局配置
 */
public class ApplicationConfig {

	/***** 华丽的分割线*********以下是LOG4J的相关配置 **********************/
	private Logger forerror;
	private Logger forinfo;
	private Logger fordebug;
	private String channelGroupName;
	private boolean postGl;// 是否上传功率到BPC
	private boolean showGl;// 列表是否显示功率输出信息
	
	private int checkBpcDataTime;// 每轮检查BPC发送到串口的间隔时间
	private int sendCacheBpcDataTime;// 每轮检查BPC发送到串口的间隔时间
	private int outputSize;// 四个窗口输出的最大字符数
	private String url;
	private String availableCom;//使用的com口
	private int intervalTime;//串口相互排斥发送最少时间间隔
	public Logger getForerror() {
		return forerror;
	}
	public void setForerror(Logger forerror) {
		this.forerror = forerror;
	}
	public Logger getForinfo() {
		return forinfo;
	}
	public void setForinfo(Logger forinfo) {
		this.forinfo = forinfo;
	}
	public Logger getFordebug() {
		return fordebug;
	}
	public void setFordebug(Logger fordebug) {
		this.fordebug = fordebug;
	}
	public int getCheckBpcDataTime() {
		return checkBpcDataTime;
	}
	public void setCheckBpcDataTime(int checkBpcDataTime) {
		this.checkBpcDataTime = checkBpcDataTime;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getChannelGroupName() {
		return channelGroupName;
	}
	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}
	public boolean isPostGl() {
		return postGl;
	}
	public void setPostGl(boolean postGl) {
		this.postGl = postGl;
	}
	public boolean isShowGl() {
		return showGl;
	}
	public void setShowGl(boolean showGl) {
		this.showGl = showGl;
	}
	public int getOutputSize() {
		return outputSize;
	}
	public void setOutputSize(int outputSize) {
		this.outputSize = outputSize;
	}
	public int getSendCacheBpcDataTime() {
		return sendCacheBpcDataTime;
	}
	public void setSendCacheBpcDataTime(int sendCacheBpcDataTime) {
		this.sendCacheBpcDataTime = sendCacheBpcDataTime;
	}
	public String getAvailableCom() {
		return availableCom;
	}
	public void setAvailableCom(String availableCom) {
		this.availableCom = availableCom;
	}
	public int getIntervalTime() {
		return intervalTime;
	}
	public void setIntervalTime(int intervalTime) {
		this.intervalTime = intervalTime;
	}
	
}