package com.bdcmd.service;

 import com.bdcmd.entity.User;
import com.bdcmd.web.dto.UserDto;

public interface UserService {
	
	public UserDto getUserDto(String username,String password ); 
	public User getUser(String userName);

	 
}
