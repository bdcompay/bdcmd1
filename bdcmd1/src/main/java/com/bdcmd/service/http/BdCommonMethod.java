package com.bdcmd.service.http;

import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * 北斗数据处理的公共方法
 * 
 * @author will.yan
 * 
 */
public final class BdCommonMethod {

	public static Timestamp calendarToTimestamp(Calendar calendar) {
		return new Timestamp(calendar.getTimeInMillis());
	}

	public static Calendar StringToCalendar(String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		Calendar dayc1 = new GregorianCalendar();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date daystart = null;
		try {
			daystart = df.parse(value);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} // start_date是类似"2013-02-02"的字符串
		dayc1.setTime(daystart); // 得到的dayc1就是你需要的calendar了
		return dayc1;
	}

	/**
	 * @param calendar
	 * @param format
	 *            转换格式 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String CalendarToString(Calendar calendar, String format) {
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateStr = sdf.format(calendar.getTime());
		return dateStr;
	}

	/**
	 * @Title: getTime
	 * @Description: TODO(获取系统时间)
	 * @param @return 返回字符串格式的时间
	 * @param @
	 * @return String
	 * @throws
	 */
	public final static String getTime() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String strTime = sdf.format(new java.util.Date());
		return strTime;
	}

	/**
	 * @Title: getProtocolLen
	 * @Description: TODO(添加协议长度，此方法主要适用于有协议长度的协议)
	 * @param @param strProtocol 传入要添加长度的协议
	 * @param @return 返回添加长度后的协议
	 * @param @
	 * @return String
	 * @throws
	 */
	public final static String getProtocolLen(String strProtocol) {
		String strRet = "";
		int intLenBye = 2;
		try {
			int intProtocolLen = (strProtocol.length() / 2) + 1 + intLenBye;// 加1是校验码长度,加intLenBye是协议长度本身
			String strTemp = castDcmStringToHexString(
					String.valueOf(intProtocolLen), intLenBye);
			strRet = strProtocol.substring(0, 10) + strTemp
					+ strProtocol.substring(10);
		} catch (Exception e) {
			System.out.println("【" + strProtocol + "】插入协议总长度错误");
		}
		return strRet;
	}

	/**
	 * @Title: getCheckCode
	 * @Description: TODO(获取校验码，传入无校验码的协议，在协议最后添加校验码，只适合需要校验码的协议)
	 * @param @param strProtocol 无校验码的协议
	 * @param @return 返回有校验码的协议
	 * @param @
	 * @return String
	 * @throws
	 */
	public final static String getCheckCode(String strProtocol) {
		String strRet = "";
		strProtocol.replace(" ", "");
		byte chrCheckCode = 0;
		for (int i = 0; i < strProtocol.length(); i = i + 2) {
			char chrTmp = 0;
			chrTmp = strProtocol.charAt(i);
			if (chrTmp != ' ') {
				byte chTmp1, chTmp2;
				chTmp1 = (byte) (castCharToHexByte(chrTmp) << 4);// 左移4位
				chrTmp = strProtocol.charAt(i + 1);
				chTmp2 = (byte) (chTmp1 + (castCharToHexByte(chrTmp) & 0xF));// 取低4位然后相加。
				if (i == 0) {
					chrCheckCode = chTmp2;
				} else {
					chrCheckCode = (byte) (chrCheckCode ^ chTmp2);
				}
			}
		}
		String strHexCheckCode = String.format("%x", chrCheckCode);
		strHexCheckCode = strHexCheckCode.toUpperCase();
		if (strHexCheckCode.length() != 2) {
			if (strHexCheckCode.length() > 2) {
				strHexCheckCode = strHexCheckCode.substring(strHexCheckCode
						.length() - 2);
			} else if (strHexCheckCode.length() < 2
					&& strHexCheckCode.length() > 0) {
				strHexCheckCode = "0" + strHexCheckCode;
			}
		}
		strRet = strProtocol + strHexCheckCode;

		return strRet;
	}

	/**
	 * 
	 * @方法说明：字符转为byte
	 * @创建日期：2013-2-4下午2:22:44
	 * @参数chr：要转换的字符
	 * @return说明：返回转换后的byte
	 */
	public final static byte castCharToHexByte(char chr) {
		byte chrRet = (byte) -1;
		if ((chr >= '0') && (chr <= '9')) {
			chrRet = (byte) (chr);
		} else if ((chr >= 'A') && (chr <= 'F')) {
			chrRet = (byte) (chr - 'A' + 10);
		} else if ((chr >= 'a') && (chr <= 'f')) {
			chrRet = (byte) (chr - 'a' + 10);
		}
		return chrRet;
	}

	/**
	 * @方法说明：十六进制字符串String转十进制字符串String
	 * @创建日期：2013-1-31下午1:21:48
	 * @参数strHexData：传入十六进制字符串
	 * @return说明：返回十进制字符串
	 */
	public final static String castHexStringToDcmString(String strHexData) {
		String strDcmData = null;
		Long lngNum = Long.parseLong(strHexData, 16);
		strDcmData = String.valueOf(lngNum);
		return strDcmData;
	}

	/**
	 * 
	 * @方法说明：协议字符串添加空格，如果不添加空格，解析时容易出现错误，如取00，可能实际为 10 01，假如黏在一起，则取出的位置是错误的
	 * @创建日期：2013-1-31下午1:22:34
	 * @参数strProtocol：无空格的协议字符串
	 * @return说明：返回有空格的字符串协议
	 */
	public final static String insertSpaceForProtocol(String strProtocol) {
		String strRetData = null;

		int intLen = strProtocol.length();
		for (int i = 0; i < intLen;) {
			String strTemp = strProtocol.substring(i, i = i + 2);
			if (2 >= i) {
				strRetData = strTemp;
			} else {
				strRetData = strRetData + " " + strTemp;
			}
		}
		return strRetData;
	}

	/**
	 * 删除协议中的空格
	 * 
	 * @param strProtocol
	 * @return @
	 */
	public final static String removeSpaceForProtocol(String strProtocol) {
		return strProtocol.replace(" ", "");
	}

	/**
	 * 
	 * @方法说明：十六进制字符串转中文字符串
	 * @创建日期：2013-1-31下午1:23:21
	 * @参数strHexData：传入要转成中文的十六进制的字符串
	 * @return说明：返回中文字符串
	 */
	public final static String castHexStringToHanziString(String strHexData) {
		String strRet = null;
		byte[] bye = castHexStringToByte(strHexData);
		try {
			strRet = new String(bye, "gbk"); // 自己调整编码试试看，如UTF-16LE什么的？
			// System.out.println(strHexData);
		} catch (Exception e) {

		}
		return strRet;
	}

	/**
	 * 
	 * @param strHanzi
	 *            传进中文字符串
	 * @return 返回十六进制的字符串
	 * @创建时间：2013-2-1下午9:07:02
	 * @功能说明：中文字符串转为十六进制字符串
	 */
	public final static String castHanziStringToHexString(String strHanzi) {
		String strRet = "";
		try {
			byte[] bye = strHanzi.getBytes("GBK");
			strRet = castBytesToString(bye);
		} catch (Exception ex) {

		}
		return strRet;
	}

	/**
	 * 
	 * @方法说明：十六进制字符串 保存到byte[]
	 * @创建日期：2013-1-31下午1:24:22
	 * @参数strHexData：传入要转成byte[]的十六进制的字符串
	 * @return说明：返回byte数组
	 */
	public final static byte[] castHexStringToByte(String strHexData) {
		if (strHexData != null) {
			byte[] bye = new byte[strHexData.length() / 2];
			int intLen = strHexData.length();
			for (int i = 0, j = 0; i < intLen; i = i + 2, j++) {
				byte tmpByte1 = (byte) strHexData.charAt(i);
				byte bye1 = (byte) (castCharToHexByte((char) tmpByte1) << 4);// 左移4位
				byte tmpByte2 = (byte) strHexData.charAt(i + 1);
				byte bye2 = (byte) (castCharToHexByte((char) tmpByte2) & 0xF);
				bye[j] = (byte) (bye1 + bye2);// 取低4位然后相加。
			}
			return bye;
		} else {
			return null;
		}
	}

	/**
	 * 
	 * @方法说明：byte数组转换为String字符串
	 * @创建日期：2013-2-4下午2:24:40
	 * @参数byeData：要转换的byte数组
	 * @return说明：返回转换后的字符串
	 */
	public final static String castBytesToString(byte[] byeData) {

		String strRet = null;
		int intLen = byeData.length;
		for (int i = 0; i < intLen; i++) {
			byte byeTemp = byeData[i];

			String strHexTemp = Integer.toHexString(byeTemp);
			if (strHexTemp.length() > 2) {
				strHexTemp = strHexTemp.substring(strHexTemp.length() - 2);
			} else if (strHexTemp.length() < 2) {
				strHexTemp = "0" + strHexTemp;
			}
			if (i == 0) {
				strRet = strHexTemp;
			} else {
				strRet = strRet + strHexTemp;
			}
		}
		strRet = strRet.toUpperCase();
		return strRet;
	}

	/**
	 * 
	 * @方法说明：十六进制字符串的经度、纬度转换为浮点数字符串的经度、纬度，一般由广嘉GPS协议使用
	 * @创建日期：2013-2-1下午12:13:49
	 * @参数strHexTude：要转换的十六进制经度或者纬度数据
	 * @参数intEffect：保留的小数点位数
	 * @return说明：返回十进制的小数点字符串经度或者纬度
	 */
	public final static String castHexStringToDcmStringGalaxyTude(
			String strHexTude, int intEffect) {
		String strFloatTude = null;
		Long lngNum = Long.parseLong(strHexTude, 16);
		String strDcmTude = String.valueOf(lngNum);
		int intLen = strDcmTude.length();
		String strReal = strDcmTude.substring(intLen - intEffect);
		String strDecimals = strDcmTude.substring(0, intLen - intEffect);
		strFloatTude = strDecimals + "." + strReal;
		return strFloatTude;
	}

	/**
	 * 
	 * @方法说明：十六进制字符串的经度、纬度转换为浮点数字符串的经度、纬度，一般由标准协议使用
	 * @创建日期：2013-2-4下午3:01:04
	 * @参数strHexTude：要转换的十六进制经度或者纬度数据
	 * @return说明：返回十进制的小数点字符串经度或者纬度
	 */
	public final static String castHexStringToDcmStringNormTude(
			String strHexTude) {
		String strFloatTude = null;
		strHexTude.replace(" ", "");
		String strHexD = strHexTude.substring(0, 2);
		String strHexF = strHexTude.substring(2, 4);
		String strHexM = strHexTude.substring(4, 6);
		String strHexMM = strHexTude.substring(6, 8);

		String strDcmD = castHexStringToDcmString(strHexD);// 度
		String strDcmF = castHexStringToDcmString(strHexF);// 分
		String strDcmM = castHexStringToDcmString(strHexM);// 秒
		String strDcmMM = castHexStringToDcmString(strHexMM);// 毫秒
		double dblDcmD = Double.parseDouble(strDcmD);
		double dblDcmF = Double.parseDouble(strDcmF);
		double dblDcmM = Double.parseDouble(strDcmM);
		double dblDcmMM = Double.parseDouble(strDcmMM);
		double dblTude = dblDcmD + dblDcmF / 60 + (dblDcmM + (dblDcmMM * 0.1))
				/ (60 * 60);
		strFloatTude = String.format("%.7f", dblTude);
		return strFloatTude;
	}

	/**
	 * 
	 * @方法说明：私有方法，一般在十进制数字转换十六进制数字的时候使用
	 * @创建日期：2013-2-1下午4:54:35
	 * @参数intData：传入16进制中，10--15数字
	 * @return说明：返回ABCDEF
	 */
	public final static String shuZhiToZhiMu(int intData) {
		String strRet = "";
		switch (intData) {
		case 10:
			strRet = "A";
			break;
		case 11:
			strRet = "B";
			break;
		case 12:
			strRet = "C";
			break;
		case 13:
			strRet = "D";
			break;
		case 14:
			strRet = "E";
			break;
		case 15:
			strRet = "F";
			break;
		default:
			strRet = "" + intData;
			break;
		}
		return strRet;
	}

	/**
	 * 
	 * @方法说明：十进制整型转十六进制字符串
	 * @创建日期：2013-2-1下午5:16:05
	 * @参数intDcmData：传入要转换的十进制整型
	 * @return说明：返回十六进制字符串
	 */
	public final static String castDcmIntToHexString(long intDcmData) {
		String str = "";
		// 1:用a去除以16，得到商和余数
		long sun = intDcmData / 16;
		int yuShu = (int) (intDcmData % 16);
		str = "" + shuZhiToZhiMu(yuShu);
		while (sun > 0) {
			// 2：继续用商去除以16，得到商和余数
			yuShu = (int) (sun % 16);
			sun = sun / 16;
			// 3：如果商为0，那么就终止
			// 4：把所有的余数倒序排列
			str = shuZhiToZhiMu(yuShu) + str;
		}
		return str;
	}

	/**
	 * 
	 * @方法说明:十进制字符串转十六进制字符串
	 * @创建日期：2013-2-1下午5:14:42
	 * @参数strDcmData：十进制数字字符串
	 * @参数intBytes：转化后的字节数
	 * @return说明：返回十六进制数据字符串
	 */
	public final static String castDcmStringToHexString(String strDcmData,
			int intBytes) {
		String strRet = null;
		long intNum = Long.parseLong(strDcmData, 10);
		String strHexData = castDcmIntToHexString(intNum);
		String strTempRet = String.valueOf(strHexData);
		int intLen = strTempRet.length();
		int intTempBytes = intBytes * 2 - intLen;
		String strTempByte = null;
		for (int i = 0; i < intTempBytes; i++) {
			if (strTempByte == null) {
				strTempByte = "0";
			} else {
				strTempByte = strTempByte + "0";
			}
		}
		if (strTempByte == null) {
			strRet = strTempRet;
		} else {
			strRet = strTempByte + strTempRet;
		}
		return strRet;
	}

	/**
	 * 
	 * @方法说明：十六进制时分秒时间转换为标准的（YY-MM-DD hh:mm:ss）年月日时分秒时间
	 * @创建日期：2013-2-4下午2:49:24
	 * @参数strHexHms：要转换的十六进制数据，长度必须为8个字符
	 * @return说明：返回YY-MM-DD hh:mm:ss格式的时间
	 */
	public final static String castHexHmsToDcmYmdhmsTime(String strHexHms) {
		String strRet = "";
		strHexHms.replace(" ", "");

		String temp_h = strHexHms.substring(0, 2);
		if (temp_h == "") {
			temp_h = "00";
		}
		String ch = castHexStringToDcmString(temp_h);
		if (ch.length() < 2 && ch.length() > 0) {
			ch = "0" + ch;
		}

		String temp_m = strHexHms.substring(2, 4);
		if (temp_m == "") {
			temp_m = "00";
		}
		String cm = castHexStringToDcmString(temp_m);

		if (cm.length() < 2 && cm.length() > 0) {
			cm = "0" + cm;
		}

		String temp_s = strHexHms.substring(4, 6);
		if (temp_s == "") {
			temp_s = "00";
		}
		String cs = castHexStringToDcmString(temp_s);
		if (cs.length() < 2 && cs.length() > 0) {
			cs = "0" + cs;
		}

		String temp_ms = strHexHms.substring(6, 8);
		if (temp_ms == "") {
			temp_ms = "00";
		}
		String cms = castHexStringToDcmString(temp_ms);
		if (cms.length() < 2 && cms.length() > 0) {
			cms = "0" + cms;
		}

		strRet = BdCommonMethod.getTime().substring(0, 11) + ch + ":" + cm + ":"
				+ cs;

		return strRet;
	}

	/**
	 * 
	 * @方法说明：此方法是将十六进制的字符串转二进制字符串
	 * @创建日期：2013-2-25下午4:12:53
	 * @参数hexString：传进来的是十六进制的字符串
	 * @return说明：返回二进制字符串
	 */
	public final static String castHexStringToBinaryString(String hexString) {
		if (hexString == null || hexString.length() % 2 != 0)
			return null;
		String bString = "", tmp;
		for (int i = 0; i < hexString.length(); i++) {
			tmp = "0000"
					+ Integer.toBinaryString(Integer.parseInt(
							hexString.substring(i, i + 1), 16));
			bString += tmp.substring(tmp.length() - 4);
		}
		return bString;
	}

	// public final static String castBinaryStringToHexString111111111 (String
	// strBinary)
	// {
	// BigInteger src = new BigInteger(strBinary, 2);
	// return src.toString();
	// }
	/**
	 * 
	 * @方法说明：此方法是将二进制字符串转十进制字符串
	 * @创建日期：2013-2-26上午11:13:11
	 * @参数strBinary：传进二进制字符串
	 * @return说明：返回十进制字符串
	 */
	public final static String castBinaryStringToDcmString(String strBinary) {
		String strRet = null;
		BigInteger src = new BigInteger(strBinary, 2);
		strRet = src.toString();
		return strRet;
	}
	

}