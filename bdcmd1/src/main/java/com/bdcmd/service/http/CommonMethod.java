package com.bdcmd.service.http;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class CommonMethod {
	/*
	 * 生成随机字符串
	 */
	public static String getRandomString(int length) {
		Random randGen = null;
		char[] numbersAndLetters = null;
		if (length < 1) {
			return null;
		}
		if (randGen == null) {
			randGen = new Random();
			numbersAndLetters = ("0123456789abcdefghijklmnopqrstuvwxyz"
					+ "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ").toCharArray();
		}
		char[] randBuffer = new char[length];
		for (int i = 0; i < randBuffer.length; i++) {
			randBuffer[i] = numbersAndLetters[randGen.nextInt(71)];
		}
		return new String(randBuffer);
	}

	/**
	 * MD5加密
	 * 
	 * @Title: getMD5
	 * @Description: TODO
	 * @param @param source
	 * @param @return
	 * @return String
	 * @throws
	 */
	public static String getMD5(byte[] source) {
		String s = null;
		char hexDigits[] = { // 用来将字节转换成 16 进制表示的字符
		'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd',
				'e', 'f' };
		try {
			java.security.MessageDigest md = java.security.MessageDigest
					.getInstance("MD5");
			md.update(source);
			byte tmp[] = md.digest(); // MD5 的计算结果是一个 128 位的长整数，
										// 用字节表示就是 16 个字节
			char str[] = new char[16 * 2]; // 每个字节用 16 进制表示的话，使用两个字符，
											// 所以表示成 16 进制需要 32 个字符
			int k = 0; // 表示转换结果中对应的字符位置
			for (int i = 0; i < 16; i++) { // 从第一个字节开始，对 MD5 的每一个字节
											// 转换成 16 进制字符的转换
				byte byte0 = tmp[i]; // 取第 i 个字节
				str[k++] = hexDigits[byte0 >>> 4 & 0xf]; // 取字节中高 4 位的数字转换,
															// >>>
															// 为逻辑右移，将符号位一起右移
				str[k++] = hexDigits[byte0 & 0xf]; // 取字节中低 4 位的数字转换
			}
			s = new String(str); // 换后的结果转换为字符串

		} catch (Exception e) {
			e.printStackTrace();
		}
		return s;
	}

	/**
	 * 根据出生日期获取年龄
	 * 
	 * @Title: getAge
	 * @Description: TODO
	 * @param @param cBirthday
	 * @param @return
	 * @param @throws Exception
	 * @return String
	 * @throws
	 */
	public static String getAge(Calendar cBirthday) throws Exception {
		Date birthDay = cBirthday.getTime();
		Calendar cal = Calendar.getInstance();

		if (cal.before(birthDay)) {
			throw new IllegalArgumentException(
					"The birthDay is before Now.It's unbelievable!");
		}

		int yearNow = cal.get(Calendar.YEAR);
		int monthNow = cal.get(Calendar.MONTH) + 1;
		int dayOfMonthNow = cal.get(Calendar.DAY_OF_MONTH);

		cal.setTime(birthDay);
		int yearBirth = cal.get(Calendar.YEAR);
		int monthBirth = cal.get(Calendar.MONTH);
		int dayOfMonthBirth = cal.get(Calendar.DAY_OF_MONTH);

		int age = yearNow - yearBirth;

		if (monthNow <= monthBirth) {
			if (monthNow == monthBirth) {
				// monthNow==monthBirth
				if (dayOfMonthNow < dayOfMonthBirth) {
					age--;
				}
			} else {
				// monthNow>monthBirth
				age--;
			}
		}

		return age + "";
	}

	public static Calendar StringToCalendar(String value) {
		if (value == null || value.equals("")) {
			return null;
		}
		Calendar dayc1 = new GregorianCalendar();
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date daystart = null;
		try {
			daystart = df.parse(value);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} // start_date是类似"2013-02-02"的字符串
		dayc1.setTime(daystart); // 得到的dayc1就是你需要的calendar了
		return dayc1;
	}

	/**
	 * @param calendar
	 * @param format
	 *            转换格式 yyyy-MM-dd HH:mm:ss
	 * @return
	 */
	public static String CalendarToString(Calendar calendar, String format) {
		if (calendar == null) {
			return null;
		}
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateStr = sdf.format(calendar.getTime());
		return dateStr;
	}

	/**
	 * @param beginCalendar
	 * @param endCalendar
	 * @return
	 */
	public static int CompareTime(Calendar beginCalendar, Calendar endCalendar) {

		long start = beginCalendar.getTimeInMillis();
		long end = endCalendar.getTimeInMillis();

		return (int) ((end - start) / 1000);

	}

	public static String getWeekDay(Calendar c) {
		if (c == null) {
			return "周一";
		}

		if (Calendar.MONDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周一";
		}
		if (Calendar.TUESDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周二";
		}
		if (Calendar.WEDNESDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周三";
		}
		if (Calendar.THURSDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周四";
		}
		if (Calendar.FRIDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周五";
		}
		if (Calendar.SATURDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周六";
		}
		if (Calendar.SUNDAY == c.get(Calendar.DAY_OF_WEEK)) {
			return "周日";
		}

		return "周一";
	}

	/**
	 * 判断两个时间是否同一天
	 * 
	 * @Title: isSameDay
	 * @Description: TODO
	 * @param @param b 时间1
	 * @param @param e 时间2
	 * @param @return
	 * @return Boolean
	 * @throws
	 */
	public static Boolean isSameDay(Calendar b, Calendar e) {
		if (b.get(Calendar.MONTH) == e.get(Calendar.MONTH)
				&& b.get(Calendar.DAY_OF_MONTH) == e.get(Calendar.DAY_OF_MONTH)) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * 找字符串
	 * 
	 * @param beginAt
	 * @param content
	 * @param beginString
	 * @param endString
	 * @return
	 */
	public static String findString(int beginAt, String content,
			String beginString, String endString) {
		int stringBegin = content.indexOf(beginString);
		int stringEnd = content.indexOf(endString);
		if (stringBegin == -1 || stringEnd == -1) {
			return null;
		}
		stringBegin = stringBegin + beginString.length();
		String result = content.substring(beginAt);
		String retrunResult = result.substring(stringBegin, stringEnd);
		return retrunResult;
	}

	public static String getTrace(Throwable t) {
		StringWriter stringWriter = new StringWriter();
		PrintWriter writer = new PrintWriter(stringWriter);
		t.printStackTrace(writer);
		StringBuffer buffer = stringWriter.getBuffer();
		return buffer.toString();
	}

	/**
	 * @param cal
	 * @param type
	 *            Calendar.YEAR，Calendar.MONTH，Calendar.DAY_OF_MONTH，Calendar.
	 *            HOUR，Calendar.MINUTE，Calendar.SECOND
	 * @return
	 */
	public static String getYearOrMonthOrDayOrHourOrMinuteOrSecond(
			Calendar cal, int type) {

		int result = cal.get(type);
		return String.valueOf(result);
	}
}
