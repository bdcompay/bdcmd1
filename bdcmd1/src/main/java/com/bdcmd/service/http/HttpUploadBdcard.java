package com.bdcmd.service.http;

import java.util.List;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.params.HttpMethodParams;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Repository;

import com.bdcmd.util.PropertyUtil;
import com.bdcmd.web.dto.CardDto;

@Repository
public class HttpUploadBdcard extends Thread {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");

	public void run() {
		try {
			Thread.sleep(60 * 1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int upLoadtime = Integer.parseInt(PropertyUtil
				.getProperty("uploadcardtime"));
		String url = PropertyUtil.getProperty("uploadcardUrl");
		String channelNumber = "";
		String bdcard = "";
		String com = "";
		List<CardDto> l = CardDto.getCardDtos();
		for (CardDto cardDto : l) {
			if (channelNumber.equals("")) {
				if (bdcard.equals("") && com.equals("")) {
					bdcard = cardDto.getServerCard();
					com = cardDto.getCom();
					channelNumber = com + ":" + bdcard;
				}

			} else {
				bdcard = cardDto.getServerCard();
				com = cardDto.getCom();
				channelNumber = channelNumber + "," + com + ":" + bdcard;
			}
		}
		while (true) {
			// TODO Auto-generated method stub
			uploadBdcard(url, channelNumber);
			try {
				Thread.sleep(upLoadtime * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	private void uploadBdcard(String url, String channelNumber) {
		forinfo.info("上传服务卡号：" + channelNumber);
		// TODO Auto-generated method stub
		try {
			HttpClient httpClient = new HttpClient();
			PostMethod post = new PostMethod(url);
			post.getParams().setParameter(
					HttpMethodParams.HTTP_CONTENT_CHARSET, "utf-8");
			post.addParameter("channelNumber", channelNumber);
			httpClient.executeMethod(post);
			String result = new String(post.getResponseBody(), "utf-8");
			System.out.println(result);
		} catch (Exception e) {
			forerror.error("上传服务卡号异常：异常信息为" + e.toString());
		}
	}
}
