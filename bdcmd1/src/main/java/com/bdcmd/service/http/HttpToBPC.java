package com.bdcmd.service.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreConnectionPNames;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bdcmd.entity.Message;
import com.bdcmd.service.IMessageService;
import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.com.CommServerService;
import com.core.Public;
@Repository
public class HttpToBPC extends WebSupport {

	// public static ConcurrentHashMap<IoSession, Subsystem> BDCmdSessionPool;//
	// ConcurrentHashMap是绝对的线程安全
	/* 以下变量为spring管理的实例，将通过构造函数进行注入 */
	@Resource
	private ApplicationConfig applicationConfig;
	@Autowired
	private IMessageService messageService;
	// private BpcComStatusController bpcComStatusController;
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");

	private final static String STATUS_NOT_EXIST = "NOT_EXIST";
	private final static String STATUS_OK = "OK";

	/*public HttpToBPC() {

		// 注入spring管理的实例
		ApplicationContext ctx = AppContext.getApplicationContext();
		applicationConfig = (ApplicationConfig) ctx
				.getBean("applicationConfig");

	}*/


	public  byte[] SendTaskApply(CommServerService commServerService) // 申请北斗数据
	{	
		byte[] bdata=null;
		String cardId=commServerService.getComStatusBean().getServerCard();
		String deviceType=commServerService.getComStatusBean().getDeviceType();
		String channelGroupName=applicationConfig.getChannelGroupName();
		int comPort=commServerService.getComStatusBean().getComPort();
		String logMessage="[发：" + Public.getsystim()
				+ "[BPC] [COM" + comPort + "][" + cardId + "]" + "[申请数据]";
		applicationConfig.getFordebug().debug(logMessage);
		commServerService.getHttpSendMassage().add(logMessage);
		String uri = applicationConfig.getUrl() + "/popData.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			//连接超时
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, 6000);
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			obj.put("serverCardNumber", cardId);
			obj.put("channelGroupName", channelGroupName);
			obj.put("deviceType", deviceType);
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				JSONObject resultJson = JSONObject
						.fromObject(result.toString());
				String status = resultJson.getString("status");
				if (status.equals(STATUS_OK)) {
					JSONObject dataJson = resultJson.getJSONObject("result");
					String dataStr = dataJson.getString("data");
					String cardIdStr = dataJson.getString("cardNumber");
					 bdata = BdCommonMethod.castHexStringToByte(dataStr);
					// 放入发送任务消息队列
					/*ToComBdDataBean toComBdDataBean = new ToComBdDataBean();
					toComBdDataBean.setBdData(bdata);
					dataCacheQueueService.addToComCacheList(comPort,
							toComBdDataBean);*/
					logMessage="[收："
							+ Public.getsystim() + "[BPC][COM" + comPort + "]["
							+ cardIdStr + "]" + "[收到发送任务数据："
							+ result.toString() + "]";
					applicationConfig.getFordebug().debug(logMessage);
					commServerService.getHttpRecieveMessage().add(logMessage);
				}

			} else {
				logMessage="[收："
						+ Public.getsystim() + "[BPC][申请数据，返回的状态有误：]"
						+ resp.getStatusLine().getStatusCode();
				applicationConfig.getFordebug().debug(logMessage);
				commServerService.getHttpRecieveMessage().add(logMessage);
			}

		} catch (Exception e) {
			logMessage="[收："
					+ Public.getsystim() + "[BPC][申请数据，发生错误：]"
					+ CommonMethod.getTrace(e);
			applicationConfig.getFordebug().debug(logMessage);
			commServerService.getHttpRecieveMessage().add(logMessage);
		}
		return bdata;
	}

	public void SendBDdata(CommServerService commServerService, byte[] bddata) // 上传北斗数据
	{	
		int comPort=commServerService.getComStatusBean().getComPort();
		String logMessage="[发：" + Public.getsystim()
				+ "[BPC] [COM" + comPort + "]" + "[上传数据]";
		applicationConfig.getFordebug().debug(logMessage);
		commServerService.getHttpSendMassage().add(logMessage);
		String uri = applicationConfig.getUrl() + "/pullBdData.do";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			httpClient.getParams().setParameter(
					CoreConnectionPNames.CONNECTION_TIMEOUT, 6000);
			httpClient.getParams().setParameter(
					CoreConnectionPNames.SO_TIMEOUT, 6000);
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			obj.put("bdChannel", "DEVICE_40");
			obj.put("dataStr", BdCommonMethod.castBytesToString(bddata));
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				JSONObject resultJson = JSONObject
						.fromObject(result.toString());
				String status = resultJson.getString("status");
				if (!status.equals(STATUS_OK)) {
					logMessage="上传数据失败，请检查原因";
					applicationConfig.getFordebug().debug(logMessage);
					commServerService.getHttpSendMassage().add(logMessage);
					//缓存发送失败数据
					Message message=new Message();
					message.setReceive_card(commServerService.getComStatusBean().getServerCard());
					message.setContent(BdCommonMethod.castBytesToString(bddata));
					messageService.save(message);
				}

			} else {
				logMessage="上传数据失败，请检查原因"
						+ resp.getStatusLine().getStatusCode();
				applicationConfig.getFordebug().debug(logMessage);
				commServerService.getHttpSendMassage().add(logMessage);
				//缓存发送失败数据
				Message message=new Message();
				message.setReceive_card(commServerService.getComStatusBean().getServerCard());
				message.setContent(BdCommonMethod.castBytesToString(bddata));
				messageService.save(message);
			}

		} catch (Exception e) {
			logMessage="上传数据失败，请检查原因"
					+ CommonMethod.getTrace(e);
			applicationConfig.getFordebug().debug(logMessage);
			commServerService.getHttpSendMassage().add(logMessage);
			//缓存发送失败数据
			Message message=new Message();
			message.setReceive_card(commServerService.getComStatusBean().getServerCard());
			message.setContent(BdCommonMethod.castBytesToString(bddata));
			messageService.save(message);
		}
	}

}