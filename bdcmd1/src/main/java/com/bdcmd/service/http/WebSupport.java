package com.bdcmd.service.http;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.junit.AfterClass;
import org.springframework.beans.factory.annotation.Autowired;
import com.bdcmd.service.application.ApplicationConfig;

public class WebSupport {

	@Autowired
	private ApplicationConfig applicationConfig;
	private static HttpClient httpClient;

	public void init() {
		httpClient = new DefaultHttpClient();

	}

	protected String doPost(String name, String string) {
		HttpPost post = new HttpPost(applicationConfig.getUrl() + name);

		HttpEntity stringEntity = new StringEntity(string,
				ContentType.APPLICATION_JSON);
		post.setEntity(stringEntity);

		HttpResponse response;
		String resp = null;
		try {
			response = httpClient.execute(post);

			HttpEntity responseEntity = response.getEntity();
			resp = EntityUtils.toString(responseEntity);
		} catch (ClientProtocolException e) {
			System.out.println(CommonMethod.getTrace(e));
		} catch (IOException e) {
			System.out.println(CommonMethod.getTrace(e));
		}

		return resp;
	}

	protected String doGet(String string) {
		HttpGet get = new HttpGet(applicationConfig.getUrl() + string);
		String resp = null;
		HttpResponse response = null;
		try {
			response = httpClient.execute(get);
			HttpEntity responseEntity = response.getEntity();
			resp = EntityUtils.toString(responseEntity);
		} catch (ClientProtocolException e) {
			System.out.println(CommonMethod.getTrace(e));
		} catch (IOException e) {
			System.out.println(CommonMethod.getTrace(e));
		}

		return resp;
	}

	@AfterClass
	public static void close() {
	}
}
