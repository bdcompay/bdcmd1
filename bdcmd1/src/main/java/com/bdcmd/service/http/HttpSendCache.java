package com.bdcmd.service.http;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

import javax.annotation.Resource;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.bdcmd.entity.Message;
import com.bdcmd.service.IMessageService;
import com.bdcmd.service.application.ApplicationConfig;
import com.core.Public;

@Repository
public class HttpSendCache extends Thread {
	@Autowired
	private IMessageService messageService;
	@Resource
	private ApplicationConfig applicationConfig;
	private final static String STATUS_NOT_EXIST = "NOT_EXIST";
	private final static String STATUS_OK = "OK";

	public void run() {
		while (true) {
			// TODO Auto-generated method stub
			SendBDdata();
			try {
				Thread.sleep(applicationConfig.getSendCacheBpcDataTime() * 1000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public void SendBDdata() // 上传本机缓存数据
	{
		List<Message> MessageList = messageService.getMessageList(20);
		if (MessageList != null) {
			for (int i = 0; i < MessageList.size(); i++) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Message message = MessageList.get(i);
				String bddata = message.getContent();

				String logMessage = "[发：" + Public.getsystim() + "[BPC]"
						+ "[上传缓存数据数据]";
				String uri = applicationConfig.getUrl() + "/pullBdData.do";
				try {
					HttpClient httpClient = new DefaultHttpClient();
					// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承
					// HttpRequest
					HttpPost httpPostReq = new HttpPost(uri);
					JSONObject obj = new JSONObject();
					obj.put("bdChannel", "DEVICE_40");
					obj.put("dataStr", bddata);
					StringEntity s = new StringEntity(obj.toString());
					s.setContentEncoding("UTF-8");
					s.setContentType("application/json");
					httpPostReq.setEntity(s);
					HttpResponse resp = httpClient.execute(httpPostReq);

					if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
						BufferedReader reader = new BufferedReader(
								new InputStreamReader(resp.getEntity()
										.getContent()));
						StringBuffer result = new StringBuffer();
						String inputLine = null;
						while ((inputLine = reader.readLine()) != null) {
							result.append(inputLine);
						}
						JSONObject resultJson = JSONObject.fromObject(result
								.toString());
						String status = resultJson.getString("status");
						if (!status.equals(STATUS_OK)) {
							logMessage = "上传数据失败，请检查原因";
							applicationConfig.getFordebug().debug(logMessage);
						} else {
							// 发送成功则删除该数据
							messageService.delete(message);
						}

					} else {
						logMessage = "上传数据失败，请检查原因"
								+ resp.getStatusLine().getStatusCode();
						applicationConfig.getFordebug().debug(logMessage);
					}

				} catch (Exception e) {
					logMessage = "上传数据失败，请检查原因" + CommonMethod.getTrace(e);
					applicationConfig.getFordebug().debug(logMessage);
				}
			}
		}
	}
}
