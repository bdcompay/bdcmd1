package com.bdcmd.service;

import java.util.List;

import com.bdcmd.entity.Card;
import com.bdcmd.web.dto.CardDto;

public interface ICardService {
	public List<CardDto> getCardList(int id);
	
	//根据comname查询com
	public CardDto querycom(String comName);
	//开启com
	public CardDto startCOm(String comName);
	//关闭com
	public CardDto closeCom(String comName);
	
	public List<Card> getCardList();
}
