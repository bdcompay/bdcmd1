package com.bdcmd.service;


import java.util.List;

import com.bdcmd.entity.Comm;
import com.bdcmd.web.dto.CommDto;

public interface ICommService {
	public Comm getComm(String com);
	
	public void saveComm(Comm comm);
	
	public void updateComm(Comm comm);
	
	public void deleComm(Comm comm);
	
	/**
	 * 获取所有没有被绑定的COM
	 */
	public List<CommDto> getNoBindComsDto();
}
