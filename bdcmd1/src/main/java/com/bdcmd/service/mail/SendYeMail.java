package com.bdcmd.service.mail;


import javax.mail.internet.MimeMessage;

import org.springframework.context.ApplicationContext;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.bdcmd.service.application.AppContext;
import com.bdcmd.util.PropertyUtil;

public class SendYeMail {
	//发送邮件方法
	static String SytemName = PropertyUtil.getProperty("system.name");
	static String mailname = PropertyUtil.getProperty("email.username");
	
	public static void sendmail(String content){
		
		String reciever=PropertyUtil.getProperty("email.reciever");
		String[] recievers=reciever.split(",");
		
		
		try {
			// 注入spring管理的实例
			ApplicationContext ctx = AppContext.getApplicationContext();
			JavaMailSenderImpl javaMailSender = (JavaMailSenderImpl) ctx.getBean("javaMailSender");
			MimeMessage mailMessage = javaMailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true,
					"utf-8");
			SimpleMailMessage mailMessage1 = new SimpleMailMessage();
			mailMessage1.setTo(recievers);
			mailMessage1.setSubject(SytemName+"系统提示");
			mailMessage1.setFrom(mailname);
			mailMessage1.setText(content);
			// 接收者
			helper.setTo(mailMessage1.getTo());
			if (mailMessage1.getSubject() != null) {
				// 邮件主题
				helper.setSubject(mailMessage1.getSubject());
			}
			if (mailMessage1.getFrom() != null) {
				// 发送者
				helper.setFrom(mailMessage1.getFrom(), "短信猫系统");
			}
			javaMailSender.send(mailMessage1);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
		
		/**
		 * 
		 * @param tittle 邮件标题
		 * @param content 邮件内容
		 */
		public static void sendmail(String tittle,String content){
			
			String reciever=PropertyUtil.getProperty("email.reciever");
			String[] recievers=reciever.split(",");
			
			
			try {
				// 注入spring管理的实例
				ApplicationContext ctx = AppContext.getApplicationContext();
				JavaMailSenderImpl javaMailSender = (JavaMailSenderImpl) ctx.getBean("javaMailSender");
				MimeMessage mailMessage = javaMailSender.createMimeMessage();
				MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true,
						"utf-8");
				SimpleMailMessage mailMessage1 = new SimpleMailMessage();
				mailMessage1.setTo(recievers);
				mailMessage1.setSubject(tittle);
				mailMessage1.setFrom(mailname);
				mailMessage1.setText(content);
				// 接收者
				helper.setTo(mailMessage1.getTo());
				if (mailMessage1.getSubject() != null) {
					// 邮件主题
					helper.setSubject(mailMessage1.getSubject());
				}
				if (mailMessage1.getFrom() != null) {
					// 发送者
					helper.setFrom(mailMessage1.getFrom(), SytemName);
				}
				javaMailSender.send(mailMessage1);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}
}
