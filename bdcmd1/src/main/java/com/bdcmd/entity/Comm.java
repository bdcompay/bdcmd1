package com.bdcmd.entity;

import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bdcmd_Comm")
public class Comm {
	private int id;
	private boolean isOpen = false;// 是否打开
	
	private boolean isApply = false;// 是否在申请下发数据
	
	private Timestamp lastGlTime = new Timestamp(System.currentTimeMillis());// 功率最后更新时间
	private int glFreq = 10;// 功率监测频度 默认10s
	private String com;// com口名称
	private String deviceType = "";// 协议
	private String serverCard = "";// 服务卡号
	private String freq = "";// 服务频度
	private String status = "未知";
	private String sentCount = "0";
	private String revCount = "0";
	private String entName;//企业名称
	private String channelGroupName;
	private boolean isBindGroup;
	private Integer rdGroupCell;

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public boolean isOpen() {
		return isOpen;
	}
	public void setOpen(boolean isOpen) {
		this.isOpen = isOpen;
	}
	public boolean isApply() {
		return isApply;
	}
	public void setApply(boolean isApply) {
		this.isApply = isApply;
	}
	public Timestamp getLastGlTime() {
		return lastGlTime;
	}
	public void setLastGlTime(Timestamp lastGlTime) {
		this.lastGlTime = lastGlTime;
	}
	public int getGlFreq() {
		return glFreq;
	}
	public void setGlFreq(int glFreq) {
		this.glFreq = glFreq;
	}
	public String getCom() {
		return com;
	}
	public void setCom(String com) {
		this.com = com;
	}
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	public String getServerCard() {
		return serverCard;
	}
	public void setServerCard(String serverCard) {
		this.serverCard = serverCard;
	}
	public String getFreq() {
		return freq;
	}
	public void setFreq(String freq) {
		this.freq = freq;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getSentCount() {
		return sentCount;
	}
	public void setSentCount(String sentCount) {
		this.sentCount = sentCount;
	}
	public String getRevCount() {
		return revCount;
	}
	public void setRevCount(String revCount) {
		this.revCount = revCount;
	}
	public String getChannelGroupName() {
		return channelGroupName;
	}
	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}
	public boolean getBindGroup() {
		return isBindGroup;
	}
	public void setBindGroup(boolean isBindGroup) {
		this.isBindGroup = isBindGroup;
	}
	public boolean isBindGroup() {
		return isBindGroup;
	}
	public Integer getRdGroupCell() {
		return rdGroupCell;
	}
	public void setRdGroupCell(Integer rdGroupCell) {
		this.rdGroupCell = rdGroupCell;
	}
	public String getEntName() {
		return entName;
	}
	public void setEntName(String entName) {
		this.entName = entName;
	}
	
}
