package com.bdcmd.entity;

import java.util.Calendar;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;



@Entity
@Table(name="b_rd_group")
public class RdGroup {
	/**
	 * id
	 */
	@Id
	@GeneratedValue
	private int id;
	/**
	 * name
	 */
	private String name;
	/**
	 * 单元1
	 */
	private String cell1;
	/**
	 * 单元2
	 */
	private String cell2;
	/**
	 * 单元3
	 */
	private String cell3;
	/**
	 * 单元4
	 */
	private String cell4;
	/**
	 * 单元5
	 */
	private String cell5;
	/**
	 * 单元6
	 */
	private String cell6;
	/**
	 * 单元7
	 */
	private String cell7;
	/**
	 * 单元8
	 */
	private String cell8;
	/**
	 * 创建时间
	 */
	 private Calendar createdTime;
	 /**
	  * 最后更新时间
	  */
	 private long lastUpdateTime = 0;
	 /**
	  * 发射频度(毫秒),默认为1秒
	  */
	 private long sendfreq = 1000;
	 /**
	  * 备注说明
	  */
	 private String explain1;
	 
	
	
	/**************************************  分割线  ********************************************/
	
	 public int getId() {
			return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCell1() {
		return cell1;
	}
	public void setCell1(String cell1) {
		this.cell1 = cell1;
	}
	public String getCell2() {
		return cell2;
	}
	public void setCell2(String cell2) {
		this.cell2 = cell2;
	}
	public String getCell3() {
		return cell3;
	}
	public void setCell3(String cell3) {
		this.cell3 = cell3;
	}
	public String getCell4() {
		return cell4;
	}
	public void setCell4(String cell4) {
		this.cell4 = cell4;
	}
	public String getCell5() {
		return cell5;
	}
	public void setCell5(String cell5) {
		this.cell5 = cell5;
	}
	public String getCell6() {
		return cell6;
	}
	public void setCell6(String cell6) {
		this.cell6 = cell6;
	}
	public String getCell7() {
		return cell7;
	}
	public void setCell7(String cell7) {
		this.cell7 = cell7;
	}
	public String getCell8() {
		return cell8;
	}
	public void setCell8(String cell8) {
		this.cell8 = cell8;
	}
	public Calendar getCreatedTime() {
		return createdTime;
	}
	public void setCreatedTime(Calendar createdTime) {
		this.createdTime = createdTime;
	}
	public String getExplain1() {
		return explain1;
	}
	public void setExplain1(String explain1) {
		this.explain1 = explain1;
	}
	public long getLastUpdateTime() {
		return lastUpdateTime;
	}
	public void setLastUpdateTime(long lastUpdateTime) {
		this.lastUpdateTime = lastUpdateTime;
	}
	public long getSendfreq() {
		return sendfreq;
	}
	public void setSendfreq(long sendfreq) {
		this.sendfreq = sendfreq;
	}
}
