package com.bdcmd.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "bdcmd_card")
public class Card {
	private int id;
	private int bm_card;//卡号
	private String bm_com;//com口号
	private int bm_freq;//频度
	private String type;//类型级别
	
	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBm_card() {
		return bm_card;
	}
	public void setBm_card(int bm_card) {
		this.bm_card = bm_card;
	}
	public String getBm_com() {
		return bm_com;
	}
	public void setBm_com(String bm_com) {
		this.bm_com = bm_com;
	}
	public int getBm_freq() {
		return bm_freq;
	}
	public void setBm_freq(int bm_freq) {
		this.bm_freq = bm_freq;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
}
