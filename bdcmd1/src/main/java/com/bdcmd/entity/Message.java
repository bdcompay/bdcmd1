package com.bdcmd.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "bdcmd_miss_message")
public class Message {
	private int id;
	private String content;
	private String receive_card;

	@Id
	@GeneratedValue
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getReceive_card() {
		return receive_card;
	}
	public void setReceive_card(String receive_card) {
		this.receive_card = receive_card;
	}
	
}
