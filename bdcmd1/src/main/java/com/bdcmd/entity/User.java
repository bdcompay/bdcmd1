package com.bdcmd.entity;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name = "bdcmd_user")
public class User {
	@Id
	@GeneratedValue
	private long id;
	/**
	 * 登录名
	 * 
	 */
	@Index(name = "Index_username")
	private String username;
	/**
	 * 登录密码
	 * 
	 */
	@Index(name = "Index_password")
	private String password;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
}