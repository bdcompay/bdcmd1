package com.bdcmd.control;

import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.bdcmd.service.com.BpcComStatusController;
import com.bdcmd.service.com.CommServerService;
import com.bdcmd.service.mail.SendYeMail;
import com.bdcmd.util.LogUtils;

/**
 * 监视系统中需要特别关注的功能
 * 
 * @author jlj
 *
 */
@Component
public class ControlSystemMonitor {
	/*
	 * com口控制类
	 */
	@Autowired
	private BpcComStatusController bpcComStatusController;
	
	//监视获取发送任务线程
	@Scheduled(cron="0 0/10 * * * ?")
	public void getSendTaskMonitor(){
		try{
			/*
			 * 1.设置监视标志
			 * 2.根据标记判断是否重启线程
			 */
			Map<String,Long> applySendTaskTimeMap = CommServerService.applySendTaskTimeMap;
			Set<String> set =  BpcComStatusController.commServerServiceMap.keySet();
			for(String comName:set){
				if(applySendTaskTimeMap.containsKey(comName)){
					long curtime = System.currentTimeMillis();
					long time = applySendTaskTimeMap.get(comName);
					//超过十分钟，重启com
					if(curtime>(time+1000*60*10)){
						String result = bpcComStatusController.closePort(comName);
						boolean b = result.contains("成功关闭");
						if(b){
							bpcComStatusController.openPort(comName);
							SendYeMail.sendmail("RD通道机-短报文发送任务线程监视器", "RD通道"+comName+"重启成功");
						}
					}
				}
			}
			LogUtils.loginfo("定时10分钟，短报文发送任务线程数量："+applySendTaskTimeMap.size());
		}catch(Exception e){
			LogUtils.logerror("监视获取发送任务线程错误", e);
		}
		
	}
}
