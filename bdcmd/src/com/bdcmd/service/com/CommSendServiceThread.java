/**  
 * @Title: CommServiceThread.java 
 * @Package com.bdcmd.service.com 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author A18ccms A18ccms_gmail_com  
 * @date 2013-6-9 上午12:34:02 
 * @version V1.0  
 */

package com.bdcmd.service.com;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import com.bdcmd.service.application.AppContext;
import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.com.bean.CommSerialConfigBean;
import com.bdcmd.service.com.bean.ToComBdDataBean;
import com.bdcmd.service.server.socket.MinaClientHandler;
import com.bdcmd.swing.BpcComStatusController;
import com.bdcmd.swing.BpcComStatusController.ComStatusBean;
import com.core.Public;

/**
 * 项目名称：bdcmdCom 类名称：CommSendServiceThread 类描述： 发送数据 创建人：will.yan 创建时间：2013-6-18
 * 下午3:18:08
 * 
 * @version
 */
public class CommSendServiceThread extends Thread {
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private DataCacheQueueService dataCacheQueueService;
	private MinaClientHandler minaClientHandler;
	private ApplicationConfig applicationConfig;
	private CommServerService commServerService;
	private BpcComStatusController bpcComStatusController;

	public CommSendServiceThread() {
		ApplicationContext ctx = AppContext.getApplicationContext();
		dataCacheQueueService = (DataCacheQueueService) ctx
				.getBean("dataCacheQueueService");
		commServerService = (CommServerService) ctx
				.getBean("commServerService");
		minaClientHandler = (MinaClientHandler) ctx
				.getBean("minaClientHandler");
		applicationConfig = (ApplicationConfig) ctx
				.getBean("applicationConfig");
		bpcComStatusController = (BpcComStatusController) ctx
				.getBean("bpcComStatusController");

	}

	public void run() {
		while (true) {
			try {
				for (final SerialDevice serialDevice : commServerService
						.getSerialDeviceList()) {
					int comid = serialDevice.getCommDataObserver().getComPort();
					final ComStatusBean comStatusBean = bpcComStatusController
							.getComStatusBean(comid);
					if (!comStatusBean.isBusy() && comStatusBean.isOpen()
							&& comStatusBean.isNormal()) {
						ToComBdDataBean toComBdDataBean = dataCacheQueueService
								.getToComCache(serialDevice
										.getCommDataObserver().getComPort());
						if (toComBdDataBean != null) {
							// 发送数据到串口
							bpcComStatusController.sendComData(serialDevice,
									toComBdDataBean.getBdData());
							comStatusBean.addSendCount();// 增加发送数量
							// 发送后启动倒计时线程
							comStatusBean.setBusy(true);
							comStatusBean.startBusyfreq();
						} else {// 向BPC申请数据
							if (!comStatusBean.isApply()) {
								Thread applyDataThread = new Thread() {
									public void run() {
										try {
											CommSerialConfigBean commSerialConfigBean = serialDevice
													.getCommSerialConfigBean();
											minaClientHandler
													.SendTaskApply(
															commSerialConfigBean
																	.getCardId(),
															commSerialConfigBean
																	.getChannelType(),
															applicationConfig
																	.getChannelGroupName(),
															commSerialConfigBean
																	.getComPort());
											comStatusBean.setApply(false);
										} catch (Exception e) {
											// TODO: handle exception
											comStatusBean.setApply(false);
										}

									}
								};
								comStatusBean.setApply(true);
								applyDataThread.start();
							}

						}
					}
				}
				sleep(applicationConfig.getCheckBpcDataTime());
			} catch (Exception e) {
				String bpclog = "[" + Public.getsystim()
						+ "][监控串口发送的缓存区数据时，发送错误]";
				applicationConfig.addData_ListModel_bpc_data(bpclog);
				forerror.error(bpclog);
				e.printStackTrace();
			}
		}

	}

}
