package com.bdcmd.service.com;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TooManyListenersException;

import javax.annotation.Resource;
import javax.comm.*;
import org.apache.log4j.Logger;

import com.bdcmd.service.application.AppContext;
import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.com.bean.CommSerialConfigBean;
import com.bdcmd.swing.MusicManager;
import com.bdcmd.swing.bean.ComPanelBean;
import com.core.PathUtil;

public class CommServerService {

	@Resource
	ApplicationConfig applicationConfig;

	private List<SerialDevice> serialDeviceList;// 串口设备类列表
	// 开启串口数据扫描线程，假如串口有完整数据则发送到BPC
	private CommRevServiceThread commRevServiceThread;
	// 开启缓存数据扫描线程，假如有缓存数据，则发送到串口
	private CommSendServiceThread commSendServiceThread;

	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");
	private MusicManager musicManager;
	private CommDriver commDriver = null;

	public CommServerService() {
		serialDeviceList = new ArrayList<SerialDevice>();
		musicManager = new MusicManager();
	}

	@SuppressWarnings("unchecked")
	public void init() {
		String driverName = "com.sun.comm.Win32Driver";

		try {
			System.loadLibrary("win32com");
			commDriver = (CommDriver) Class.forName(driverName).newInstance();
			commDriver.initialize();
		} catch (InstantiationException e1) {
			System.out.println(e1);

		} catch (IllegalAccessException e1) {
			System.out.println(e1);

		} catch (ClassNotFoundException e1) {
			System.out.println(e1);

		}
		commRevServiceThread = new CommRevServiceThread();
		commSendServiceThread = new CommSendServiceThread();
		commRevServiceThread.start();
		commSendServiceThread.start();

	}

	/**
	 * @Title: openCom
	 * @Description: TODO(打开串口)
	 * @param @param comid 串口号
	 * @param @return
	 * @return boolean 返回类型
	 */
	public ComPanelBean openCom(int comid) {

		CommSerialConfigBean commSerialConfigBean = applicationConfig
				.getCommSerialConfigBean(comid);
		ComPanelBean comPanelBean = new ComPanelBean();
		try {

			SerialDevice serialDevice = new SerialDevice(commSerialConfigBean);
			serialDevice.openPort();
			addSerialDeviceList(serialDevice);
			comPanelBean.setRetrunValue(true);
			comPanelBean.setReturnLog("成功打开串口COM" + comid);
			return comPanelBean;
		} catch (PortInUseException e) {
			comPanelBean.setRetrunValue(false);
			comPanelBean.setReturnLog("打开串口COM" + comid + "失败：" + "COM" + comid
					+ ":端口已经被占用!" + e.getMessage());
			forerror.error("COM" + comid + ":端口已经被占用!" + e.getMessage());
			return comPanelBean;
		} catch (TooManyListenersException e) {
			comPanelBean.setRetrunValue(false);
			comPanelBean.setReturnLog("打开串口COM" + comid + "失败：" + "COM" + comid
					+ ":端口监听者过多!" + e.getMessage());
			forerror.error("COM" + comid + ":端口监听者过多!" + e.getMessage());
			return comPanelBean;
		} catch (UnsupportedCommOperationException e) {
			comPanelBean.setRetrunValue(false);
			comPanelBean.setReturnLog("打开串口COM" + comid + "失败：" + "COM" + comid
					+ ":端口操作命令不支持!" + e.getMessage());
			forerror.error("COM" + comid + ":端口操作命令不支持!" + e.getMessage());
			return comPanelBean;
		} catch (NoSuchPortException e) {
			comPanelBean.setRetrunValue(false);
			comPanelBean.setReturnLog("打开串口COM" + comid + "失败：" + "COM" + comid
					+ ":端口不存在!" + e.getMessage());
			forerror.error("COM" + comid + ":端口不存在!" + e.getMessage());
			return comPanelBean;
		} catch (IOException e) {
			comPanelBean.setRetrunValue(false);
			comPanelBean.setReturnLog("打开串口COM" + comid + "失败：" + "COM" + comid
					+ ":其它异常!" + e.getMessage());
			forerror.error("COM" + comid + ":其它异常!" + e.getMessage());
			return comPanelBean;
		}

	}

	/**
	 * @Title: closeCom
	 * @Description: TODO(关闭串口)
	 * @param @param comid
	 * @param @return
	 * @return boolean 返回类型
	 */
	public ComPanelBean closeCom(int comid) {
		ComPanelBean comPanelBean = new ComPanelBean();
		SerialDevice serialDevice = getSerialDevice(comid);

		if (serialDevice == null) {
			comPanelBean.setRetrunValue(false);
			comPanelBean.setReturnLog("串口COM" + comid + "不存在或未打开！");
			return comPanelBean;
		} else {
			if (serialDevice.closePort()) {
				remove(serialDevice);
				comPanelBean.setRetrunValue(true);
				comPanelBean.setReturnLog("成功关闭串口COM" + comid + "！");
				return comPanelBean;
			} else {
				comPanelBean.setRetrunValue(false);
				comPanelBean.setReturnLog("关闭串口COM" + comid + "失败，发生未知错误！");
				return comPanelBean;
			}

		}
	}

	public List<SerialDevice> getSerialDeviceList() {
		return serialDeviceList;
	}

	public void addSerialDeviceList(SerialDevice serialDevice) {
		synchronized (serialDeviceList) {
			serialDeviceList.add(serialDevice);
		}
	}

	public void remove(SerialDevice serialDevice) {
		synchronized (serialDeviceList) {
			serialDeviceList.remove(serialDevice);
		}

	}

	public SerialDevice getSerialDevice(int comid) {
		for (SerialDevice serialDevice : serialDeviceList) {
			if (serialDevice.getCommDataObserver().getComPort() == comid) {
				return serialDevice;
			}
		}
		return null;

	}

	public void destory() {
		System.out.println("destory");
	}

	public void playAlertMusic() {
		musicManager.play(applicationConfig.getAlertMusicPath());
	}

	public void stopAlertMusic() {
		musicManager.stop();
	}
}
