package com.bdcmd.service.com;

import java.io.*;
import java.util.*;

import javax.comm.*;

import com.bdcmd.service.com.bean.CommSerialConfigBean;

/**
 * 项目名称：bdcmdCom 类名称：SerialDevice 类描述： 串口设备类，一个实体代表一个串口 创建人：will.yan
 * 创建时间：2013-6-19 上午9:18:03
 * 
 * @version
 */
public class SerialDevice extends Observable implements Runnable,
		SerialPortEventListener {
	private CommDataObserver commDataObserver;
	private CommSerialConfigBean commSerialConfigBean;
	private static CommPortIdentifier portId;
	private int delayRead = 200;
	private int numBytes; // buffer中的实际数据字节数
	private static byte[] readBuffer = new byte[4096]; // 4k的buffer空间,缓存串口读入的数据
	private InputStream inputStream;
	private OutputStream outputStream;
	private SerialPort serialPort;

	/**
	 * 初始化端口操作的参数.
	 * 
	 * @see
	 */
	@SuppressWarnings("unchecked")
	public SerialDevice(CommSerialConfigBean commSerialConfigBean) {
		this.commSerialConfigBean = commSerialConfigBean;

	}

	private void init() throws NoSuchPortException, IOException,
			PortInUseException, TooManyListenersException,
			UnsupportedCommOperationException {

		// 参数初始化
		int timeout = 100;
		int rate = commSerialConfigBean.getBaudrate();
		int dataBits = commSerialConfigBean.getDatabits();
		int stopBits = commSerialConfigBean.getStopbits();
		int parity = commSerialConfigBean.getParity();
		delayRead = 20;
		String port = "COM" + commSerialConfigBean.getComPort();
		// 打开端口
		portId = CommPortIdentifier.getPortIdentifier(port);
		serialPort = (SerialPort) portId.open("SerialReader", timeout);
		inputStream = serialPort.getInputStream();
		outputStream = serialPort.getOutputStream();
		serialPort.addEventListener(this);
		serialPort.notifyOnDataAvailable(true);
		serialPort.setSerialPortParams(rate, dataBits, stopBits, parity);
		System.out.println("成功开启端口：" + commDataObserver.getComPort());
		Thread readThread = new Thread(this);
		readThread.start();
	}

	/**
	 * Method declaration
	 * 
	 * @see
	 */
	public void run() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
		}
	}

	/**
	 * Method declaration
	 * 
	 * @param event
	 * @see
	 */
	public void serialEvent(SerialPortEvent event) {
		try {
			// 等待delayRead秒钟让串口把数据全部接收后在处理
			Thread.sleep(delayRead);
			System.out.print("serialEvent[" + event.getEventType() + "] ");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		switch (event.getEventType()) {
		case SerialPortEvent.BI: // 10
		case SerialPortEvent.OE: // 7
		case SerialPortEvent.FE: // 9
		case SerialPortEvent.PE: // 8
		case SerialPortEvent.CD: // 6
		case SerialPortEvent.CTS: // 3
		case SerialPortEvent.DSR: // 4
		case SerialPortEvent.RI: // 5
		case SerialPortEvent.OUTPUT_BUFFER_EMPTY: // 2
			break;
		case SerialPortEvent.DATA_AVAILABLE: // 1
			try {
				// 多次读取,将所有数据读入
				// while (inputStream.available() > 0) {
				// numBytes = inputStream.read(readBuffer);
				// }
				numBytes = inputStream.read(readBuffer);

				changeMessage(readBuffer, numBytes);
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		}
	}

	// 通过observer pattern将收到的数据发送给observer
	// 将buffer中的空字节删除后再发送更新消息,通知观察者
	public void changeMessage(byte[] message, int length) {
		setChanged();
		byte[] temp = new byte[length];
		System.arraycopy(message, 0, temp, 0, length);

		notifyObservers(temp);
	}

	/** 显示系统端口清单信息 */
	@SuppressWarnings("unchecked")
	static void listPorts() {
		Enumeration portEnum = CommPortIdentifier.getPortIdentifiers();
		while (portEnum.hasMoreElements()) {
			CommPortIdentifier portIdentifier = (CommPortIdentifier) portEnum
					.nextElement();
			System.out.println(portIdentifier.getName() + " - "
					+ getPortTypeName(portIdentifier.getPortType()));
		}
	}

	static String getPortTypeName(int portType) {
		switch (portType) {
		case CommPortIdentifier.PORT_PARALLEL:
			return "Parallel";
		case CommPortIdentifier.PORT_SERIAL:
			return "Serial";
		default:
			return "unknown type(未知端口类型)";
		}
	}

	/**
	 * 
	 * @return A HashSet containing the CommPortIdentifier for all serial ports
	 *         that are not currently being used.(返回未被使用的端口)
	 */
	@SuppressWarnings("unchecked")
	public static HashSet<CommPortIdentifier> getAvailableSerialPorts() {
		HashSet<CommPortIdentifier> h = new HashSet<CommPortIdentifier>();
		Enumeration thePorts = CommPortIdentifier.getPortIdentifiers();
		while (thePorts.hasMoreElements()) {
			CommPortIdentifier com = (CommPortIdentifier) thePorts
					.nextElement();
			switch (com.getPortType()) {
			case CommPortIdentifier.PORT_SERIAL:
				try {
					CommPort thePort = com.open("CommUtil", 50);
					thePort.close();
					h.add(com);
				} catch (PortInUseException e) {
					System.out.println("Port, " + com.getName()
							+ ", is in use.");
				} catch (Exception e) {
					System.out.println("Failed to open port " + com.getName()
							+ e);
				}
			}
		}

		return h;
	}

	public CommDataObserver getCommDataObserver() {
		return commDataObserver;
	}

	public void openPort() throws NoSuchPortException, IOException,
			PortInUseException, TooManyListenersException,
			UnsupportedCommOperationException {
		this.commDataObserver = new CommDataObserver();
		this.commDataObserver.setComPort(commSerialConfigBean.getComPort());
		this.addObserver(commDataObserver);
		init();
	}

	public OutputStream getOutputStream() {
		return outputStream;
	}

	public boolean closePort() {
		try {
			serialPort.close();
			serialPort = null;
			return true;
		} catch (Exception e) {
			System.out.println(e.toString());
			return false;
		}

	}

	public CommSerialConfigBean getCommSerialConfigBean() {
		return commSerialConfigBean;
	}

	public void setCommSerialConfigBean(
			CommSerialConfigBean commSerialConfigBean) {
		this.commSerialConfigBean = commSerialConfigBean;
	}

}
