/**  
* @Title: ToComBdDataBean.java 
* @Package com.bdcmd.service.com 
* @Description: TODO(用一句话描述该文件做什么) 
* @author A18ccms A18ccms_gmail_com  
* @date 2013-6-7 下午2:07:59 
* @version V1.0  
*/ 

package com.bdcmd.service.com.bean;

/**   
 * 项目名称：bdcmdCom   
 * 类名称：ToComBdDataBean   
 * 类描述：   
 * 创建人：will.yan
 * 创建时间：2013-6-7 下午2:07:59    
 * @version    
 */

public class ToComBdDataBean {
	private byte[] bdData;
	public byte[] getBdData() {
		return bdData;
	}
	public void setBdData(byte[] bdData) {
		this.bdData = bdData;
	}

	
}
