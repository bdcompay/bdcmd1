/**  
 * @Title: CommServiceThread.java 
 * @Package com.bdcmd.service.com 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author A18ccms A18ccms_gmail_com  
 * @date 2013-6-9 上午12:34:02 
 * @version V1.0  
 */

package com.bdcmd.service.com;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.bdcmd.service.application.AppContext;
import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.server.socket.MinaClientHandler;
import com.bdcmd.swing.BpcComStatusController;
import com.bdcmd.swing.BpcComStatusController.ComStatusBean;
import com.core.Public;
import com.pro.Analysis;

/**
 * 项目名称：bdcmdCom 类名称：CommServiceThread 类描述： 创建人：will.yan 创建时间：2013-6-9
 * 上午12:34:02
 * 
 * @version
 */

public class CommRevServiceThread extends Thread {

	private MinaClientHandler minaClientHandler;
	private CommServerService commServerService;
	private ApplicationConfig applicationConfig;
	private BpcComStatusController bpcComStatusController;
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");

	public CommRevServiceThread() {
		ApplicationContext ctx = AppContext.getApplicationContext();
		commServerService = (CommServerService) ctx
				.getBean("commServerService");
		minaClientHandler = (MinaClientHandler) ctx
				.getBean("minaClientHandler");
		applicationConfig = (ApplicationConfig) ctx
				.getBean("applicationConfig");
		bpcComStatusController = (BpcComStatusController) ctx
				.getBean("bpcComStatusController");
	}

	public void run() {
		while (true) {
			try {
				for (SerialDevice serialDevice : commServerService
						.getSerialDeviceList()) {
					byte[] proData = serialDevice.getCommDataObserver()
							.getProData();
					if (proData != null) {
						int comid = serialDevice.getCommDataObserver()
								.getComPort();
						// 判断是否为功率信息
						String strData = Analysis.ByteHexToString(proData,
								proData.length);
						if (strData.indexOf("245369675F") != -1
								|| strData.indexOf("24474C5A4B") != -1) {
							bpcComStatusController.setGLStatus(strData, comid);
							if (applicationConfig.isPostGl()) {// 是否将功率信息上传到BPC服务器
								try {
									// // 封装发送到BPC
									minaClientHandler
											.SendBDdata(comid, proData);
								} catch (Exception e) {
									String logString = "[" + Public.getsystim()
											+ " 监控串口接收的缓存区数据时，发送到BPC错误]";
									forerror.error(logString);
									applicationConfig
											.addData_ListModel_bpc_data(logString);
								}
							}
							if (applicationConfig.isShowGl()) {// 是否在列表显示功率信息
								String revLogString = "[收："
										+ Public.getsystim() + " Com" + comid
										+ "]  [Data:" + strData + "]";
								forinfo.info(revLogString);
								applicationConfig
										.addData_ListModel_com_rev(revLogString);
							}
						} else if (strData.indexOf("245273635F") != -1||strData.indexOf("24534A5858") != -1||strData.indexOf("24464B5858") != -1) {// 回执,自检
							String revLogString = "[收：" + Public.getsystim()
									+ " Com" + comid + "]  [Data:" + strData
									+ "]";
							forinfo.info(revLogString);
							applicationConfig
									.addData_ListModel_com_rev(revLogString);
						} else {
							String revLogString = "[收：" + Public.getsystim()
									+ " Com" + comid + "]  [Data:" + strData
									+ "]";
							forinfo.info(revLogString);
							applicationConfig
									.addData_ListModel_com_rev(revLogString);
							ComStatusBean comStatusBean = bpcComStatusController
									.getComStatusBean(comid);
							comStatusBean.addRevCount();// 增加接收数量
							try {
								// // 封装发送到BPC
								minaClientHandler.SendBDdata(comid, proData);
							} catch (Exception e) {
								String logString = "[" + Public.getsystim()
										+ " 监控串口接收的缓存区数据时，发送到BPC错误]";
								forerror.error(logString);
								String logstr = "[BPC异常：" + Public.getsystim()
										+ " 监控串口接收的缓存区数据时，发送到BPC错误]";
								applicationConfig
										.addData_ListModel_bpc_cache(logstr);
							}
						}

					}
				}
				sleep(1000);
			} catch (Exception e) {
				// TODO: handle exception
				String logString = "监控串口接收的缓存区数据时，发生错误";
				forerror.error(logString);
				applicationConfig.addData_ListModel_com_rev(logString);
				e.printStackTrace();
			}

		}
	}

}
