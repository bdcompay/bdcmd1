package com.bdcmd.service.com;

import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import org.apache.log4j.Logger;
import com.bdcmd.service.com.bean.ToBpcBdDataBean;
import com.bdcmd.service.com.bean.ToComBdDataBean;

public class DataCacheQueueService {
	private ConcurrentHashMap<ToComBdDataBean, Integer> toComBdDataBeans;// 准备发送到串口的缓存队列
	private ConcurrentLinkedQueue<ToBpcBdDataBean> toBpcBdDataBeans;// 准备发送到bpc的缓存队列
	private static Logger fordebug = Logger.getLogger("fordebug");

	public DataCacheQueueService() {
		this.toComBdDataBeans = new ConcurrentHashMap<ToComBdDataBean, Integer>();
		this.toBpcBdDataBeans = new ConcurrentLinkedQueue<ToBpcBdDataBean>();
	}

	/**
	 * @Title: addToComCacheList
	 * @Description: TODO(加入Com缓存消息队列，准备发送到串口)
	 * @param @param toComBdDataBean
	 * @return void 返回类型
	 */
	public void addToComCacheList(int comid, ToComBdDataBean toComBdDataBean) {
		this.toComBdDataBeans.put(toComBdDataBean, comid);
		fordebug.debug("加入Com缓存消息队列，当前队列数量：" + this.toComBdDataBeans.size());
	}

	/**
	 * @Title: getCacheData
	 * @Description: TODO(取出一条Com缓存中的数据)
	 * @param @return
	 * @return SmsDataCacheBean 返回类型
	 */
	public ToComBdDataBean getToComCache(int comid) {
		for (Iterator<ToComBdDataBean> itr = toComBdDataBeans.keySet()
				.iterator(); itr.hasNext();) {
			ToComBdDataBean toComBdDataBean = (ToComBdDataBean) itr.next();
			int value = toComBdDataBeans.get(toComBdDataBean);
			if (value == comid) {				
				toComBdDataBeans.remove(toComBdDataBean);
				return toComBdDataBean;
			}
		}
		return null;
	}

	/**
	 * @Title: addToComCacheList
	 * @Description: TODO(加入BPC缓存消息队列，准备发送到BPC)
	 * @param @param toComBdDataBean
	 * @return void 返回类型
	 */
	public void addToBpcBdDataCacheList(ToBpcBdDataBean toBpcBdDataBean) {
		this.toBpcBdDataBeans.offer(toBpcBdDataBean);
		fordebug.debug("加入BPC缓存消息队列，当前队列数量：" + this.toBpcBdDataBeans.size());
	}

	/**
	 * @Title: getCacheData
	 * @Description: TODO(取出一条BPC缓存中的数据)
	 * @param @return
	 * @return SmsDataCacheBean 返回类型
	 */
	public ToBpcBdDataBean getToBpcCache() {

		ToBpcBdDataBean dataCacheBean = this.toBpcBdDataBeans.poll();
		return dataCacheBean;
	}

}
