package com.bdcmd.service.server.socket;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;

import net.sf.json.JsonConfig;
import net.sf.json.processors.JsonValueProcessor;

import org.apache.log4j.Logger;

/**
 * Bean工具
 * 
 * @author zfc
 * 
 */
public class BeansUtil {
	private static Logger logger = Logger.getLogger(BeansUtil.class);

	/**
	 * 根据属性名拷贝bean的属性
	 * 
	 * @param dest
	 * @param orig
	 * @param ignoreNull
	 *            是否忽略值为null的属性
	 * @param ignoreProperties
	 */
	public static void copyBean(Object dest, final Object orig,
			boolean ignoreNull, final String... ignoreProperties) {
		StringBuffer ignore = new StringBuffer("");
		if (ignoreProperties != null) {
			for (String s : ignoreProperties) {
				ignore.append(s).append(",");
			}
		}

		if (orig == null || dest == null) {
			dest = null;
		} else {
			Class<?> origClass = orig.getClass();
			Class<?> destClass = dest.getClass();
			Method[] destMethods = destClass.getMethods();
			String destMethodName, fieldName;
			Object fieldValue;
			Method origMethod;
			for (Method destMethod : destMethods) {
				destMethodName = destMethod.getName();
				if (destMethodName.matches("^set\\w+")) {
					fieldName = destMethodName.replaceAll("^set", "");
					if (ignore.toString().contains(
							fieldName.substring(0, 1).toLowerCase()
									+ fieldName.substring(1)))
						continue;
					try {
						origMethod = origClass.getMethod("get" + fieldName);
					} catch (NoSuchMethodException e) {
						try {
							origMethod = origClass.getMethod("is" + fieldName);
						} catch (Exception e1) {
							origMethod = null;
						}
					}
					if (origMethod == null) {
						// logger.debug("源对象没有属性" + fieldName +
						// "的获取方法,将忽略此属性的拷贝");
					} else {
						try {
							fieldValue = origMethod.invoke(orig);
							if ((!ignoreNull || fieldValue != null)
									&& destMethod.getParameterTypes()[0] == origMethod
											.getReturnType()) {
								destMethod.invoke(dest, fieldValue);
							}
						} catch (Exception e) {
							logger.error("拷贝属性" + fieldName + "失败");
						}
					}
				}
			}

		}
	}

	/**
	 * 根据属性名拷贝bean的属性
	 * 
	 * @param dest
	 *            目标对象
	 * @param orig
	 *            源对象
	 * @param ignoreNull
	 *            是否忽略值为null的属性
	 */
	public static void copyBean(Object dest, final Object orig,
			boolean ignoreNull) {
		copyBean(dest, orig, ignoreNull, "");
	}

	public static Object getProperty(Object bean, String field) {
		if (bean == null || field == null || field.length() < 1) {
			return null;
		}
		field = field.substring(0, 1).toUpperCase() + field.substring(1);
		Class<?> clazz = bean.getClass();
		Method method = null;
		try {
			method = clazz.getMethod("get" + field);
		} catch (NoSuchMethodException e) {
			try {
				method = clazz.getMethod("is" + field);
			} catch (Exception e1) {
				method = null;
			}
		}
		if (method == null) {
			return null;
		}
		Object result = null;
		try {
			result = method.invoke(bean);
		} catch (Exception e) {
			result = null;
		}
		return result;
	}

	/**
	 * 根据属性名拷贝bean的属性
	 * 
	 * @param dest
	 *            目标
	 * @param orig
	 *            源
	 * @param ignoreProperties
	 *            要忽略的属性
	 */
	public static void copyBean(Object dest, final Object orig,
			String... ignoreProperties) {
		copyBean(dest, orig, false, ignoreProperties);
	}

	/**
	 * 获取json-lib时间格式化的配置对象,通过此返回的对象作为配置,可自定义json中时间类型的格式
	 * 
	 * @return
	 */
	public static JsonConfig getJsonDateFormatCfg() {
		JsonConfig cfg = new JsonConfig();
		cfg.registerJsonValueProcessor(Calendar.class,
				new JsonValueProcessor() {
					private final String format = "yyyy-MM-dd HH:mm:ss";

					public Object processObjectValue(String key, Object value,
							JsonConfig arg2) {
						if (value instanceof Calendar) {
							String str = new SimpleDateFormat(format)
									.format(((Calendar) value).getTime());
							return str;
						}
						return value == null ? null : value.toString();
					}

					public Object processArrayValue(Object value,
							JsonConfig arg1) {
						if (value == null)
							return null;
						if (value instanceof Date) {
							String str = new SimpleDateFormat(format)
									.format((Date) value);
							return str;
						}
						return value.toString();
					}
				});

		cfg.registerJsonValueProcessor(Date.class, new JsonValueProcessor() {
			private final String format = "yyyy-MM-dd HH:mm:ss";

			public Object processObjectValue(String key, Object value,
					JsonConfig arg2) {
				if (value == null)
					return "";
				if (value instanceof Date) {
					String str = new SimpleDateFormat(format)
							.format((Date) value);
					return str;
				}
				return value.toString();
			}

			public Object processArrayValue(Object value, JsonConfig arg1) {
				if (value == null)
					return null;
				if (value instanceof Date) {
					String str = new SimpleDateFormat(format)
							.format((Date) value);
					return str;
				}
				return value.toString();
			}
		});
		return cfg;
	}

	/**
	 * Scans all classes accessible from the context class loader which belong
	 * to the given package and subpackages.
	 * 
	 * @param packageName
	 *            The base package
	 * @return The classes
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static Iterable<Class<?>> getClasses(String packageName)
			throws ClassNotFoundException, IOException {
		ClassLoader classLoader = Thread.currentThread()
				.getContextClassLoader();
		String path = packageName.replace('.', '/');
		Enumeration<URL> resources = classLoader.getResources(path);
		List<File> dirs = new ArrayList<File>();
		while (resources.hasMoreElements()) {
			URL resource = resources.nextElement();
			dirs.add(new File(resource.getFile()));
		}
		List<Class<?>> classes = new ArrayList<Class<?>>();
		for (File directory : dirs) {
			classes.addAll(findClasses(directory, packageName));
		}

		return classes;
	}

	/**
	 * Recursive method used to find all classes in a given directory and
	 * subdirs.
	 * 
	 * @param directory
	 *            The base directory
	 * @param packageName
	 *            The package name for classes found inside the base directory
	 * @return The classes
	 * @throws ClassNotFoundException
	 */
	private static List<Class<?>> findClasses(File directory, String packageName)
			throws ClassNotFoundException {
		List<Class<?>> classes = new ArrayList<Class<?>>();
		if (!directory.exists()) {
			return classes;
		}
		File[] files = directory.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				classes.addAll(findClasses(file,
						packageName + "." + file.getName()));
			} else if (file.getName().endsWith(".class")) {
				classes.add(Class.forName(packageName
						+ '.'
						+ file.getName().substring(0,
								file.getName().length() - 6)));
			}
		}
		return classes;
	}
}
