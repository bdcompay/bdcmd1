package com.bdcmd.service.server.socket;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import net.sf.json.JSONObject;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.apache.mina.core.session.IoSession;
import org.springframework.context.ApplicationContext;

import com.bdcmd.service.application.AppContext;
import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.com.DataCacheQueueService;
import com.bdcmd.service.com.bean.ToComBdDataBean;
import com.core.Public;

public class MinaClientHandler extends WebSupport {

	// public static ConcurrentHashMap<IoSession, Subsystem> BDCmdSessionPool;//
	// ConcurrentHashMap是绝对的线程安全
	/* 以下变量为spring管理的实例，将通过构造函数进行注入 */

	public static IoSession ioSession;
	private ApplicationConfig applicationConfig;
	private DataCacheQueueService dataCacheQueueService;
	// private BpcComStatusController bpcComStatusController;
	private static Logger forerror = Logger.getLogger("forerror");
	private static Logger forinfo = Logger.getLogger("forinfo");
	private static Logger fordebug = Logger.getLogger("fordebug");

	private final static String STATUS_NOT_EXIST = "NOT_EXIST";
	private final static String STATUS_OK = "OK";

	public MinaClientHandler() {

		// 注入spring管理的实例
		ApplicationContext ctx = AppContext.getApplicationContext();
		applicationConfig = (ApplicationConfig) ctx
				.getBean("applicationConfig");
		dataCacheQueueService = (DataCacheQueueService) ctx
				.getBean("dataCacheQueueService");

	}

	/*
	 * 分割线********************************************************以下是发送数据到BPC的函数*
	 * ***********************************************************
	 */

	public void SendTaskApply(String cardId, String bdChannelType,
			String channelGroupName, int comPort) // 申请北斗数据
	{
		applicationConfig.addData_ListModel_bpc_data("[发：" + Public.getsystim()
				+ "[BPC] [COM" + comPort + "][" + cardId + "]" + "[申请数据]");
		String uri = applicationConfig.getUrl() + "/popData";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();

			obj.put("serverCardNumber", cardId);
			obj.put("channelGroupName", channelGroupName);
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				JSONObject resultJson = JSONObject
						.fromObject(result.toString());
				String status = resultJson.getString("status");
				if (status.equals(STATUS_OK)) {
					JSONObject dataJson = resultJson.getJSONObject("result");
					String dataStr = dataJson.getString("data");
					String cardIdStr = dataJson.getString("cardNumber");
					byte[] bdata = BdCommonMethod.castHexStringToByte(dataStr);
					// 放入发送任务消息队列
					ToComBdDataBean toComBdDataBean = new ToComBdDataBean();
					toComBdDataBean.setBdData(bdata);
					dataCacheQueueService.addToComCacheList(comPort,
							toComBdDataBean);
					applicationConfig.addData_ListModel_bpc_data("[收："
							+ Public.getsystim() + "[BPC][COM" + comPort + "]["
							+ cardIdStr + "]" + "[收到发送任务数据："
							+ result.toString() + "]");
				}

			} else {
				applicationConfig.addData_ListModel_bpc_data("[收："
						+ Public.getsystim() + "[BPC][申请数据，返回的状态有误：]"
						+ resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			applicationConfig.addData_ListModel_bpc_data("[收："
					+ Public.getsystim() + "[BPC][申请数据，发生错误：]"
					+ CommonMethod.getTrace(e));
		}
	}

	public void SendBDdata(int comPort, byte[] bddata) // 上传北斗数据
	{
		applicationConfig.addData_ListModel_bpc_data("[发：" + Public.getsystim()
				+ "[BPC] [COM" + comPort + "]" + "[上传数据]");
		String uri = applicationConfig.getUrl() + "/pullBdData";
		try {
			HttpClient httpClient = new DefaultHttpClient();
			// HttpPost 实现 HttpUriRequest 接口,HttpUriRequest接口 继承 HttpRequest
			HttpPost httpPostReq = new HttpPost(uri);
			JSONObject obj = new JSONObject();
			obj.put("bdChannel", "DEVICE_40");
			obj.put("dataStr", BdCommonMethod.castBytesToString(bddata));
			StringEntity s = new StringEntity(obj.toString());
			s.setContentEncoding("UTF-8");
			s.setContentType("application/json");
			httpPostReq.setEntity(s);
			HttpResponse resp = httpClient.execute(httpPostReq);

			if (resp.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				BufferedReader reader = new BufferedReader(
						new InputStreamReader(resp.getEntity().getContent()));
				StringBuffer result = new StringBuffer();
				String inputLine = null;
				while ((inputLine = reader.readLine()) != null) {
					result.append(inputLine);
				}
				JSONObject resultJson = JSONObject
						.fromObject(result.toString());
				String status = resultJson.getString("status");
				if (!status.equals(STATUS_OK)) {
					applicationConfig
							.addData_ListModel_bpc_data("上传数据失败，请检查原因");
				}

			} else {
				applicationConfig.addData_ListModel_bpc_data("上传数据失败，请检查原因"
						+ resp.getStatusLine().getStatusCode());
			}

		} catch (Exception e) {
			applicationConfig.addData_ListModel_bpc_data("上传数据失败，请检查原因"
					+ CommonMethod.getTrace(e));
		}
	}

}