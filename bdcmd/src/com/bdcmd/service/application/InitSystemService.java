package com.bdcmd.service.application;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import com.bdcmd.swing.BdcmdIndex;
import com.bdcmd.swing.BpcComStatusController;

public class InitSystemService {
	@Resource
	private ApplicationConfig applicationConfig;
	@Resource
	private BpcComStatusController bpcComStatusController;

	public void init() {
		String filename = System.getProperty("user.dir");
		System.setProperty("WORKDIR", filename);
		applicationConfig.setFordebug(Logger.getLogger("fordebug"));
		applicationConfig.setForerror(Logger.getLogger("forerror"));
		applicationConfig.setForinfo(Logger.getLogger("forinfo"));
		Logger forinfo = applicationConfig.getForinfo();
		Logger fordebug = applicationConfig.getFordebug();
		Logger forerror = applicationConfig.getForerror();

		// 开启界面
		BdcmdIndex.main(null);
		bpcComStatusController.start();
	}

	public void destory() {
		System.out.println("destory");
	}

}
