package com.bdcmd.service.application;

import java.awt.TextArea;

import javax.annotation.Resource;
import javax.swing.DefaultListModel;

import org.apache.log4j.Logger;

import com.bdcmd.service.com.CommServerService;
import com.bdcmd.service.com.bean.CommSerialConfigBean;

/*
 * 
 * 全局配置
 */
public class ApplicationConfig {
	private String ENTROLE = "ROLE_ENT_USER";
	private String USERROLE = "ROLE_USER";

	/***** 华丽的分割线*********以下是LOG4J的相关配置 **********************/
	private Logger forerror;
	private Logger forinfo;
	private Logger fordebug;

	private String subname;// bpc的用户名
	private String subpwd;// bpc的密码
	private boolean postGl;// 是否上传功率到BPC
	private boolean showGl;// 列表是否显示功率输出信息
	private int checkBpcDataTime;// 每轮检查BPC发送到串口的间隔时间
	private String alertMusicPath;// 报警音乐的路径
	private int outputSize;// 四个窗口输出的最大字符数
	private String url;
	private String channelGroupName;
	// 串口配置
	@Resource
	private CommSerialConfigBean commSerialConfigBean1;
	@Resource
	private CommSerialConfigBean commSerialConfigBean2;
	@Resource
	private CommSerialConfigBean commSerialConfigBean3;
	@Resource
	private CommSerialConfigBean commSerialConfigBean4;
	@Resource
	private CommSerialConfigBean commSerialConfigBean5;
	@Resource
	private CommSerialConfigBean commSerialConfigBean6;
	@Resource
	private CommSerialConfigBean commSerialConfigBean7;
	@Resource
	private CommSerialConfigBean commSerialConfigBean8;
	@Resource
	private CommSerialConfigBean commSerialConfigBean9;
	@Resource
	private CommSerialConfigBean commSerialConfigBean10;

	// 界面数据列表控件配置
	// private DefaultListModel listModel_com_rev = new DefaultListModel();
	private TextArea textArea_Com_Rev;
	private TextArea textArea_bpc_data;
	private TextArea textArea_error;
	private TextArea textArea_Com_Send;

	private DefaultListModel listModel_coms_status = new DefaultListModel();

	public boolean isPostGl() {
		return postGl;
	}

	public void setPostGl(boolean postGl) {
		this.postGl = postGl;
	}

	public boolean isShowGl() {
		return showGl;
	}

	public void setShowGl(boolean showGl) {
		this.showGl = showGl;
	}



	public String getSubname() {
		return subname;
	}

	public void setSubname(String subname) {
		this.subname = subname;
	}

	public String getSubpwd() {
		return subpwd;
	}

	public void setSubpwd(String subpwd) {
		this.subpwd = subpwd;
	}

	public String getENTROLE() {
		return ENTROLE;
	}

	public void setENTROLE(String eNTROLE) {
		ENTROLE = eNTROLE;
	}

	public String getUSERROLE() {
		return USERROLE;
	}

	public void setUSERROLE(String uSERROLE) {
		USERROLE = uSERROLE;
	}

	public Logger getForerror() {
		return forerror;
	}

	public void setForerror(Logger forerror) {
		this.forerror = forerror;
	}

	public Logger getForinfo() {
		return forinfo;
	}

	public void setForinfo(Logger forinfo) {
		this.forinfo = forinfo;
	}

	public Logger getFordebug() {
		return fordebug;
	}

	public void setFordebug(Logger fordebug) {
		this.fordebug = fordebug;
	}



	public int getCheckBpcDataTime() {
		return checkBpcDataTime;
	}

	public void setCheckBpcDataTime(int checkBpcDataTime) {
		this.checkBpcDataTime = checkBpcDataTime;
	}



	public CommSerialConfigBean getCommSerialConfigBean(Integer comid) {
		switch (comid) {
		case 1:
			return commSerialConfigBean1;
		case 2:
			return commSerialConfigBean2;
		case 3:
			return commSerialConfigBean3;
		case 4:
			return commSerialConfigBean4;
		case 5:
			return commSerialConfigBean5;
		case 6:
			return commSerialConfigBean6;
		case 7:
			return commSerialConfigBean7;
		case 8:
			return commSerialConfigBean8;
		case 9:
			return commSerialConfigBean9;
		case 10:
			return commSerialConfigBean10;
		default:
			return null;
		}
	}

	public DefaultListModel getListModel_coms_status() {
		return listModel_coms_status;
	}

	public void addData_ListModel_com_rev(String element) {
		synchronized (this.textArea_Com_Rev) {
			String contentString = textArea_Com_Rev.getText();
			contentString = element + "\r\n" + contentString;
			textArea_Com_Rev.setText(contentString);
			if (contentString.length() > getOutputSize()) {
				textArea_Com_Rev.setText("");
			}
		}

	}

	public void addListModel_com_send(String element) {
		synchronized (this.textArea_Com_Send) {
			String contentString = textArea_Com_Send.getText();
			contentString = element + "\r\n" + contentString;
			textArea_Com_Send.setText(contentString);
			if (contentString.length() > getOutputSize()) {
				textArea_Com_Send.setText("");
			}
		}
	}

	public void addData_ListModel_bpc_cache(String element) {
		synchronized (this.textArea_error) {
			String contentString = textArea_error.getText();
			contentString = element + "\r\n" + contentString;
			textArea_error.setText(contentString);
			if (contentString.length() > getOutputSize()) {
				textArea_error.setText("");
			}
		}
	}

	public void addData_ListModel_bpc_data(String element) {
		synchronized (this.textArea_bpc_data) {
			String contentString = textArea_bpc_data.getText();
			contentString = element + "\r\n" + contentString;
			textArea_bpc_data.setText(contentString);
			if (contentString.length() > getOutputSize()) {
				textArea_bpc_data.setText("");
			}
		}
	}

	public void removeAllData_ListModel() {

		textArea_Com_Rev.setText("");
		textArea_bpc_data.setText("");
		textArea_Com_Send.setText("");
		textArea_error.setText("");
	}

	public TextArea getTextArea_Com_Rev() {
		return textArea_Com_Rev;
	}

	public void setTextArea_Com_Rev(TextArea textArea_Com_Rev) {
		this.textArea_Com_Rev = textArea_Com_Rev;
	}

	public TextArea getTextArea_bpc_data() {
		return textArea_bpc_data;
	}

	public void setTextArea_bpc_data(TextArea textArea_bpc_data) {
		this.textArea_bpc_data = textArea_bpc_data;
	}

	public TextArea getTextArea_error() {
		return textArea_error;
	}

	public void setTextArea_error(TextArea textArea_error) {
		this.textArea_error = textArea_error;
	}

	public TextArea getTextArea_Com_Send() {
		return textArea_Com_Send;
	}

	public void setTextArea_Com_Send(TextArea textArea_Com_Send) {
		this.textArea_Com_Send = textArea_Com_Send;
	}

	public String getAlertMusicPath() {
		return alertMusicPath;
	}

	public void setAlertMusicPath(String alertMusicPath) {
		this.alertMusicPath = alertMusicPath;
	}

	public int getOutputSize() {
		return outputSize;
	}

	public void setOutputSize(int outputSize) {
		this.outputSize = outputSize;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getChannelGroupName() {
		return channelGroupName;
	}

	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}
}