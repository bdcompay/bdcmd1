/**  
 * @Title: ComStatusBean.java 
 * @Package com.bdcmd.swing 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author A18ccms A18ccms_gmail_com  
 * @date 2013-6-25 上午10:54:36 
 * @version V1.0  
 */

package com.bdcmd.swing;

import java.awt.Color;
import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import com.bdcmd.service.application.ApplicationConfig;
import com.bdcmd.service.com.CommServerService;
import com.bdcmd.service.com.SerialDevice;
import com.bdcmd.service.com.bean.CommSerialConfigBean;
import com.bdcmd.service.server.socket.MinaClientHandler;
import com.bdcmd.swing.bean.ComPanelBean;
import com.core.Public;
import com.pro.Analysis;

/**
 * 项目名称：bdcmdCom 类名称：ComStatusBean 类描述： 创建人：will.yan 创建时间：2013-6-25 上午10:54:36
 * 
 * @version
 */

public class BpcComStatusController extends Thread {
	private static Logger forerror = Logger.getLogger("forerror");
	private List<ComStatusBean> comStatusBeans;
	private BpcStatusBean bpcStatusBean;
	private JList jlist_coms_status;
	@Resource
	private ApplicationConfig applicationConfig;
	@Resource
	private CommServerService commServerService;
	@Resource
	private MinaClientHandler minaClientHandler;

	public JList getJlist_coms_status() {
		return jlist_coms_status;
	}

	public void setJlist_coms_status(JList jlist_coms_status) {
		this.jlist_coms_status = jlist_coms_status;
	}

	public BpcComStatusController() {
		comStatusBeans = new ArrayList<BpcComStatusController.ComStatusBean>();
		bpcStatusBean = new BpcStatusBean();

	}

	public ComStatusBean getComStatusBean(int comid) {
		return comStatusBeans.get(comid - 1);
	}

	public BpcStatusBean getBpcStatusBean() {
		return bpcStatusBean;
	}

	public void stopAlert() {
		commServerService.stopAlertMusic();
	}

	public void sendComData(SerialDevice serialDevice, byte[] proData) {
		try {
			serialDevice.getOutputStream().write(proData);
			String element = "[发：" + Public.getsystim() + " Com"
					+ serialDevice.getCommDataObserver().getComPort()
					+ "] [Data:"
					+ Analysis.ByteHexToString(proData, proData.length) + "]";
			applicationConfig.addListModel_com_send(element);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void openOrcloseCom(int comid, JButton buttonCom) {
		ComStatusBean comStatusBean = getComStatusBean(comid);
		if (!comStatusBean.isOpen()) {
			ComPanelBean comPanelBean = commServerService.openCom(comid);
			if (comPanelBean.isRetrunValue()) {
				comStatusBean.setOpen(true);
				JOptionPane.showMessageDialog(null,
						comPanelBean.getReturnLog(), "提示",
						JOptionPane.INFORMATION_MESSAGE);
				buttonCom.setText("关闭串口" + comid);
				comStatusBean.setOpenStatus("√");
				comStatusBean.startGlMonitor();// 开始功率监测
			} else {
				JOptionPane.showMessageDialog(null,
						comPanelBean.getReturnLog(), "提示",
						JOptionPane.INFORMATION_MESSAGE);
			}
		} else {
			ComPanelBean comPanelBean = commServerService.closeCom(comid);
			if (comPanelBean.isRetrunValue()) {
				comStatusBean.setOpen(false);
				comStatusBean.setNormal(false);
				comStatusBean.setOpenStatus("×");
				comStatusBean.setStatus("未知");
				comStatusBean.setEastStatus("□□□□(0000)");
				comStatusBean.setWestStatus("□□□□(0000)");
				comStatusBean.setBackStatus("□□□□(0000)");
				comStatusBean.setSentCount("0");
				comStatusBean.setRevCount("0");
				comStatusBean.setGlback(0);
				comStatusBean.setGleast(0);
				comStatusBean.setGlwest(0);

				JOptionPane.showMessageDialog(null,
						comPanelBean.getReturnLog(), "提示",
						JOptionPane.INFORMATION_MESSAGE);
				buttonCom.setText("打开串口" + comid);

			} else {
				JOptionPane.showMessageDialog(null,
						comPanelBean.getReturnLog(), "提示",
						JOptionPane.INFORMATION_MESSAGE);
			}
		}
	}

	/**
	 * @Title: setGLStatus
	 * @Description: TODO(更新串口功率状态信息)
	 * @param @param strData
	 * @param @param comid
	 * @return void 返回类型
	 */
	public void setGLStatus(String strData, int comid) {
		String bserverCard = "";
		int eastgl1, eastgl2 = 0, eastgl = 0, westgl1, westgl2 = 0, westgl = 0, backgl1 = 0, backgl2 = 0, backgl = 0;
		if (strData.indexOf("245369675F") != -1) {
			bserverCard = strData.substring(10, 16);
			eastgl1 = Integer.valueOf(strData.substring(16, 18));
			eastgl2 = Integer.valueOf(strData.substring(18, 20));
			eastgl = eastgl1 > eastgl2 ? eastgl1 : eastgl2;
			westgl1 = Integer.valueOf(strData.substring(20, 22));
			westgl2 = Integer.valueOf(strData.substring(22, 23));
			westgl = westgl1 > westgl2 ? westgl1 : westgl2;
			backgl1 = Integer.valueOf(strData.substring(24, 26));
			backgl2 = Integer.valueOf(strData.substring(26, 28));
			backgl = backgl1 > backgl2 ? backgl1 : backgl2;
		} else if (strData.indexOf("24474C5A4B") != -1) {
			bserverCard = strData.substring(14, 20);
			eastgl1 = Integer.valueOf(strData.substring(20, 22));
			eastgl2 = Integer.valueOf(strData.substring(22, 24));
			eastgl = eastgl1 > eastgl2 ? eastgl1 : eastgl2;
			westgl1 = Integer.valueOf(strData.substring(24, 26));
			westgl2 = Integer.valueOf(strData.substring(26, 28));
			westgl = westgl1 > westgl2 ? westgl1 : westgl2;
			backgl1 = Integer.valueOf(strData.substring(28, 30));
			backgl2 = Integer.valueOf(strData.substring(30, 32));
			backgl = backgl1 > backgl2 ? backgl1 : backgl2;
		} else {
			String logString = "解析功率时，无法正确解析命令头";
			forerror.error(logString);
			applicationConfig.addData_ListModel_com_rev(logString);
			return;
		}
		ComStatusBean comStatusBean = getComStatusBean(comid);
		comStatusBean.setGleast(eastgl);
		comStatusBean.setEastStatus(getGLIcon(eastgl, eastgl2));
		comStatusBean.setGlwest(westgl);
		comStatusBean.setWestStatus(getGLIcon(westgl, westgl2));
		comStatusBean.setGlback(backgl);
		comStatusBean.setBackStatus(getGLIcon(backgl1, backgl2));
		comStatusBean.setNormal(true);
		comStatusBean.setLastGlTime(new Timestamp(System.currentTimeMillis()));

	}

	private String getGLIcon(int gl1, int gl2) {
		Integer glCount = gl1 > gl2 ? gl1 : gl2;
		String tailString = "(0" + gl1 + "0" + gl2 + ")";

		switch (glCount) {
		case 0:
			return "□□□□" + tailString;
		case 1:
			return "■□□□" + tailString;
		case 2:
			return "■■□□" + tailString;
		case 3:
			return "■■■□" + tailString;
		case 4:
			return "■■■■" + tailString;
		default:
			return "异常值" + tailString;
		}
	}

	public void openOrclosebpc(JButton buttonBpc) {
		// TODO add your handling code here:
		if (bpcStatusBean.isOpen()) {// 关闭
			bpcStatusBean.setOpen(false);
			bpcStatusBean.setOpenStatus("已关闭");
			buttonBpc.setText("启动服务");
		} else {
			bpcStatusBean.setOpen(true);
			bpcStatusBean.setOpenStatus("已连接");
			buttonBpc.setText("关闭服务");
		}
	}

	// 定时响应更新界面上的BPC,串口功率的信息
	public void run() {

		bpcStatusBean.setUsername(applicationConfig.getSubname());
		applicationConfig.getListModel_coms_status().add(0,
				bpcStatusBean.getBpcStr());

		for (int i = 1; i <= 10; i++) {
			CommSerialConfigBean commSerialConfigBean = applicationConfig
					.getCommSerialConfigBean(i);
			ComStatusBean comStatusBean = new ComStatusBean();
			comStatusBean.setComPort(i);
			comStatusBean.setServerCard(commSerialConfigBean.getCardId());
			comStatusBean.setFreq(commSerialConfigBean.getFreq());
			comStatusBean.setDeviceType(commSerialConfigBean.getChannelType());
			comStatusBean.setGlFreq(commSerialConfigBean.getGlFreq());
			comStatusBeans.add(comStatusBean);
			applicationConfig.getListModel_coms_status().add(i,
					comStatusBean.getComStr());

		}
		while (true) {
			String bpcString = bpcStatusBean.getBpcStr();
			applicationConfig.getListModel_coms_status().set(0, bpcString);
			int index = 1;
			for (ComStatusBean comStatusBean : comStatusBeans) {

				String comStr = comStatusBean.getComStr();
				applicationConfig.getListModel_coms_status().set(index, comStr);
				index++;
			}

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	public class ComStatusBean extends Thread {
		private boolean isOpen = false;//是否打开
		private boolean isBusy = false;//是否忙碌
		private boolean isApply = false;//是否在申请下发数据
		private boolean isNormal = false;//
		private Timestamp lastGlTime = new Timestamp(System.currentTimeMillis());//功率最后更新时间
		private int glFreq;//频度
		private int comPort;//com号序号
		private String deviceType = "";//协议
		private String serverCard = "";//服务卡号
		private String freq = "";//
		private String busyfreq = "00";//剩余忙碌时间
		private String openStatus = "×";
		private String status = "未知";//
		private String eastStatus = "□□□□(0000)";
		private String westStatus = "□□□□(0000)";
		private String backStatus = "□□□□(0000)";
		private String sentCount = "0";
		private String revCount = "0";
		private int gleast = 0;//
		private int glwest = 0;//
		private int glback = 0;//

		public String getComStr() {

			return " 【COM:" + comPort + "】【串口状态：" + openStatus + "】【倒计时："
					+ busyfreq + "】【状态：" + status + "】【服务卡:" + serverCard
					+ "，频度:" + freq + "秒，设备协议类型:" + deviceType + "】【东星："
					+ eastStatus + "】【 西星：" + westStatus + "】【备用星："
					+ backStatus + "】【发送数量：" + sentCount + "】【接收数量：" + revCount
					+ "】";
		}

		/**
		 * @Title: startBusyfreq
		 * @Description: TODO(串口开始倒计时)
		 * @param
		 * @return void 返回类型
		 */
		public void startBusyfreq() {
			Thread thread = new Thread() {
				public void run() {

					int busyfreq = Integer.valueOf(applicationConfig
							.getCommSerialConfigBean(comPort).getFreq());

					for (int i = 1; i <= busyfreq; i++) {
						String busyfreqsString = String.valueOf(busyfreq - i);
						setBusyfreq(busyfreqsString);
						try {
							sleep(1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					setBusy(false);

				}
			};
			thread.start();
		}

		/**
		 * @Title: startGlMonitor
		 * @Description: TODO(开始监控功率)
		 * @param
		 * @return void 返回类型
		 */
		public void startGlMonitor() {

			Thread thread = new Thread() {
				public void run() {
					while (isOpen) {
						SerialDevice serialDevice = commServerService
								.getSerialDevice(comPort);
						sendComData(serialDevice, getGlPro());
						try {
							sleep(glFreq * 1000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (!isOpen) {
							jlist_coms_status.setCellRenderer(new CellRender(
									comPort, Color.WHITE));
							break;
						}
						Timestamp currentTime = new Timestamp(
								System.currentTimeMillis());
						try {
							int diffSeconds = Public.getTimeDiff(lastGlTime,
									currentTime);
							if (diffSeconds > (glFreq)) {
								String logstr = "[信号异常：" + Public.getsystim()
										+ " COM" + comPort + " 超过:" + glFreq
										+ "秒未收到功率信息！";
								applicationConfig
										.addData_ListModel_bpc_cache(logstr);
								// 超时没收到功率,报警
								setStatus("超过:" + glFreq + "秒未收到功率信息！");
								setNormal(false);
								setEastStatus("□□□□(0000)");
								setWestStatus("□□□□(0000)");
								setBackStatus("□□□□(0000)");

								// 报警
								Thread alertThread = new Thread() {
									public void run() {
										commServerService.playAlertMusic();
									}
								};
								alertThread.start();
								jlist_coms_status
										.setCellRenderer(new CellRender(
												comPort, Color.RED));
							} else {
								if (isGlAlert()) {// 没信号
									String logstr = "[信号异常："
											+ Public.getsystim() + " COM"
											+ comPort + " 信号太弱，请移动模组！";
									applicationConfig
											.addData_ListModel_bpc_cache(logstr);
									setStatus("信号太弱，请移动模组！");

									setNormal(false);
									// 报警
									Thread alertThread = new Thread() {
										public void run() {
											commServerService.playAlertMusic();
										}
									};
									alertThread.start();
									jlist_coms_status
											.setCellRenderer(new CellRender(
													comPort, Color.RED));

								} else {
									if (!isBusy) {
										setStatus("正常");
										commServerService.stopAlertMusic();
										jlist_coms_status
												.setCellRenderer(new CellRender(
														comPort, Color.GREEN));
									} else {
										setStatus("忙碌");
										commServerService.stopAlertMusic();
										jlist_coms_status
												.setCellRenderer(new CellRender(
														comPort, Color.YELLOW));
									}

								}
							}
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			};
			thread.start();
		}

		private byte[] getGlPro() {

			if (getDeviceType().equals("DEVICE_4IN1")) {
				byte[] bProData = new byte[12];
				bProData[0] = (byte) 0x24;
				bProData[1] = (byte) 0x53;
				bProData[2] = (byte) 0x69;
				bProData[3] = (byte) 0x67;
				bProData[4] = (byte) 0x5F;
				byte[] bCardid = Analysis.intToByteArray1(Integer
						.valueOf(serverCard));
				bProData[5] = (byte) 0x00;
				bProData[6] = (byte) 0x00;
				bProData[7] = (byte) 0x00;
				bProData[8] = (byte) 0x00;
				bProData[9] = (byte) 0x00;
				bProData[10] = (byte) 0x5F;
				bProData[11] = (byte) 0x0A;
				return bProData;
			} else if (getDeviceType().equals("DEVICE_40")) {
				// 24474C5A4B
				byte[] bProData = new byte[12];
				bProData[0] = (byte) 0x24;
				bProData[1] = (byte) 0x47;
				bProData[2] = (byte) 0x4C;
				bProData[3] = (byte) 0x4A;
				bProData[4] = (byte) 0x43;
				bProData[5] = (byte) 0x00;
				bProData[6] = (byte) 0x0C;
				byte[] bCardid = Analysis.intToByteArray1(Integer
						.valueOf(serverCard));
				bProData[7] = bCardid[0];
				bProData[8] = bCardid[1];
				bProData[9] = bCardid[2];
				bProData[10] = (byte) 0x00;
				byte[] pcrc = new byte[11];
				System.arraycopy(bProData, 0, pcrc, 0, 11);
				bProData[11] = (byte) Analysis.getCRC(pcrc);
				return bProData;
			}
			return null;

		}

		public boolean isApply() {
			return isApply;
		}

		public void setApply(boolean isApply) {
			this.isApply = isApply;
		}

		public int getGlFreq() {
			return glFreq;
		}

		public void setGlFreq(int glFreq) {
			this.glFreq = glFreq;
		}

		public int getComPort() {
			return comPort;
		}

		public String getOpenStatus() {
			return openStatus;
		}

		public void setOpenStatus(String openStatus) {
			this.openStatus = openStatus;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getEastStatus() {
			return eastStatus;
		}

		public void setEastStatus(String eastStatus) {
			this.eastStatus = eastStatus;
		}

		public String getWestStatus() {
			return westStatus;
		}

		public void setWestStatus(String westStatus) {
			this.westStatus = westStatus;
		}

		public String getBackStatus() {
			return backStatus;
		}

		public void setBackStatus(String backStatus) {
			this.backStatus = backStatus;
		}

		public String getSentCount() {
			return sentCount;
		}

		public void setSentCount(String sentCount) {
			this.sentCount = sentCount;
		}

		public String getRevCount() {
			return revCount;
		}

		public void setRevCount(String revCount) {
			this.revCount = revCount;
		}

		public String getServerCard() {
			return serverCard;
		}

		public void setServerCard(String serverCard) {
			this.serverCard = serverCard;
		}

		public String getFreq() {
			return freq;
		}

		public void setFreq(String freq) {
			this.freq = freq;
		}

		public String getBusyfreq() {
			return busyfreq;
		}

		public void setBusyfreq(String busyfreq) {
			this.busyfreq = busyfreq;
		}

		public boolean isOpen() {
			return isOpen;
		}

		public void setOpen(boolean isOpen) {
			this.isOpen = isOpen;
		}

		public void setComPort(int comPort) {
			this.comPort = comPort;
		}

		public String getDeviceType() {
			return deviceType;
		}

		public void setDeviceType(String deviceType) {
			this.deviceType = deviceType;
		}

		public boolean isBusy() {
			return isBusy;
		}

		public void setBusy(boolean isBusy) {
			this.isBusy = isBusy;
		}

		public boolean isNormal() {
			return isNormal;
		}

		public void setNormal(boolean isNormal) {
			this.isNormal = isNormal;
		}

		public Timestamp getLastGlTime() {
			return lastGlTime;
		}

		public void setLastGlTime(Timestamp lastGlTime) {
			this.lastGlTime = lastGlTime;
		}

		public int getGleast() {
			return gleast;
		}

		public void setGleast(int gleast) {
			this.gleast = gleast;
		}

		public int getGlwest() {
			return glwest;
		}

		public void setGlwest(int glwest) {
			this.glwest = glwest;
		}

		public int getGlback() {
			return glback;
		}

		public void setGlback(int glback) {
			this.glback = glback;
		}

		public void addRevCount() {
			int revsCount = Integer.valueOf(this.revCount);
			revsCount++;
			this.revCount = String.valueOf(revsCount);
		}

		public void addSendCount() {
			int sendCount = Integer.valueOf(this.sentCount);
			sendCount++;
			this.sentCount = String.valueOf(sendCount);
		}

		/**
		 * @Title: isGlAlert
		 * @Description: TODO(查看是否满足)
		 * @param @return
		 * @return boolean 返回类型
		 */
		public boolean isGlAlert() {
			int starsCount = 0;
			CommSerialConfigBean commSerialConfigBean = applicationConfig
					.getCommSerialConfigBean(this.comPort);
			if (gleast >= commSerialConfigBean.getStarGlAlert()) {
				starsCount++;
			}
			if (glwest >= commSerialConfigBean.getStarGlAlert()) {
				starsCount++;
			}
			if (glback >= commSerialConfigBean.getStarGlAlert()) {
				starsCount++;
			}
			if (starsCount < commSerialConfigBean.getStarCountAlert()) {
				return true;
			} else {
				return false;
			}
		}
	}

	public class BpcStatusBean {
		private boolean isOpen = false;
		private String bpcPort = "";
		private String username = "";
		private String openStatus = "未登录";
		private String status = "";
		private String sentCount = "0";
		private String revCount = "0";

		public String getBpcStr() {
			return " 【当前服务状态：" + openStatus + "】【BPC端口：" + bpcPort + "】【用户名："
					+ username + "】";
		}

		public boolean isOpen() {
			return isOpen;
		}

		public void setOpen(boolean isOpen) {
			this.isOpen = isOpen;
		}

		public String getBpcPort() {
			return bpcPort;
		}

		public void setBpcPort(String bpcPort) {
			this.bpcPort = bpcPort;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getOpenStatus() {
			return openStatus;
		}

		public void setOpenStatus(String openStatus) {
			this.openStatus = openStatus;
		}

		public String getStatus() {
			return status;
		}

		public void setStatus(String status) {
			this.status = status;
		}

		public String getSentCount() {
			return sentCount;
		}

		public void setSentCount(String sentCount) {
			this.sentCount = sentCount;
		}

		public String getRevCount() {
			return revCount;
		}

		public void setRevCount(String revCount) {
			this.revCount = revCount;
		}

	}

}
