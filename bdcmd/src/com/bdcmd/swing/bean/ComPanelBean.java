/**  
 * @Title: ComPanelBean.java 
 * @Package com.bdcmd.swing.bean 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author A18ccms A18ccms_gmail_com  
 * @date 2013-6-24 上午11:20:00 
 * @version V1.0  
 */

package com.bdcmd.swing.bean;

/**
 * 项目名称：bdcmdCom 类名称：ComPanelBean 类描述： 创建人：will.yan 创建时间：2013-6-24 上午11:20:00
 * 
 * @version
 */

public class ComPanelBean {
	private boolean retrunValue;
	private String returnLog;

	public boolean isRetrunValue() {
		return retrunValue;
	}

	public void setRetrunValue(boolean retrunValue) {
		this.retrunValue = retrunValue;
	}

	public String getReturnLog() {
		return returnLog;
	}

	public void setReturnLog(String returnLog) {
		this.returnLog = returnLog;
	}

}
