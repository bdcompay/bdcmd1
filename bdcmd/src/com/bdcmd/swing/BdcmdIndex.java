/*
 * bdcmd.java
 *
 * Created on __DATE__, __TIME__
 */

package com.bdcmd.swing;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import org.springframework.context.ApplicationContext;

import com.bdcmd.service.application.AppContext;
import com.bdcmd.service.application.ApplicationConfig;

/**
 * 
 * @author __USER__
 */
public class BdcmdIndex extends javax.swing.JFrame {
	private ApplicationConfig applicationConfig;

	private BpcComStatusController bpcComStatusController;

	/** Creates new form bdcmd */
	public BdcmdIndex() {
		// 注入spring管理的实例
		ApplicationContext ctx = AppContext.getApplicationContext();
		applicationConfig = (ApplicationConfig) ctx
				.getBean("applicationConfig");
		bpcComStatusController = (BpcComStatusController) ctx
				.getBean("bpcComStatusController");
		initComponents();
		initData();
	}

//GEN-BEGIN:initComponents
// <editor-fold defaultstate="collapsed" desc="Generated Code">
private void initComponents() {

jPanel1 = new javax.swing.JPanel();
jPanel3 = new javax.swing.JPanel();
jTabbedPane_system = new javax.swing.JTabbedPane();
 javax.swing.JPanel jPanel_system_monitor = new javax.swing.JPanel();
jScrollPane2 = new javax.swing.JScrollPane();
jTable_com_send = new javax.swing.JTable();
jPanel_bpc_coms = new javax.swing.JPanel();
jButton_bpc = new javax.swing.JButton();
jButton_com1 = new javax.swing.JButton();
jButton_com2 = new javax.swing.JButton();
jButton_com3 = new javax.swing.JButton();
jButton_com4 = new javax.swing.JButton();
jButton_com5 = new javax.swing.JButton();
jButton_com6 = new javax.swing.JButton();
jButton_com7 = new javax.swing.JButton();
jButton_com8 = new javax.swing.JButton();
jButton_com9 = new javax.swing.JButton();
jButton_com10 = new javax.swing.JButton();
jButton_stopAlert = new javax.swing.JButton();
jButton_clearOutputList = new javax.swing.JButton();
jPanel_coms_status = new javax.swing.JPanel();
jScrollPane6 = new javax.swing.JScrollPane();
jList_coms_status = new javax.swing.JList();
jPanel_data_monitor = new javax.swing.JPanel();
jPanel_com_rev = new javax.swing.JPanel();
textArea_Com_Rev = new java.awt.TextArea();
jPanel_com_send = new javax.swing.JPanel();
textArea_com_send = new java.awt.TextArea();
jPanel_bpc = new javax.swing.JPanel();
textArea_bpc_data = new java.awt.TextArea();
jPanel_cache = new javax.swing.JPanel();
textArea_error = new java.awt.TextArea();

org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
jPanel1.setLayout(jPanel1Layout);
jPanel1Layout.setHorizontalGroup(
jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
.add(0, 100, Short.MAX_VALUE)
);
jPanel1Layout.setVerticalGroup(
jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
.add(0, 100, Short.MAX_VALUE)
);

org.jdesktop.layout.GroupLayout jPanel3Layout = new org.jdesktop.layout.GroupLayout(jPanel3);
jPanel3.setLayout(jPanel3Layout);
jPanel3Layout.setHorizontalGroup(
jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
.add(0, 100, Short.MAX_VALUE)
);
jPanel3Layout.setVerticalGroup(
jPanel3Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
.add(0, 100, Short.MAX_VALUE)
);

setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
setTitle("\u5317\u6597\u901a\u9053\u63a5\u53e3\u673a");
setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

jTable_com_send.setModel(new javax.swing.table.DefaultTableModel(
	new Object [][] {
		{null, null, null, null},
		{null, null, null, null},
		{null, null, null, null},
		{null, null, null, null}
	},
	new String [] {
		"Title 1", "Title 2", "Title 3", "Title 4"
	}
));
jScrollPane2.setViewportView(jTable_com_send);

jPanel_bpc_coms.setBackground(javax.swing.UIManager.getDefaults().getColor("InternalFrame.activeTitleGradient"));

jButton_bpc.setText("\u542f\u52a8\u670d\u52a1");
jButton_bpc.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_bpcActionPerformed(evt);
}
});

jButton_com1.setText("\u6253\u5f00\u4e32\u53e31");
jButton_com1.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com1ActionPerformed(evt);
}
});

jButton_com2.setText("\u6253\u5f00\u4e32\u53e32");
jButton_com2.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com2ActionPerformed(evt);
}
});

jButton_com3.setText("\u6253\u5f00\u4e32\u53e33");
jButton_com3.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com3ActionPerformed(evt);
}
});

jButton_com4.setText("\u6253\u5f00\u4e32\u53e34");
jButton_com4.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com4ActionPerformed(evt);
}
});

jButton_com5.setText("\u6253\u5f00\u4e32\u53e35");
jButton_com5.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com5ActionPerformed(evt);
}
});

jButton_com6.setText("\u6253\u5f00\u4e32\u53e36");
jButton_com6.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com6ActionPerformed(evt);
}
});

jButton_com7.setText("\u6253\u5f00\u4e32\u53e37");
jButton_com7.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com7ActionPerformed(evt);
}
});

jButton_com8.setText("\u6253\u5f00\u4e32\u53e38");
jButton_com8.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com8ActionPerformed(evt);
}
});

jButton_com9.setText("\u6253\u5f00\u4e32\u53e39");
jButton_com9.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com9ActionPerformed(evt);
}
});

jButton_com10.setText("\u6253\u5f00\u4e32\u53e310");
jButton_com10.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_com10ActionPerformed(evt);
}
});

jButton_stopAlert.setText("\u505c\u6b62\u62a5\u8b66");
jButton_stopAlert.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_stopAlertActionPerformed(evt);
}
});

jButton_clearOutputList.setText("\u6e05\u9664\u8f93\u51fa\u5217\u8868");
jButton_clearOutputList.addActionListener(new java.awt.event.ActionListener() {
public void actionPerformed(java.awt.event.ActionEvent evt) {
jButton_clearOutputListActionPerformed(evt);
}
});

org.jdesktop.layout.GroupLayout jPanel_bpc_comsLayout = new org.jdesktop.layout.GroupLayout(jPanel_bpc_coms);
jPanel_bpc_coms.setLayout(jPanel_bpc_comsLayout);
jPanel_bpc_comsLayout.setHorizontalGroup(
jPanel_bpc_comsLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
.add(jPanel_bpc_comsLayout.createSequentialGroup()
.addContainerGap()
.add(jButton_bpc)
.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
.add(jButton_com1)
.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
.add(jButton_com2)
.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
.add(jButton_com3)
.addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
.add(jButton_com4)
.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
.add(jButton_com5)
.addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
						.add(jButton_com6)
						.addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED)
						.add(jButton_com7)
						.addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED)
						.add(jButton_com8)
						.addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED)
						.add(jButton_com9)
						.addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED)
						.add(jButton_com10)
						.addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED)
						.add(jButton_stopAlert)
						.addPreferredGap(
								org.jdesktop.layout.LayoutStyle.RELATED)
						.add(jButton_clearOutputList)
						.addContainerGap(
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								Short.MAX_VALUE)));
		jPanel_bpc_comsLayout
				.setVerticalGroup(jPanel_bpc_comsLayout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(jPanel_bpc_comsLayout
								.createSequentialGroup()
								.addContainerGap()
								.add(jPanel_bpc_comsLayout
										.createParallelGroup(
												org.jdesktop.layout.GroupLayout.BASELINE)
										.add(jButton_bpc).add(jButton_com1)
										.add(jButton_com2).add(jButton_com3)
										.add(jButton_com4).add(jButton_com5)
										.add(jButton_com6).add(jButton_com7)
										.add(jButton_com8).add(jButton_com9)
										.add(jButton_com10)
										.add(jButton_stopAlert)
										.add(jButton_clearOutputList))
								.addContainerGap(
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));

		jPanel_coms_status.setBackground(javax.swing.UIManager.getDefaults()
				.getColor("info"));

		jList_coms_status.setFont(new java.awt.Font("微软雅黑", 0, 18));
		jList_coms_status.setModel(new javax.swing.AbstractListModel() {
			String[] strings = { "" };

			public int getSize() {
				return strings.length;
			}

			public Object getElementAt(int i) {
				return strings[i];
			}
		});
		jScrollPane6.setViewportView(jList_coms_status);

		org.jdesktop.layout.GroupLayout jPanel_coms_statusLayout = new org.jdesktop.layout.GroupLayout(
				jPanel_coms_status);
		jPanel_coms_status.setLayout(jPanel_coms_statusLayout);
		jPanel_coms_statusLayout
				.setHorizontalGroup(jPanel_coms_statusLayout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(org.jdesktop.layout.GroupLayout.TRAILING,
								jPanel_coms_statusLayout
										.createSequentialGroup()
										.addContainerGap()
										.add(jScrollPane6,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												1234, Short.MAX_VALUE)
										.addContainerGap()));
		jPanel_coms_statusLayout
				.setVerticalGroup(jPanel_coms_statusLayout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(org.jdesktop.layout.GroupLayout.TRAILING,
								jPanel_coms_statusLayout
										.createSequentialGroup()
										.add(28, 28, 28)
										.add(jScrollPane6,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												331, Short.MAX_VALUE)));

		jPanel_data_monitor.setBackground(new java.awt.Color(255, 255, 255));

		jPanel_com_rev.setBackground(javax.swing.UIManager.getDefaults()
				.getColor("RadioButtonMenuItem.selectionBackground"));

		org.jdesktop.layout.GroupLayout jPanel_com_revLayout = new org.jdesktop.layout.GroupLayout(
				jPanel_com_rev);
		jPanel_com_rev.setLayout(jPanel_com_revLayout);
		jPanel_com_revLayout.setHorizontalGroup(jPanel_com_revLayout
				.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(jPanel_com_revLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_Com_Rev,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								590, Short.MAX_VALUE).addContainerGap()));
		jPanel_com_revLayout.setVerticalGroup(jPanel_com_revLayout
				.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(jPanel_com_revLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_Com_Rev,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								80, Short.MAX_VALUE).addContainerGap()));

		jPanel_com_send.setBackground(javax.swing.UIManager.getDefaults()
				.getColor("CheckBoxMenuItem.selectionBackground"));

		org.jdesktop.layout.GroupLayout jPanel_com_sendLayout = new org.jdesktop.layout.GroupLayout(
				jPanel_com_send);
		jPanel_com_send.setLayout(jPanel_com_sendLayout);
		jPanel_com_sendLayout.setHorizontalGroup(jPanel_com_sendLayout
				.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(jPanel_com_sendLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_com_send,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								597, Short.MAX_VALUE).addContainerGap()));
		jPanel_com_sendLayout.setVerticalGroup(jPanel_com_sendLayout
				.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(jPanel_com_sendLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_com_send,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								80, Short.MAX_VALUE).addContainerGap()));

		jPanel_bpc.setBackground(javax.swing.UIManager.getDefaults().getColor(
				"Menu.selectionBackground"));

		org.jdesktop.layout.GroupLayout jPanel_bpcLayout = new org.jdesktop.layout.GroupLayout(
				jPanel_bpc);
		jPanel_bpc.setLayout(jPanel_bpcLayout);
		jPanel_bpcLayout.setHorizontalGroup(jPanel_bpcLayout
				.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(jPanel_bpcLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_bpc_data,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								590, Short.MAX_VALUE).addContainerGap()));
		jPanel_bpcLayout.setVerticalGroup(jPanel_bpcLayout.createParallelGroup(
				org.jdesktop.layout.GroupLayout.LEADING).add(
				jPanel_bpcLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_bpc_data,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								80, Short.MAX_VALUE).addContainerGap()));

		jPanel_cache.setBackground(javax.swing.UIManager.getDefaults()
				.getColor("Menu.selectionBackground"));

		org.jdesktop.layout.GroupLayout jPanel_cacheLayout = new org.jdesktop.layout.GroupLayout(
				jPanel_cache);
		jPanel_cache.setLayout(jPanel_cacheLayout);
		jPanel_cacheLayout.setHorizontalGroup(jPanel_cacheLayout
				.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(jPanel_cacheLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_error,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								597, Short.MAX_VALUE).addContainerGap()));
		jPanel_cacheLayout.setVerticalGroup(jPanel_cacheLayout
				.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
				.add(jPanel_cacheLayout
						.createSequentialGroup()
						.addContainerGap()
						.add(textArea_error,
								org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
								80, Short.MAX_VALUE).addContainerGap()));

		org.jdesktop.layout.GroupLayout jPanel_data_monitorLayout = new org.jdesktop.layout.GroupLayout(
				jPanel_data_monitor);
		jPanel_data_monitor.setLayout(jPanel_data_monitorLayout);
		jPanel_data_monitorLayout
				.setHorizontalGroup(jPanel_data_monitorLayout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(jPanel_data_monitorLayout
								.createSequentialGroup()
								.addContainerGap()
								.add(jPanel_data_monitorLayout
										.createParallelGroup(
												org.jdesktop.layout.GroupLayout.LEADING)
										.add(jPanel_bpc,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.add(jPanel_com_rev,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED)
								.add(jPanel_data_monitorLayout
										.createParallelGroup(
												org.jdesktop.layout.GroupLayout.LEADING)
										.add(jPanel_cache,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.add(jPanel_com_send,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addContainerGap()));
		jPanel_data_monitorLayout
				.setVerticalGroup(jPanel_data_monitorLayout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(jPanel_data_monitorLayout
								.createSequentialGroup()
								.addContainerGap()
								.add(jPanel_data_monitorLayout
										.createParallelGroup(
												org.jdesktop.layout.GroupLayout.TRAILING)
										.add(jPanel_com_send,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.add(jPanel_com_rev,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED)
								.add(jPanel_data_monitorLayout
										.createParallelGroup(
												org.jdesktop.layout.GroupLayout.LEADING)
										.add(jPanel_cache,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.add(jPanel_bpc,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))
								.addContainerGap()));

		org.jdesktop.layout.GroupLayout jPanel_system_monitorLayout = new org.jdesktop.layout.GroupLayout(
				jPanel_system_monitor);
		jPanel_system_monitor.setLayout(jPanel_system_monitorLayout);
		jPanel_system_monitorLayout
				.setHorizontalGroup(jPanel_system_monitorLayout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(jPanel_system_monitorLayout
								.createSequentialGroup()
								.add(551, 551, 551)
								.add(jScrollPane2,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										349, Short.MAX_VALUE)
								.add(370, 370, 370))
						.add(jPanel_system_monitorLayout
								.createSequentialGroup()
								.addContainerGap()
								.add(jPanel_data_monitor,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE))
						.add(jPanel_system_monitorLayout
								.createSequentialGroup()
								.addContainerGap()
								.add(jPanel_system_monitorLayout
										.createParallelGroup(
												org.jdesktop.layout.GroupLayout.LEADING)
										.add(jPanel_coms_status,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE)
										.add(jPanel_bpc_coms,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
												Short.MAX_VALUE))));
		jPanel_system_monitorLayout
				.setVerticalGroup(jPanel_system_monitorLayout
						.createParallelGroup(
								org.jdesktop.layout.GroupLayout.LEADING)
						.add(jPanel_system_monitorLayout
								.createSequentialGroup()
								.add(jScrollPane2,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										0,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED)
								.add(jPanel_bpc_coms,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED)
								.add(jPanel_coms_status,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
								.addPreferredGap(
										org.jdesktop.layout.LayoutStyle.RELATED)
								.add(jPanel_data_monitor,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										org.jdesktop.layout.GroupLayout.DEFAULT_SIZE,
										Short.MAX_VALUE)));

		jTabbedPane_system.addTab("\u6570\u636e\u76d1\u63a7",
				jPanel_system_monitor);

		org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(
				getContentPane());
		getContentPane().setLayout(layout);
		layout.setHorizontalGroup(layout.createParallelGroup(
				org.jdesktop.layout.GroupLayout.LEADING).add(
				jTabbedPane_system,
				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 1275,
				Short.MAX_VALUE));
		layout.setVerticalGroup(layout.createParallelGroup(
				org.jdesktop.layout.GroupLayout.LEADING).add(
				jTabbedPane_system,
				org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, 703,
				Short.MAX_VALUE));

		jTabbedPane_system.getAccessibleContext().setAccessibleName("");

		pack();
	}// </editor-fold>
	//GEN-END:initComponents

	private void jButton_clearOutputListActionPerformed(
			java.awt.event.ActionEvent evt) {
		applicationConfig.removeAllData_ListModel();
	}

	private void jButton_stopAlertActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		// bpcComStatusController.stop();
		bpcComStatusController.stopAlert();
	}

	/**
	 * @Title: jButton_com10ActionPerformed
	 * @Description: TODO(打开或关闭串口10)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com10ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(10, jButton_com10);
	}

	/**
	 * @Title: jButton_com9ActionPerformed
	 * @Description: TODO(打开或关闭串口9)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com9ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(9, jButton_com9);
	}

	/**
	 * @Title: jButton_com8ActionPerformed
	 * @Description: TODO(打开或关闭串口8)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com8ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(8, jButton_com8);
	}

	/**
	 * @Title: jButton_com7ActionPerformed
	 * @Description: TODO(打开或关闭串口9)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com7ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(7, jButton_com7);
	}

	/**
	 * @Title: jButton_com6ActionPerformed
	 * @Description: TODO(打开或关闭串口6)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com6ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(6, jButton_com6);
	}

	/**
	 * @Title: jButton_com5ActionPerformed
	 * @Description: TODO(打开或关闭串口5)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com5ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(5, jButton_com5);
	}

	/**
	 * @Title: jButton_com4ActionPerformed
	 * @Description: TODO(打开或关闭串口4)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com4ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(4, jButton_com4);
	}

	/**
	 * @Title: jButton_com3ActionPerformed
	 * @Description: TODO(打开或关闭串口3)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com3ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(3, jButton_com3);
	}

	/**
	 * @Title: jButton_com2ActionPerformed
	 * @Description: TODO(打开或关闭串口2)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com2ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(2, jButton_com2);
	}

	/**
	 * @Title: jButton_com1ActionPerformed
	 * @Description: TODO(打开或者关闭串口1)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_com1ActionPerformed(java.awt.event.ActionEvent evt) {
		// TODO add your handling code here:
		bpcComStatusController.openOrcloseCom(1, jButton_com1);
	}

	/**
	 * @Title: jButton_bpcActionPerformed
	 * @Description: TODO(登录或者退出BPC)
	 * @param @param evt
	 * @return void 返回类型
	 */
	private void jButton_bpcActionPerformed(java.awt.event.ActionEvent evt) {
		bpcComStatusController.openOrclosebpc(jButton_bpc);
	}

	private void initData() {

		// 增加数据
		// jList_bpc.setModel(applicationConfig.getListModel_bpc_data());
		// jList_com_rev.setModel(applicationConfig.getListModel_com_rev());
		applicationConfig.setTextArea_bpc_data(textArea_bpc_data);
		applicationConfig.setTextArea_Com_Rev(textArea_Com_Rev);
		applicationConfig.setTextArea_Com_Send(textArea_com_send);
		applicationConfig.setTextArea_error(textArea_error);

		jList_coms_status
				.setModel(applicationConfig.getListModel_coms_status());
		bpcComStatusController.setJlist_coms_status(jList_coms_status);
		// for (int i = 0; i < 100; i++) {
		// commServerService.getListModel_com_rev().add(0, i);
		// commServerService.getListModel_bpc_cache().add(0, i);
		// commServerService.getListModel_bpc_data().add(0, i);
		// commServerService.getListModel_com_send().add(0, i);
		// }
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String args[]) {
		try {
			UIManager
					.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new BdcmdIndex().setVisible(true);

			}
		});
	}

	//GEN-BEGIN:variables
	// Variables declaration - do not modify
	private javax.swing.JButton jButton_bpc;
	private javax.swing.JButton jButton_clearOutputList;
	private javax.swing.JButton jButton_com1;
	private javax.swing.JButton jButton_com10;
	private javax.swing.JButton jButton_com2;
	private javax.swing.JButton jButton_com3;
	private javax.swing.JButton jButton_com4;
	private javax.swing.JButton jButton_com5;
	private javax.swing.JButton jButton_com6;
	private javax.swing.JButton jButton_com7;
	private javax.swing.JButton jButton_com8;
	private javax.swing.JButton jButton_com9;
	private javax.swing.JButton jButton_stopAlert;
	private javax.swing.JList jList_coms_status;
	private javax.swing.JPanel jPanel1;
	private javax.swing.JPanel jPanel3;
	private javax.swing.JPanel jPanel_bpc;
	private javax.swing.JPanel jPanel_bpc_coms;
	private javax.swing.JPanel jPanel_cache;
	private javax.swing.JPanel jPanel_com_rev;
	private javax.swing.JPanel jPanel_com_send;
	private javax.swing.JPanel jPanel_coms_status;
	private javax.swing.JPanel jPanel_data_monitor;
	private javax.swing.JScrollPane jScrollPane2;
	private javax.swing.JScrollPane jScrollPane6;
	private javax.swing.JTabbedPane jTabbedPane_system;
	private javax.swing.JTable jTable_com_send;
	private java.awt.TextArea textArea_Com_Rev;
	private java.awt.TextArea textArea_bpc_data;
	private java.awt.TextArea textArea_com_send;
	private java.awt.TextArea textArea_error;
	// End of variables declaration//GEN-END:variables

}