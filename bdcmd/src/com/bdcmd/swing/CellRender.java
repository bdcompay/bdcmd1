package com.bdcmd.swing;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import com.bdcmd.service.com.bean.ToComBdDataBean;

/*
 JList wmslist = new JList();
 DefaultListModel listModel2 = new DefaultListModel();
 listModel2.addElement("固定电话:" + rs.getString("phone"));
 listModel2.addElement("手机号码:" + rs.getString("mbphone"));
 listModel2.addElement("联系人:" + rs.getString("contact"));
 listModel2.addElement("承运商:" + rs.getString("ship_via"));
 listModel2.addElement("地址:" + rs.getString("address"));
 wmslist.setModel(listModel2);
 //wmslist.setCellRenderer(new MyRenderer(1, Color.RED));
 wmslist.setCellRenderer(new MyRenderer(new int[]{2, 3}, Color.RED));
 */

public class CellRender extends DefaultListCellRenderer {

	private Color rowcolor;
	private int row;
	private static ConcurrentHashMap<Integer, Color> rowsColor= new ConcurrentHashMap<Integer, Color>();

	public CellRender(int row, Color color) {
		this.rowcolor = color;
		this.row = row;
		
	}

	public Component getListCellRendererComponent(JList list, Object value,
			int index, boolean isSelected, boolean cellHasFocus) {
		super.getListCellRendererComponent(list, value, index, isSelected,
				cellHasFocus);

		if (index == row) {
			setBackground(this.rowcolor);
			rowsColor.put(index, this.rowcolor);
			// setFont(getFont().deriveFont((float) (getFont().getSize())));
		} else {
			Color color = rowsColor.get(index);
			if (color != null) {
				setBackground(color);
			}

		}

		return this;
	}
}