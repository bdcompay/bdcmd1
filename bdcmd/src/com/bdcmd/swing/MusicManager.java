/**  
 * @Title: MusicManager.java 
 * @Package com.bdcmd.swing 
 * @Description: TODO(用一句话描述该文件做什么) 
 * @author A18ccms A18ccms_gmail_com  
 * @date 2013-6-26 下午2:16:08 
 * @version V1.0  
 */

package com.bdcmd.swing;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.sound.sampled.*;
import javax.swing.text.StyledEditorKit.BoldAction;

/**
 * 项目名称：bdcmdCom 类名称：MusicManager 类描述： 创建人：will.yan 创建时间：2013-6-26 下午2:16:08
 * 
 * @version
 */

public class MusicManager {
	private boolean isStop = false;

	public void play(String filename) {
		try {
			isStop=false;
			// Play now.
			while (!isStop) {
				File file = new File(filename);
				AudioInputStream in = AudioSystem.getAudioInputStream(file);
				AudioInputStream din = null;
				AudioFormat baseFormat = in.getFormat();

				AudioFileFormat baseFileFormat = AudioSystem
						.getAudioFileFormat(file);
				// System.out.println(baseFileFormat);
				// System.out.println(baseFileFormat.properties());

				AudioFormat decodedFormat = new AudioFormat(
						AudioFormat.Encoding.PCM_SIGNED,
						baseFormat.getSampleRate(), 16,
						baseFormat.getChannels(), baseFormat.getChannels() * 2,
						baseFormat.getSampleRate(), false);
				din = AudioSystem.getAudioInputStream(decodedFormat, in);
				rawplay(decodedFormat, din);
				in.close();
			}

		} catch (Exception e) {

		}
	}

	public void stop() {
		this.isStop = true;
	}

	private void rawplay(AudioFormat targetFormat, AudioInputStream din)
			throws IOException, LineUnavailableException {
		byte[] data = new byte[4096];
		SourceDataLine line = getLine(targetFormat);
		if (line != null) {
			// Start
			line.start();
			int nBytesRead = 0;
			int nBytesWritten = 0;
			while (nBytesRead != -1) {
				nBytesRead = din.read(data, 0, data.length);
				if (nBytesRead != -1)
					nBytesWritten = line.write(data, 0, nBytesRead);
			}
			// Stop
			line.drain();
			line.stop();
			line.close();
			din.close();
		}
	}

	private SourceDataLine getLine(AudioFormat audioFormat)
			throws LineUnavailableException {
		SourceDataLine res = null;
		DataLine.Info info = new DataLine.Info(SourceDataLine.class,
				audioFormat);
		res = (SourceDataLine) AudioSystem.getLine(info);
		res.open(audioFormat);
		return res;
	}

	public static String toHexString(byte[] b) {
		if (b == null)
			return null;
		StringBuilder sb = new StringBuilder();
		for (byte x : b) {
			String tt = Integer.toHexString(x & 0xff);
			sb.append(tt).append("00".substring(tt.length()));
		}
		return sb.toString();
	}

	public static byte[] md5(String str) {
		MessageDigest md;
		byte b[] = null;
		try {
			md = MessageDigest.getInstance("MD5");
			md.update(str.getBytes());
			b = md.digest();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return b;
	}

}
