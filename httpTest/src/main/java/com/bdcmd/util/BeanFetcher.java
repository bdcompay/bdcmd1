package com.bdcmd.util;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.stereotype.Component;

/**
 * 对beanFactory的封�?
 * 
 * @author QingFeng.Su
 * 
 */
@Component
public class BeanFetcher implements BeanFactoryAware {
	// 定义BeanFactory为类静�?�变�?
	private static BeanFactory factory;

	// 定义为类静�?�方�?
	public static <T> T getBean(String beanId, Class<T> clazz) {
		return factory.getBean(beanId, clazz);
	}

	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		factory = beanFactory;

	}
}
