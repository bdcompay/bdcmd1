package com.bdcmd.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdcmd.dao.IDownMessageDao;
import com.bdcmd.entity.DownMessage;

@Repository("downmessageDaoImpl")
public class DownMessageDaoImpl extends IDaoImpl<DownMessage, Integer>
		implements IDownMessageDao {

}
