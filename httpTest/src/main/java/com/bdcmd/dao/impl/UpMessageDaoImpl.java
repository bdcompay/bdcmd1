package com.bdcmd.dao.impl;

import org.springframework.stereotype.Repository;

import com.bdcmd.dao.IUpMessageDao;
import com.bdcmd.entity.UpMessage;

@Repository("upmessageDaoImpl")
public class UpMessageDaoImpl extends IDaoImpl<UpMessage, Integer> implements
		IUpMessageDao {

}
