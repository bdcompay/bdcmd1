package com.bdcmd.web.handler;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import util.PostParamBdData;
import util.SendResut;

import com.bdcmd.entity.DownMessage;
import com.bdcmd.entity.UpMessage;
import com.bdcmd.http.json.ObjectResult;
import com.bdcmd.http.json.ResultStatus;
import com.bdcmd.service.IDownMessageService;
import com.bdcmd.service.IUpMessageService;

@Controller
@RequestMapping("/")
public class HttpTest {
	@Autowired
	private IUpMessageService upMessageServiceImpl;
	@Autowired
	private IDownMessageService downMessageServiceImpl;

	// comdetail页面
	@RequestMapping(value = "/popData", method = RequestMethod.POST)
	public @ResponseBody ObjectResult popData(
			@RequestBody PostParamBdData postParamBdData) {
		ObjectResult objectResult = new ObjectResult();
		System.out.println(postParamBdData.getServerCardNumber());
		try {
			// CacheCardDataApiDto result = bdControl.popData(
			// postParamBdData.getServerCardNumber(),
			// postParamBdData.getChannelGroupName());
			
			SendResut result = new SendResut();
			// System.out.println(postParamBdData.getServerCardNumber());
			// System.out.println(postParamBdData.getChannelGroupName());
//			DownMessage down = downMessageServiceImpl.find(1);
			DownMessage down = downMessageServiceImpl.findByCard(postParamBdData.getServerCardNumber());
//			if(postParamBdData.getServerCardNumber().equals("306333")){
//				down=null;
//			}
			if (down != null) {
				result.setCardNumber(down.getCard());
				result.setData(down.getMessage());
				if (result != null && down.getMessage() != null
						&& !down.getMessage().equals("")) {
					objectResult.setStatus(ResultStatus.OK);
					objectResult.setResult(result);
				} else {
					objectResult.setStatus(ResultStatus.NOT_EXIST);
				}
				//
				DownMessage down1 = new DownMessage();
				Date date = new Date();
				down1.setDate(date);
				down1.setCard(postParamBdData.getServerCardNumber());
				down1.setMessage(down.getMessage());
			}
			// downMessageServiceImpl.save(down1);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		// System.out.println(httpPostReq.getEntity());
		return objectResult;
	}

	/**
	 * 通道接口机提交数据接口
	 * 
	 * @param channelType
	 * @param dataStr
	 * @return
	 */
	@RequestMapping(value = "/pullBdData", method = RequestMethod.POST)
	public @ResponseBody ObjectResult pullBdData(
			@RequestBody PostParamBdData postParamBdData) {

		ObjectResult objectResult = new ObjectResult();
		try {
			// Boolean result = bdControl
			// .pullRevData(postParamBdData.getDataStr());
			Boolean result = true;
			System.out.println("收到返回" + postParamBdData.getDataStr());
			UpMessage up = new UpMessage();
			up.setMessage(postParamBdData.getDataStr());
			Date date = new Date();
			up.setDate(date);
			upMessageServiceImpl.save(up);
			if (result) {
				objectResult.setResult(true);
				objectResult.setStatus(ResultStatus.OK);
			} else {
				objectResult.setResult(false);
				objectResult.setStatus(ResultStatus.FAILED);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			objectResult.setResult(null);
			objectResult.setStatus(ResultStatus.SYS_ERROR);
		}
		return objectResult;
	}

}
