package com.bdcmd.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdcmd.dao.IDownMessageDao;
import com.bdcmd.entity.DownMessage;
import com.bdcmd.service.IDownMessageService;

@Service("downMessageServiceImpl")
public class DownMessageServiceImpl implements IDownMessageService {
	@Autowired
	private IDownMessageDao downmessageDaoImpl;

	public void save(DownMessage down) {
		downmessageDaoImpl.save(down);
	}

	public DownMessage find(int id) {
		return downmessageDaoImpl.get(id);
	}

	public DownMessage findByCard(String card) {
		// TODO Auto-generated method stub
		DownMessage downMessage=null;
		String hql="from DownMessage where card="+card;
		List<DownMessage> l=downmessageDaoImpl.getList(hql, 0, 1, null);
		if(l!=null&&l.size()>0){
			downMessage=l.get(0);
		}
		return downMessage;
	}

}
