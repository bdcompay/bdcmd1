package com.bdcmd.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bdcmd.dao.IUpMessageDao;
import com.bdcmd.entity.UpMessage;
import com.bdcmd.service.IUpMessageService;

@Service("upMessageServiceImpl")
public class UpMessageServiceImpl implements IUpMessageService {
	@Autowired
	private IUpMessageDao upmessageDaoImpl;

	public void save(UpMessage up) {
		upmessageDaoImpl.save(up);
	}

}
