package com.bdcmd.service;

import com.bdcmd.entity.DownMessage;

public interface IDownMessageService {
	public void save(DownMessage down);

	public DownMessage find(int id);
	
	public DownMessage findByCard(String card);

}
