package util;

public class PostParamBdData {
	private String bdChannel;
	private String dataStr;
	private String serverCardNumber;
	private String channelGroupName;

	public String getBdChannel() {
		return bdChannel;
	}

	public void setBdChannel(String bdChannel) {
		this.bdChannel = bdChannel;
	}

	public String getDataStr() {
		return dataStr;
	}

	public void setDataStr(String dataStr) {
		this.dataStr = dataStr;
	}

	public String getServerCardNumber() {
		return serverCardNumber;
	}

	public void setServerCardNumber(String serverCardNumber) {
		this.serverCardNumber = serverCardNumber;
	}

	public String getChannelGroupName() {
		return channelGroupName;
	}

	public void setChannelGroupName(String channelGroupName) {
		this.channelGroupName = channelGroupName;
	}

}