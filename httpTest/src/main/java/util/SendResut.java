package util;

import java.util.List;

import com.bdcmd.http.json.ObjectResult;

public class SendResut extends ObjectResult {

	private String cardNumber;
	private String data;

	public String getCardNumber() {
		return cardNumber;
	}

	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

}
